<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

header('Content-Type: text/javascript');
?>
$(function() {
    $("#download").click(function(e) {
        e.preventDefault();
        $("#skip").remove();
        $(this).replaceWith('<div class="exw exw-spinner"></div><div id="note">Installing... Please wait</div><progress id="progress" min="0" max="100"></progress>');
            $.post("<?php if(isset($_SERVER['QUERY_STRING'])) echo '?'.$_SERVER['QUERY_STRING']; ?>", function(data, status) {
               
            });
            $("#progress").val(0);
                var e = setInterval(function() {
                    $.getJSON("/progress", function(data) {
                    $("#progress").html(data.progress);
                    $("#progress").pval(data.progress);
                    $("#note").html(data.message);
                    if(data.progress == '100' || data.message == 'Something went wrong :/') {
                        clearInterval(e);
                        if(data.progress == '100') {
                            $('.exw-spinner').addClass('close');
                            $("#note").html('Complete');
                        } else {
                            $('.exw-spinner').addClass('exw-error');
                            $("#progress").addClass('exw-error');
                            console.error(data.debug);
                        }
                    }
                });
                }, 1000);
            
    });
    
    $("#toggler").click(function(e) {
        e.preventDefault();
       $("#release").slideToggle();
       var toggle = $(this).html();
       if(toggle == 'Show') $(this).html('Hide');
       else $(this).html('Show');
    });
    $("#skip").click(function(e) {
        e.preventDefault();
        var $this = $(this);
       $.post("", {action:"skip", version:$(this).data("version")}, function(data, status) {
           $("#upgradeversion").fadeOut();
       });
           
    });
    
});
<?php die;