<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$theme = new EternityX\Theme(EX_THEME);
$theme = $theme->getManifest();

ob_start(); ?>
<h1>Theme Credits</h1>
<img src="<?=EX_URL;?>/api/v2/theme/icon" alt="<?=$theme['name']?> icon" style="float: left; margin-right: 10px;" />
<div>
    <strong>Name:</strong> <?=$theme['name'];?><br>
    <strong>Description:</strong> <?=$theme['description'];?><br>
    <strong>Author:</strong> <?=$theme['author'];?><br>
    <strong>Copyright:</strong> <?=$theme['copyright'];?><br>
    <strong>URL:</strong> <a href="<?=$theme['url'];?>"><?=$theme['url'];?></a><br>
    <strong>Version:</strong> <?=$theme['version'];?>
</div>
<?php
$content = trim(ob_get_clean());

$page = new EternityX\Page('Theme');
$page->setTheme(new \EternityX\Theme(EX_THEME));
$page->setContent($content);
$page->setAssets(null);
$page->setMetadata(null);
die($page);