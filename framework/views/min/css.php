<?php

header('Content-Type: text/css');
$this->setCache(false);
foreach(glob(APP_ROOT.'/assets/css/*.css') as $filename) {
	ob_start();
	include $filename;
	$content = trim(ob_get_clean());
	
	$mincontent = new Minify($content, Minify::TYPE_CSS);
	echo $mincontent->minify() . PHP_EOL;
}  
die;