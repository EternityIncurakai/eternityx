<?php

header('Content-Type: text/javascript');
$this->setCache(false);
ob_start();
if(APP_ROOT != EX_ROOT.'/application') {
    foreach(glob_recursive(EX_ROOT.'/application/assets/js/*.js') as $filename) {
	include $filename;
}  
}
foreach(glob_recursive(APP_ROOT.'/assets/js/*.js') as $filename) {
	include $filename;
}  

foreach(glob_recursive(ENV_ROOT.'/library/js/*.js') as $filename) {
    include $filename;
}
$theme = new \EternityX\Theme(EX_THEME);
echo $theme->getBehavior();

$content = trim(ob_get_clean());
$mincontent = new \EternityX\Minify($content, \EternityX\Minify::TYPE_JS);
echo $mincontent->minify();
die;