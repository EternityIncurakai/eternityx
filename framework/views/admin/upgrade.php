<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if($this->request->method == 'post') {
    header('Content-Type: text/plain');
    if(!isset($_POST['action'])) $_POST['action'] = '';
    switch($_POST['action']) {
        case 'skip' :
            die((string)file_put_contents(ENV_ROOT.'/data/skips.csv', file_get_contents(ENV_ROOT.'/data/skips.csv').','.$_POST['version']));
        default :
            include EX_ROOT.'/maintenance/upgrading/utilities/setup.php';
            die;
    }
}

ob_start();
if(!file_exists(ENV_ROOT.'/data/temp/upgradeavailable.txt')) ex_check_upgrade();
clearstatcache();
if(file_exists(ENV_ROOT.'/data/temp/upgradeavailable.txt') && !in_array(file_get_contents(ENV_ROOT.'/data/temp/upgradeavailable.txt'), parse_csv_file(ENV_ROOT.'/data/skips.csv'))) {
        $ver = file_get_contents(ENV_ROOT.'/data/temp/upgradeavailable.txt');
        $release = (array)json_decode(file_get_contents(EX_UPGRADE_VER_URL));
        $size = $release['size'] / 1024;
        $release = $release['release'];
        $btn = new \EternityX\UI\Button('download', 'green');
        $btn->href = "download";
        $btn->text = "Install";
        $btn->icon = "download";
        
        ?>
        <div id="upgradeversion">
        <div class="top">
            <img src="https://eternityincurakai.com/img/favicon.png" alt="" />
            <h1>EternityX v<?=$ver;?></h1>
            <h2>Eternity Incurakai Studios</h2>
            <h3><?=round($size, 2)?> KB</h3>
            <?=$btn;?>
            <a id="skip" href="#" data-version="<?=$ver;?>">Or, Skip This Version</a>
        </div><?php
        echo "<strong id='bar'>Release Notes: <a href=\"#\" id=\"toggler\">Hide</a></strong><br><textarea readonly id='release'>{$release}</textarea>";
        echo "</div>";
} else { ?>
<div class="updated">
    <h1>Everything is up to date!</h1>
    <p>We don't have any new stuff.  Have a nice day! ☺</p>
</div>
<?php
}

$content = trim(ob_get_clean());

ob_start();
$query = (isset($_SERVER['QUERY_STRING'])) ? '?'.$_SERVER['QUERY_STRING'] : '';
?>
<style>
    
    .updated {
        color: #666;
        border: 5px solid #666;
        border-radius: 25px;
        text-align: center;
        box-shadow: 0 0 5px #666;
        position: relative;
        height: 150px;
    }
    
    .updated:before {
        content: '\f118';
        font-family: fontAwesome;
        position: absolute;
        top: 0;
        left: 0;
        font-size: 100px;
        line-height: 150px;
        padding-left: 50px;
        
    }
    
    .top h1, .top h2, .top h3 {
        margin: 0;
        padding: 0;
        font-weight: normal;
    }
    .top img {
        height: 100px;
        width: 100px;
        margin-right: 30px;
        float: left;
    }
    .top {
        position: relative;
        padding: 30px;
        background: -moz-linear-gradient(top,  rgba(97,179,41,1) 0%, rgba(57,130,53,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(97,179,41,1)), color-stop(100%,rgba(57,130,53,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(97,179,41,1) 0%,rgba(57,130,53,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(97,179,41,1) 0%,rgba(57,130,53,0) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(97,179,41,1) 0%,rgba(57,130,53,0) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(97,179,41,1) 0%,rgba(57,130,53,0) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#61b329', endColorstr='#00398235',GradientType=0 ); /* IE6-9 */        
    }
    
    #download, .exw-spinner {
        position: absolute;
        top: 30px;
        right: 30px;
    }
    
    #skip {
        position: absolute;
        top: 105px;
        right: 100px;
        color: #060 !important;
        cursor: pointer !important;
    }
    
    #release {
        width: calc(100% - 10px);
        padding: 5px;
        font-family: monospace;
        height: 300px;
    }
    #note {
        position:absolute;
        bottom: 30px;
        right: 30px;
        font-size: 1.1em;
    }
    
    progress {
        position: absolute;
        bottom: 0;
        right: 0;
        width: 100%;
        color: #61B329;
        height: 10px;
        background: #CCC;
        border-radius: 0;
    }
    
    progress::-webkit-progress-value {
        border-radius: 0 5px 5px 0;
        background: #61B329 !important;
        box-shadow: 5px 0 5px #61B329;
    }
    
    progress::-moz-progress-bar {
        border-radius: 0 5px 5px 0;
        background: #61B329 !important;
        box-shadow: 5px 0 5px #61B329;
    }
    
    progress::-webkit-progress-bar {
        background: #CCC;
    }
    #bar {
        background: #61B329;
        border: 2px solid #50A218;
        display: block;
        height: 20px;
        line-height: 20px;
        padding-left: 10px;
    }
    
    #bar a {
        float: right;
        margin-right: 20px;
    }
</style>
<script defer src="/js/upgrade.js<?=$query;?>"></script>
<?php$assets = ob_get_clean();

$page = new \EternityX\Page('Upgrade');
$page->setTheme($GLOBALS['theme']);
$page->setAssets($assets);
$page->setMetadata(null);
$page->setContent($content);
die($page);