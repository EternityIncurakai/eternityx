<?php

// get all defined/declared stuff 
$classes = get_declared_classes();
$interfaces = get_declared_interfaces();
$traits = get_declared_traits();
$functions = get_defined_functions();
$constants = get_defined_constants();
$variables = get_defined_vars();


function debugsection($struct, $vars, $recursion=FALSE, $named=FALSE) {
	echo "<section>";
	echo "<h2>{$struct}:</h2>";
	echo "<ul>";
	if($recursion)
	{
		foreach($vars as $key=>$ar)
		{
			echo "<h3>{$key}:</h3>";
			if($named)
			{
				foreach($ar as $name=>$var) echo "<li>".$name."=>".$var."</li>";
			}
			else
			{
				foreach($ar as $var) echo "<li>".$var."</li>";
			}
		}
	}
	else
	{
		foreach($vars as $var) echo "<li>".$var."</li>";
	}
	echo "</ul>";
	echo "</section>";
}

ob_start();
echo "<h1>Debug</h1>";
debugsection('Classes', $classes);
debugsection('Interfaces', $interfaces);
debugsection('Traits', $traits);
debugsection('Functions', $functions, TRUE);
debugsection('Constants', $constants, FALSE, TRUE);
debugsection('Variables', $variables, FALSE, TRUE);
$content = trim(ob_get_clean());

$page = new \EternityX\Page('Debug');
$page->setTheme(new \EternityX\Theme(EX_THEME));
$page->setContent($content);
$page->setMetadata(null);
$page->setAssets(null);

die($page);
