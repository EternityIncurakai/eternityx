<?php
$this->authTo(EternityX\Dispatcher::AUTH_MOD);

ob_start();
phpinfo();
$content = trim(ob_get_clean());
$removes = array(
	1 => "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>",
	2 => "a:link {color: #000099; text-decoration: none; background-color: #ffffff;}\na:hover {text-decoration: underline;}",
	3 => "<title>phpinfo()</title><meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW,NOARCHIVE\" /></head>",
	4 => "<body>",
	5 => "</body>",
	6 => "</html>"
	
);
$content = str_replace($removes, "", $content);
$content = str_replace("img {float: right; border: 0px;}", "#container img {float: right; border: 0px;}", $content);
ob_start(); ?>
<style>
	table {
		width: 80%;
	}
</style>
<?php $content .= trim(ob_get_clean());
   
$page = new \EternityX\Page('PHP Information');
$page->setTheme(new \EternityX\Theme(EX_THEME));
$page->setContent($content);
$page->setAssets(null);
$page->setMetadata(null);

die($page);

