<?php
/**
 * The core bootstrapper for EternityX.
 * 
 * This is the class that maps all important directories and files and imports
 * them when needed. Which at this time, is right away, just to have all of them
 * in memory.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since 2.0.12 "Sunbeam 12"
 * @version 0.1.0-alpha
 */
class Bootstrapper {
    private $dirs = ['models','libraries'];
    
    const MODE_PRODUCTION = 1;
    const MODE_DEVELOPMENT = 2;
    const MODE_MAINTENANCE = 3;
    static public $mode = self::MODE_DEVELOPMENT;
    
    public function __construct() {
        foreach(parse_ini_file(ENV_ROOT.'/configs/php.ini') as $key => $val) 
            ini_set($key, $val);
    }
    
    
    public function setupMode() {
        $host = $_SERVER['HTTP_HOST'];
        
        if(isset($_GET['mode']) && $_GET['mode'] == 'production') 
            $override = self::MODE_PRODUCTION;
        
        if((isset($_ENV['unittesting']) && $_ENV['unittesting'] == true) || (isset($_GET['mode']) && $_GET['mode'] == 'development')) 
            $override = self::MODE_DEVELOPMENT;
        
        if(in_array($host, ['http://localhost', '127.0.0.0', '127.0.0.1'])) self::$mode = self::MODE_DEVELOPMENT;
        if(isset($override)) self::$mode = $override;
        
        // light maintenance mode
        if(file_exists(EX_ROOT.'/.maintenance')) self::$mode = self::MODE_MAINTENANCE;
        
        define('DEBUG', (self::$mode == self::MODE_DEVELOPMENT) ? true : false);
        
        if(DEBUG) {
            ini_set('expose_php', 1);
            ini_set('display_errors', 1);
            ini_set('log_errors', 1);
        }
    }
    
    
    public function dispatch() {
        if(self::$mode == self::MODE_DEVELOPMENT || self::$mode == self::MODE_PRODUCTION) { return; }
        else {
            
            new EternityX\Maintenance\Page;
        }
    }
    
    /**
     * Imports files recursively using glob().
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 0.1.0-alpha
     * 
     * @param string $dir the directory to search
     * @param boolean $recursive determines whether to return the recursively fetched files.
     * @return mixed
     */
    private function import($dir, $recursive = false) {
        $pattern = $dir.'/*.php';
        $files = glob($pattern);
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dire) $files = array_merge($files, $this->import($dire, true));
        if($recursive) return $files;
        foreach($files as $file) include_once $file;
    }
    
    
    /**
     * Sets the map in which to import files.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 0.1.0-alpha
     * 
     * @param array $dirs array containing a list of directories to map
     * @throws Exception
     */
    public function mapDirs(array $dirs) {
        foreach($this->dirs as $key => $val) {
            if(!array_key_exists($val, $dirs)) { throw new Exception('Not all directories were mapped'); }
        }
        
        foreach($dirs as $dir) { 
            if(!is_dir($dir)) throw new Exception("Directory {$dir} does not exist"); 
            $this->import($dir);
        }
    }
    
    /**
     * Sets where to look for the functions file.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 0.1.0-alpha
     * 
     * @param type $file
     * @throws Exception
     */
    public function mapFunctions($file) {
        if(!file_exists($file)) throw new Exception('Function file specified does not exist');
        include $file;
    }
    
    /**
     * Creates a aliases based on values presented in the array.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 0.1.0-alpha
     * 
     * @param array $aliases
     */
    public function mapAliases(array $aliases) {
        foreach($aliases as $class => $alias) {
            class_alias($class, $alias);
        }
    }
}
