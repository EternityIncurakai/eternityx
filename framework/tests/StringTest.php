<?php
/**
 * Unit Test for the String Class.
 * 
 * This is the unit test for the string
 * class residing in the String package
 * for EternityX.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since 2.1.0α2
 * @package EternityX\Strings
 */

namespace EternityX\Tests;

use EternityX\String;

class StringTest extends \PHPUnit_Framework_Testcase {
    
    public function testBeginsWith() {
        $this->assertTrue(String::beginsWith('StringTestcase', 'String'));
        $this->assertFalse(String::beginsWith('StringTestcase', 'Food'));
    }
    
    public function testEndsWith() {
        $this->assertTrue(String::endsWith('StringTestcase', 'case'));
        $this->assertFalse(String::endsWith('StringTestcase', 'Drink'));
    }
    
    public function testHasUppercase() {
        $this->assertTrue(String::hasUppercase('String'));
        $this->assertFalse(String::hasUppercase('string'));
    }
    
    public function testHasLowercase() {
        $this->assertTrue(String::hasLowercase('String'));
        $this->assertFalse(String::hasLowercase('STRING'));
    }
    
    public function testHasInteger() {
        $this->assertTrue(String::hasInteger('String1'));
        $this->assertFalse(String::hasInteger('String'));
    }
    
    public function testHasSpecial() {
        $this->assertTrue(String::hasSpecial('String!'));
        $this->assertFalse(String::hasSpecial('String'));
    }
    
    public function testLengthInRange() {
        $this->assertTrue(String::lengthInRange('string', 1, 6));
        $this->assertFalse(String::lengthInRange('string', 7, 15));
    }
    
    public function testIsVersion() {
        $this->assertTrue(String::isVersion('2.1.0'));
        $this->assertTrue(String::isVersion('2.1.0α2'));
        $this->assertFalse(String::isVersion('notaversion'));
        //$this->assertFalse(String::isVersion('2.1.0α')); // all alphas should be assigned a number.
        //$this->assertFalse(String::isVersion('2.1.0π2')); // pi is not allotted a version specification
    }
    
    public function testIsDate() {
        $this->assertTrue(String::isDate('9/9/2014 9:09 AM'));
        $this->assertTrue(String::isDate('9/9/2014 9:09'));
        $this->assertTrue(String::isDate('09/09/2014 09:09 AM'));
        $this->assertTrue(String::isDate('09/09/2014'));
        $this->assertFalse(String::isDate('not a date'));
    }
    
    public function testIsURL() {
        $this->assertTrue(String::isURL('http://eternityincurakai.com'));
        $this->assertFalse(String::isURL('asdfsadfdfasdpoo'));
    } 
    
    public function testIsEmail() {
        $this->assertTrue(String::isEmail('xenok09@gmail.com'));
        $this->assertFalse(String::isEmail('xenok09@asdfjfadj0-asdfads.com'));
    }
}