<?php

namespace EternityX\Tests;

$_ENV['unittesting'] = true;
include dirname(dirname(__DIR__)).'/index.php';

class ControllerTest extends \PHPUnit_Framework_Testcase {
    
    /**
     * @dataProvider provideClass
     */
    public function testRegister($ctr) {
        $ctr->register('testcase', 'test1', function() {
            echo 'test';
        });
        
        $this->assertTrue($ctr->exists('testcase.test1'));
        $this->assertFalse($ctr->exists('testcase.test2'));
    }
    
    public function provideClass() {
        $class = new \EternityX\Controller();
        return array(array($class));
    }
    
}