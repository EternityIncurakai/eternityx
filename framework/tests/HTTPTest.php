<?php
/**
 * Unit Test for the HTTP Package.
 * 
 * This is the unit test for the request and
 * response classes residing in the HTTP package
 * for EternityX.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since 2.1.0α2
 * @package EternityX\HTTP
 */

namespace EternityX\Tests;

use \PHPUnit_Framework_Testcase;
use \EternityX\HTTP\Request;
use \EternityX\HTTP\Response;
use \EternityX\HTTP\Exception;

class HTTPTest extends PHPUnit_Framework_Testcase {
    
    public function testExists() {
        $this->assertTrue(class_exists('\EternityX\HTTP\Request'));
        $this->assertTrue(class_exists('\EternityX\HTTP\Response'));
        $this->assertTrue(class_exists('\EternityX\HTTP\Exception'));
        $this->assertTrue(trait_exists('\EternityX\Traits\tRequire'));
    }
    
    
    /**
     * @depends testExists
     */
    public function testRequest() {
        $req = new Request('http://eternityx.eternityincurakai.com/themes/exmodern/check');
        $this->assertEquals($req->getResponse()->code, 200);
        $this->assertEquals($req->getResponse()->headers['content-type'], 'text/plain');
    }    
}
