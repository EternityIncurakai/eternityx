<?php

namespace EternityX\Tests;
define('EX_INSTALLED', true);



/**
 * @todo add icon invalidation checking.
 */
class ThemeTest extends \PHPUnit_Framework_Testcase {
    public $theme;
    
    
    public function testExists() {
        $this->assertTrue(\EternityX\Theme::exists('exmodern'));
        $this->assertFalse(\EternityX\Theme::exists('blahblah2as'));
        $this->assertTrue(class_exists('EternityX\Theme'));
        $this->assertTrue(interface_exists('EternityX\iCmztn'));
    }
    
    /**
     * @depends testExists
     */
    public function testConstruct() {
        
        try { $this->theme = new \EternityX\Theme('exmodern'); }
        catch(EternityX\ThemeException $e) { $this->fail('Unexpected exception was thrown'); }
        $this->assertEquals($this->theme->getManifest()['name'], 'exmodern');
        $this->assertInstanceOf('\EternityX\Theme', $this->theme);
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 4
     * @expectedExceptionMessage Theme is incompatible with this version of EternityX
     */
    public function testBadConstructVersion() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        file_put_contents(\EternityX\Theme::DIRECTORY.'/tester/manifest.json', json_encode([
            'name' => 'tester',
            'description' => 'blah',
            'author' => 'no one',
            'url' => 'http://localhost',
            'copyright' => 'test',
            'version' => '1.0.0',
            'req_version' => '2.2.0'
        ]));
        
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
        
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 1
     * @expectedExceptionMessage Theme does not contain all required files.
     */
    public function testBadConstructManifestMissing() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 1
     * @expectedExceptionMessage Theme does not contain all required files.
     */
    public function testBadConstructTemplateMissing() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/manifest.json', \EternityX\Theme::DIRECTORY.'/tester/manifest.json');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 1
     * @expectedExceptionMessage Theme does not contain all required files.
     */
    public function testBadConstructStylesheetMissing() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/manifest.json', \EternityX\Theme::DIRECTORY.'/tester/manifest.json');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 1
     * @expectedExceptionMessage Theme does not contain all required files.
     */
    public function testBadConstructBehaviorMissing() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/manifest.json', \EternityX\Theme::DIRECTORY.'/tester/manifest.json');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
    }
    
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 1
     * @expectedExceptionMessage Theme does not contain all required files.
     */
    public function testBadConstructIconMissing() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/manifest.json', \EternityX\Theme::DIRECTORY.'/tester/manifest.json');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        $theme = new \EternityX\Theme('tester');
    } 
    /**
     * @expectedException EternityX\ThemeException
     * @expectedExceptionCode 2
     * @expectedExceptionMessage Theme manifest is invalid/corrupt
     */
    public function testBadConstructManifestInvalid() {
        $this->cleanup();
        mkdir(\EternityX\Theme::DIRECTORY.'/tester');
        file_put_contents(\EternityX\Theme::DIRECTORY.'/tester/manifest.json', json_encode([
            'name' => 'tester',
            'description' => 'blah', // author is missing - which should throw the exception
            'copyright' => 'test',
            'url' => 'http://localhost',
            'version' => '1.0.0',
            'req_version' => '2.0.8'
        ]));
        
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/page.tpl', \EternityX\Theme::DIRECTORY.'/tester/page.tpl');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/style.css', \EternityX\Theme::DIRECTORY.'/tester/style.css');    
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/behavior.js', \EternityX\Theme::DIRECTORY.'/tester/behavior.js');
        copy(\EternityX\Theme::DIRECTORY.'/exmodern/icon.png', \EternityX\Theme::DIRECTORY.'/tester/icon.png');
        $theme = new \EternityX\Theme('tester');
    }
    
    /**
     * @depends testConstruct
     * @dataProvider provideTheme
     */
    public function testGetManifest($theme) {
        $manifest = $theme->getManifest();
        $this->assertInternalType('array', $manifest);
        
        $reqs = ['name', 'description', 'author', 'url', 'version', 'copyright', 'req_version'];
        foreach($reqs as $req) $this->assertArrayHasKey($req, $manifest);
    }
    
    /**
     * @depends testConstruct
     * @dataProvider provideTheme
     */
    public function testGetTemplate($theme) {
        $tpl = $theme->getTemplate();
        $this->assertInternalType('string', $tpl);
        
        $reqs = ['meta','title','assets','baseurl','hlogo','hlogotxt','/application/includes/header/navigation.php','/application/includes/header/announcement.php','content','logo','logotxt','copyright','/application/includes/footer/navigation.php'];
        foreach($reqs as $req) $this->assertRegExp('/\<\{\{tplex\.(sub|include|echo)\:\{'.preg_quote($req,'/').'\}\/tplex\}\}\>/', $tpl);
    }
    
    /**
     * @depends testConstruct
     * @dataProvider provideTheme
     */
    public function testGetStylesheet($theme) {
        $css = $theme->getStylesheet();
        $this->assertInternalType('string', $css);
    }
    
    /**
     * @depends testConstruct
     * @dataProvider provideTheme
     */
    public function testGetBehavior($theme) {
        $js = $theme->getBehavior();
        $this->assertInternalType('string', $js);
    }
    
    /**
     * @depends testConstruct
     * @dataProvider provideTheme
     */
    public function testGetIcon($theme) {
        $img = $theme->getIcon();
        $this->assertInternalType('string', $img);
    }
    
    /**
     * @before
     * @after
     */
    public function cleanup() {
        $dir = \EternityX\Theme::DIRECTORY.'/tester';
        if(is_dir($dir)) {
            foreach(scandir($dir) as $file) {
                if ('.' === $file || '..' === $file) continue;
                unlink("$dir/$file");
            }
            rmdir($dir);
        }
    }
    
    public function provideTheme() {
        return [[new \EternityX\Theme('exmodern')]];
    }
}

