/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var user = {};
$(function() {
user = Backbone.Model.extend({
        urlRoot : '/api/v2/user',

        constructor : function() {
                var $this = this;

                // this makes a request to retreieve all the current user's information
                $.get("<?=EX_URL;?>/api/v2/user", function(data, status) {
                        if(!data) return false;
                        $this.id = data.id;
                        $this.username = data.username;
                        $this.permissions = data.permissions;
                        $this.email = data.email;
                        $this.date = data.date;
                        $this.enabled = data.enabled;	
                });
        },
});


var LoginView = Backbone.View.extend({
    el : $("#login"),
        events : {
                "keyup #username" : "validateUsername",
                "keyup #password" : "validatePassword",
                "submit form" : "login",
                "click [href='/logout']" : "logout"
        },
        initialize : function() {
            console.log('test');
        },
        render: function() {
            return this;
        },
        
        _invalid : function(el) {
            el.attr("style", "box-shadow: 0 0 5px red !important;");
            el.data('validated', "false");
                
            // disable submit
            this.$el.find('input[type="submit"]').disable();
        },
        
        _valid : function(el) {
            el.removeAttr("style");
            el.data('validated', "true");
            // attempt to enable submit
            if (this._formready()) {
                this.$el.find('input[type="submit"]').enable();
            }
        },
        
        _formready : function() {
            var username = this.$el.find('#username');
            var password = this.$el.find('#password');
            
            if(username.data('validated') == 'true' && password.data('validated') == 'true') {
                return true;
            } else {
                return false;
            }
        },
        
        validateUsername : function(e) {
            
            var el = this.$el.find('#username');
            var value = el.val();
            var length = value.length;
            
            if(e.keyCode === 13) {
                el.parent().parent().submit();
            }
            
            if((length <= 15 && length >= 5) && !value.match(/([^a-zA-Z0-9]+)/g)) {
                this._valid(el);
            } else {
                this._invalid(el);
            }
        },
        
        validatePassword : function() {
          var el = this.$el.find('#password');
          var val = el.val();
          var length = val.length;
          
          if((length <= 18 && length >= 8) && val.match(/([a-z]+)/g) && val.match(/([A-Z]+)/g) && val.match(/([0-9]+)/g) && val.match(/([^a-zA-Z0-9]+)/g)) {
              this._valid(el);
          } else {
              this._invalid(el);
          }
        },
        
        login: function(e) {
            e.preventDefault();
            var username = this.$el.find('#username').val();
            var password = this.$el.find('#password').val();
            $.post("<?=EX_URL;?>/api/v2/user/login", { username:username, password:password }, function(data, status) {
                if(data.response === true) {
                    location.reload();
                } else {
                    alert('Login failed.');
                }
            });
        }
});

var LogoutView = Backbone.View.extend({
    el : $('a[href="/logout"]'),
    events: {
        "click" : "logout"
    }, 
    
    initialize : function() {
        console.log('initialized the logout view');
    },
    
    render : function() {
        return this;
    },
    
    logout : function (e) {
        e.preventDefault();
        $.post("<?=EX_URL;?>/api/v2/user/logout", function(data, status) {
           //location.reload(); 
        });
    }
});

var login = new LoginView();
var logout = new LogoutView();
});