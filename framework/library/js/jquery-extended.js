sessionStorage.pval = 0;
jQuery.fn.extend({
    pval : function(val, span) {
        var $this = this;
        var cval = this.val();
        if(val == sessionStorage.pval || val == 0) return;
        sessionStorage.pval = val;
        if(!span) span = 1000;
        var intv = span / (val - cval);
        var i = 1;
        var t = setInterval(function () {
            if(i == val - cval) clearInterval(t);
            $this.val(++cval);
        }, intv);
    }
});