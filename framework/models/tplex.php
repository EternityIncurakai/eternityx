<?php
/* 
 * TPLEX Package
 * 
 * File containing elements of the TPLEX package:
 * it's main class and corresponding exception class.
 * Allows for easy use of templating on EternityX.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.0.10
 * @package EternityX\TPLEX
 */


namespace EternityX;

/**
 * The core TPLEX class.
 * 
 * Core class for implementing use of a 
 * tplex file or string and parsing it accordingly.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.10
 * @package EternityX\TPLEX
 */
class TPLEX {
	
    /**
     * The template file or string.
     * 
     * @var string 
     */
	protected $tpl;
    
    /**
     * Beginning and end syntaxes.
     * 
     * @var array
     */
	protected $syntax = array(
		'start' => "<\{\{tplex",
		'end' => "\/tplex\}\}>"
	);
    
    /**
     * Constructs a new TPLEX object.
     * 
     * Constructs a new TPLEX object for parsing
     * and output.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * 
     * @param string $tpl tplex file or string
     * @param string $loc location of file (defaults to APP_ROOT)
     * @throws TPLEXException
     * @return void
     */
	public function __construct($tpl, $loc = 'APP') {
        if(is_file($tpl)) {
            if(file_exists($tpl)) $this->tpl = file_get_contents($tpl);
            else if(file_exists(constant(strtoupper($loc).'_ROOT').'/templates/'.$tpl.'.tpl')) $this->tpl = constant(strtoupper($loc).'_ROOT').'/templates/'.$tpl.'.tpl';
            else throw new TPLEXException(TPLEXException::INVALID_TEMPLATE);
        } else $this->tpl = $tpl;
		$this->parse();
	}
    
    /**
     * toString implementation override.
     * 
     * overrides the toString implementation, and provides
     * the parsed template as a string for echoing. Returns empty
     * if not completely parsed.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * @since v2.1.0α1 returns empty if not completely parsed
     * 
     * @return string
     */
	public function __toString() {
        if($this->parsed()) return $this->tpl;
        else return '';
	}
    
    /**
     * Performs basic parsing actions.
     * 
     * This performs the basic parsing actions
     * involved with the TPLEX parsing process.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * 
     * @return void
     */
	public function parse() {
		$this->tpl = preg_replace_callback("/".$this->syntax['start']."\.(echo|include)\:\{([a-zA-Z0-9\$\'\"-_]+)\}".$this->syntax['end']."/is", "self::dispatch", $this->tpl);
	}
    
    /**
     * Checks whether or not the template is parsed.
     * 
     * Checks whether or not the template has been 
     * completely parsed.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return bool
     */
    public function parsed() {
        return (!preg_match("/".$this->syntax['start']."\.(echo|include|sub)\:\{([a-zA-Z0-9\$\'\"-_]+)\}".$this->syntax['end']."/is", $this->tpl));
    }
    
    /**
     * Dispatches replace methods.
     * 
     * This dispatches replace methods (sub*) for
     * easier use of parameters.  
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @param array $matches list of matches found.
     * @return string
     */
    protected function dispatch(array $matches) {
        array_shift($matches); // initial match is entire string
        $function = array_shift($matches);
        $parameters = $matches;
        
        return call_user_func_array([$this, 'sub'.ucfirst($function)], $parameters);
    }
    
    /**
     * TPLEX Echo replace method.
     * 
     * The TPLEX echo replace method, replaces
     * the respective syntax with the value of 
     * the requested constant or request key.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * @since v2.1.0α1 renamed from tplexEcho to subEcho
     * 
     * @param string $constant name of the constant to fetch
     * @param string $key name of the request key
     * @return string
     * @throws TPLEXEception
     */
    protected function subEcho($constant, $key = '') {
		if(!defined($constant) && !isset($constant)) throw new TPLEXEception(TPLEXException::INVALID_CONST);
		if(defined($constant)) return (string)constant($constant);
		else return (isset($_REQUEST[$key])) ? (string)$_REQUEST[$key] : null;				
	}
    
    /**
     * TPLEX Include replace method.
     * 
     * The TPLEX include replace method replaces
     * the respective syntax with the output of a
     * specified include file.
     *
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * @since v2.1.0α1 renamed from tplexInclude to subInclude
     * 
     * @param string $file the file to include
     * @return string
     * @throws TPLEXException
     */
    public function subInclude($file) {
        $pdo = (isset($GLOBALS['db'])) ? $GLOBALS['db'] : null; // some scripts still use the pdo var.
        
        // determine location of file
		if(file_exists(EX_ROOT.$file)) $file = EX_ROOT.$file;
		else if(file_exists(APP_ROOT.'/includes/'.$file)) $file = APP_ROOT.'/includes/'.$file;		
		else throw new TPLEXException(TPLEXException::INVALID_INCLUDE);
		
		ob_start();
		include $file;
		return trim(ob_get_clean());
	}
    
    
    /**
     * TPLEX Sub (var) method.
     * 
     * The TPLEX sub/var replace method allowing 
     * for easy replacement of a syntax with output.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.10
     * 
     * @param string $key variable to replace
     * @param string $val value to replace variable with
     * @return void
     */
	public function __set($key, $val) {
        if($key == 'tpl' || $key == 'syntax') { parent::__set($key, $val); return; }
        $this->tpl = preg_replace("/".$this->syntax['start']."\.sub\:\{".$key."\}".$this->syntax['end']."/", $val, $this->tpl);
    }
}


/**
 * TPLEX Exception Class.
 * 
 * The custom exception class used for TPLEX
 * exceptions.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.10
 * @package EternityX\TPLEX
 */
class TPLEXException extends Exception {
	
    const INVALID_CONST = 1;
    const INVALID_INCLUDE = 2;
    const INVALID_TEMPLATE = 3;
    
    /**
     * The exception category to use.
     * 
     * @var int 
     */
    public $category = 4;
    
    /**
     * List of exceptions.
     * 
     * @var array
     */
	protected $exceptions = array(
		1 => 'Constant not present',
		2 => 'Include file does not exist',
        3 => 'Invalid template'
	);
	
}
