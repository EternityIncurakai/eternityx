<?php
/* 
 * Streams Package
 * 
 * File containing elements of the stream package:
 * it's main class and corresponding exception class.
 * Allows for easy use to create and access custom 
 * stream wrappers (ie eternityx://)
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.1.0α1
 * @package EternityX\Streams
 */


namespace EternityX;

/**
 * The core streams class.
 * 
 * This is the core class for accessing
 * and creating custom stream wrappers for PHP
 * and easy use.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.1.0α1
 * @package EternityX\Streams
 */
class Stream {
    
    /**
     * Protocol to use
     * 
     * @var string 
     */
    public $protocol = 'eternityx://';
    
    /**
     * Access URL's hostname (example.com)
     * 
     * @var string
     */
    protected $host;
    
    /**
     * Access URL's endpoint (/test)
     * 
     * @var string
     */
    protected $endpoint;
    
    /**
     * Access URL's Parameters (?test=test&test2=test)
     * 
     * @var string
     */
    protected $params;
    
    /**
     * Pointer to position of output.
     * 
     * @var int
     */
    protected $position = 0;
    
    /**
     * Route type to file_get_contents();
     */
    const ROUTE_FGC = 1;
    
    /**
     * Route type to static delegate (Theme::exists)
     */
    const ROUTE_DELEGATE = 2;
    
    /**
     * Route type to cached variable ($GLOBALS['theme']->getManifest())
     */
    const ROUTE_VAR = 3;
    
    /**
     * List of routes to use.
     * 
     * @var array 
     */
    protected $routes = [
        'themes' => [
            'type' => self::ROUTE_FGC,
            'route' => 'http://eternityx.eternityincurakai.com/themes'
        ],
        
        'theme' => [
            'routes' => [
                'manifest' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getManifest']
                ],
                
                'icon' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'icon']
                ],
                
                'template' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getTemplate']
                ]
            ]
        ]
    ];
    
    /**
     * The currently used route
     * 
     * @var string
     */
    protected $route;
    
    /**
     * Opens the stream
     * 
     * This initiates the "transfer" and opens the stream.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @param string $path the path that was opened
     * @param string $mode the mode to open the path in
     * @param array $options options to use when opening the path (currently unused)
     * @param string $opened_path idk what this is
     * @return boolean always true
     */
    public function stream_open($path, $mode, $options, &$opened_path) {
        $path = strtok($path, '?');
        $this->params = strtok('?');
        $path = str_replace($this->protocol, '', $path);
        if(strpos($path, '/') != null) {
            $this->host = substr($path, 0, strpos($path, '/'));
            $this->endpoint = str_replace($this->host.'/', '', $path);
        } else {
            $this->host = $path;
            $this->endpoint = '';
        }
        $this->position = 0;
        
        $this->route = $this->routes[$this->host];
        if(array_key_exists('routes', $this->route)) $this->route = $this->route['routes'][$this->endpoint];
        return true;
    }
    
    /**
     * Reads data from the stream.
     * 
     * This function reads data from the stream
     * based on how many bytes were specified.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @param int $count how many bytes to return
     * @return string
     */
    public function stream_read($count) {
        $this->return = substr($this->route(), $this->position, $count);
        $this->position = strlen($this->return);
        return $this->return;
    }
    
    /**
     * Routes to data.
     * 
     * This sets up the correct route to use
     * and returns data accordingly.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return string
     */
    public function route() {
        switch($this->route['type']) {
            case self::ROUTE_FGC :
                return file_get_contents($this->route['route'].'/'.$this->endpoint.$this->params);
                
            case self::ROUTE_DELEGATE :
                parse_str($this->params, $params);
                $return = call_user_func_array($this->route['route'], $params);
                if(is_array($return)) $return = json_encode($return);
                return (string)$return;
                
            case self::ROUTE_VAR :
                parse_str($this->params, $params);
                $var = $GLOBALS[$this->route['route'][0]];
                $return = call_user_func_array(array($var, $this->route['route'][1]), $params);
                if(is_array($return)) $return = json_encode($return);
                return (string)$return;
        }
    }
    
    /**
     * Tells the current position.
     * 
     * This function tells the current position of 
     * the pointer on the data.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return int
     */
    public function stream_tell() {
        return $this->position;
    }
    
    /**
     * I honestly don't know what this is for.
     * 
     * IDK what its for but apparently its necessary.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return array
     */
    public function url_stat() {
        return array();
    }
    
    /**
     * I honestly dont know what this is for either.
     * 
     * IDK what its for but apparently necessary.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return array
     */
    public function stream_stat() {
        return array();
    }
    
    /**
     * Determines whether we are at the end of file.
     * 
     * Determines whether the pointer is at the end of
     * the returned data or not.
     * 
     * @author XenoK <xenok@eternityincurakai.com
     * @since v2.1.0α1
     * 
     * @return boolean
     */
    public function stream_eof() {
        return ($this->position >= strlen($this->return));
    }
}
