<?php
/**
 * @package unfinished
 * @todo document
 * @todo add stream
 * @todo add exception class
 */

namespace EternityX;

// workaround cases
use \PDO;
use \Imagick;
use \SplFileObject;

/**
 * The core User class.
 * 
 * The core user class allowing for
 * logging in, signing up, and management
 * of users, as well as much more, built into
 * EternityX.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.0
 * @package EternityX\User
 */
final class User {
	public $id;
    public $loggedin;
    private $data;
    
	private $db;	
	private $metatables = array(
		'default' 	=> 'SELECT * FROM `users_meta` WHERE `id`=?',
		'email' 	=> 'SELECT * FROM `users_meta-email` WHERE `id`=?',
		'password'	=> 'SELECT * FROM `users_meta-password` WHERE `id`=?'
	);
    
    static private $metacols = [
        'code' => 'users_meta-email',
        'codeGenerated' => 'users_meta-email'
    ];
	
	// user ranks
	const RANK_NEW = 1; 	// New Incurakai 
	const RANK_USER = 2;  	// Incurakai
	const RANK_MOD = 3;   	// Sahoja [Incurakai]
	const RANK_ADMIN = 4;   // Riathor [Incurakai]
    const RANK_SUPER = 5;
    
	static private $ranks = array(
		1 => 'New Incurakai', 
		2 => 'Incurakai',
		3 => 'Society Moderator',
		4 => 'Staff Member',
		5 => 'Superuser'
		
	);
	
	
	// avatar configs
    static private $avatarReqs = [
        'exts' => ['png', 'jpg', 'bmp', 'jpeg', 'gif'],
        'types' => ['image/png', 'image/jpg', 'image/bmp', 'image/jpeg', 'image/gif', 'image/pjpeg', 'image/x-png'],
    ];
	
	
	static private function getMetatable($key) {
        if(array_key_exists($key, self::$metacols)) return self::$metacols[$key];
        else return 'user_meta';
    }
    
	public function __construct($pdo, $id) {
        if(!is_numeric((int)$id)) is_type_error(__FUNCTION__, $id, 2, 'int');
		$this->db = $GLOBALS['db'];
		$this->id = $id;
		
        $data = $this->db->prepare("SELECT * FROM `users` JOIN `users_meta` USING (`id`) JOIN `users_meta-email` USING (`id`) WHERE `users`.`id`=?");
        $data->bindValue(1, $this->id);
        $data->execute();
        $this->data = $data->fetch(PDO::FETCH_OBJ);
        if($this->data) $this->loggedin = true;
	}
	
    public function __isset($key) {
        return isset($this->data->{$key});
    }
    
    public function __unset($key) {
        if(isset($this->data->{$key})) $this->{$key} = '';
    }
    
    public function __get($key) {
        switch($key) {
            case 'rank' :
                return self::$ranks[$this->permissions];
            
            case 'avatar' :
                if($this->data->avatar == '') return file_get_contents('http://www.newportoak.com/wp-content/uploads/default-avatar.jpg');
                else return $this->data->avatar;
            
            case 'ips' :
                $list = $this->db->prepare("SELECT `address` FROM `users_ips` WHERE `userID`=?");
                $list->bindValue(1, $this->id);
                $list->execute();
                return $list->fetch(PDO::FETCH_NUM);
             
            case 'avatarhtml' :
                return '<img src="'.EX_URL.'/api/v2/user/'.$this->id.'/avatar" alt="'.$this->username.'\'s Avatar">';
                
            default :
                if(isset($this->data->{$key})) return $this->data->{$key};
                break;
        } return;
    }
    
    public function __set($key, $value) {
        switch($key) {
            case 'permissions' :
                $this->permissions = $value;
                break;
            
            case 'id' :
                break;
            
            default :
                $update = $this->db->prepare("UPDATE `".self::getMetatable($key)."` SET `".$key."`=? WHERE `id`=?");
                $update->bindValue(1, $value);
                $update->bindValue(2, $this->id);
                $update->execute();
                $this->data->{$key} = $value;
        }
    }
    
    public function __toString() {
        ob_start(); ?>
        <a class="exw exw-user">
            <img src="<?=EX_URL;?>/api/v2/user/<?=$this->id;?>/avatar" alt="<?= $this->username ?>'s Avatar" />
            <span class="name"><?= $this->username; ?></span>
        </a>
<?php      return trim(ob_get_clean());
    }
    
    public function isRank($rank = self::RANK_USER) {
        if(!is_numeric($rank)) is_type_error (__FUNCTION__, 'rank', 1);
        return ($user->permissions >= $rank);
    }
    
    
    public function demote() {
        if($this->permissions <= 1) return false;
        $update = $this->db->prepare("UPDATE `users` SET `permissions`=? WHERE `id`=?");
        $update->bindValue(1, ($this->permissions - 1));
        $update->bindValue(2, $this->id);
        $update->execute();
        $this->permissions--;
    }

    public function promote() {
        if($this->permissions >= 5) return false;
        $update = $this->db->prepare("UPDATE `users` SET `permissions`=? WHERE `id`=?");
        $update->bindValue(1, ($this->permissions + 1));
        $update->bindValue(2, $this->id);
        $update->execute();
        $this->permissions++;
    }
    
    public function hasRelationTo($user) {
        if(is_numeric($user)) $id = $user;
        else if($user instanceof User) $id = $user->id;
        
        $rel = $this->db->prepare("SELECT COUNT(`id`) FROM `users_friends` WHERE ((`requesterID`=? AND `friendID`=?) OR (`requesterID`=? AND `friendID`=?)) AND `confirmed`='1'");
        $rel->bindValue(1, $this->id);
        $rel->bindValue(2, $id);
        $rel->bindValue(3, $id);
        $rel->bindValue(4, $this->id);
        $rel->execute();
        
        return (bool)$rel->fetchColumn();
    }
    
    public function wantsRelationTo($user) {
        if(is_numeric($user)) $id = $user;
        else if($user instanceof User) $id = $user->id;
        
        $rel = $this->db->prepare("SELECT COUNT(`id`) FROM `users_friends` WHERE ((`requesterID`=? AND `friendID`=?) OR (`requesterID`=? AND `friendID`=?)) AND `confirmed`='0'");
        $rel->bindValue(1, $this->id);
        $rel->bindValue(2, $id);
        $rel->bindValue(3, $id);
        $rel->bindValue(4, $this->id);
        $rel->execute();
        
        return (bool)$rel->fetchColumn();
    }
    
    public function getRelationTo($user, $fetch = PDO::FETCH_ASSOC) {
        if(!$this->hasRelationTo($user)) return false;
        
        if(is_numeric($user)) $id = $user;
        else if($user instanceof User) $id = $user->id;
        
        $rel = $this->db->prepare("SELECT * FROM `users_friends` WHERE ((`requesterID`=? AND `friendID`=?) OR (`requesterID`=? AND `friendID`=?)) AND `confirmed`='1' LIMIT 1");
        $rel->bindValue(1, $this->id);
        $rel->bindValue(2, $id);
        $rel->bindValue(3, $id);
        $rel->bindValue(4, $this->id);
        $rel->execute();
        
        return $rel->fetch($fetch);
    }
    
    public function deleteRelationTo($user) {
        if(!$this->hasRelationTo($user) && !$this->wantsRelationTo($user)) return false;
        $id = $this->getRelationTo($user)['id'];
        
        $rel = $this->db->prepare("DELETE FROM `users_friends` WHERE `id`=?");
        $rel->bindValue(1, $id);
        $rel->execute();
    }
    
    /**
     * 
     * @param \EternityX\User $user
     * @return boolean
     * @todo add messaging
     */
    public function createRelationTo($user) {
        if($this->hasRelationTo($user)) return false;
        
        if(is_numeric($user)) $id = $user;
        else if($user instanceof User) $id = $user->id;
        
        $cre = $this->db->prepare("INSERT INTO `users_friends` (`requesterID`, `friendID`, `confirmed`) VALUES (?,?,?)");
        $cre->bindValue(1, $this->id);
        $cre->bindValue(2, $id);
        $cre->bindValue(3, false);
        $cre->execute();
    }
    
    public function getRelations($type = PDO::FETCH_ASSOC) {
        $rels = $this->db->prepare("SELECT `users`.`username`, `users`.`id`, `users_friends`.`confirmed` FROM `users_friends` JOIN `users` ON (`users`.`id`=`users_friends`.`friendID` AND `users`.`id`!=?) OR (`users`.`id`=`users_friends`.`requesterID` AND `users`.`id`!=?) WHERE `users_friends`.`requesterID`=? OR `users_friends`.`friendID`=?");
        $rels->bindValue(1, $this->id);
        $rels->bindValue(2, $this->id);
        $rels->bindValue(3, $this->id);
        $rels->bindValue(4, $this->id);
        $rels->execute();
        
        return $rels->fetch($type);
    }
    
    
    public function uploadAvatar() {
        $avatar = $_FILES['avatar'];
        $ext = pathinfo($avatar['name'], PATHINFO_EXTENSION);
        $type = strtolower($avatar['type']);
        $size = $avatar['size'];
        
        if(!in_array($ext, self::$avatarReqs['exts'])) return false;
        if(!in_array($type, self::$avatarReqs['types'])) return false;
        
        switch($type) {
            case 'image/jpg' :
            case 'image/jpeg' :
                $img = imagecreatefromjpeg($avatar['tmp_name']);
                break;
                
            case 'image/png' :
                $img = imagecreatefrompng($avatar['tmp_name']);
                imagealphablending($img, true);
                imagesavealpha($img, true);
                break;
            
            case 'image/gif' :
                $img = imagecreatefromgif($avatar['tmp_name']);
                break;
            
            case 'image/bmp' :
                $img = imagecreatefrombmp($avatar['tmp_name']);
                break;
        }
        
        ob_start();
        imagepng($img, null, 9);
        $this->avatar = ob_get_clean();
        unlink($_FILES['tmp_name']);
    }
    
    /**
     * @todo revamp
     * @todo add IP logging
     * @todo incorporate SQL procedure
     * 
     * @param type $username
     * @param type $password
     * @return boolean
     * @throws UserException
     */
    static public function login($username, $password) {
        $usernameLen = strlen($username);
        $passwordLen = strlen($password);

        // validate lengths
        if(!in_range($username, 5, 15)) throw new UserException(1,1);
        if(!in_range($password, 8, 18)) throw new UserException(1,2);

        // validate contents
        if(!ctype_alnum($username)) throw new UserException(1,3);
        if(!self::passwordCriteria($password)) throw new UserException(1,4);

        // fetch actual password
        $creds = $GLOBALS['db']->prepare("SELECT `password`,`id`,`permissions` FROM `users` WHERE `username`=?");
        $creds->bindValue(1, $username);
        $creds->execute();
        $creds->bindColumn(1, $opassword);
        $creds->bindColumn(2, $oid);
        $creds->bindColumn(3, $opermissions);
        $creds->fetchAll();

        if(strlen($opassword) == 128) { 
            $salt = sha1($username.$oid.substr(sha1($password),12).$oid);
            $password = hash('sha512', hash('sha512',$salt.$password).'dAtuhex#2!n4d0093aa4%%^');
            if($password == $opassword) {
               // update to new version
                
                $password = password_hash($password, PASSWORD_DEFAULT, [ 'cost' => 10 ]);
                $update = $GLOBALS['db']->prepare("UPDATE `users` SET `password`=? WHERE `username`=?");
                $update->bindValue(1, $password);
                $update->bindValue(2, $username);
                $update->execute();

               // login
               session_regenerate_id(true); // security purposes 
               $_SESSION['id'] = $oid;
               $_SESSION['username'] = $username;
               $_SESSION['permissions'] = $opermissions;
               session_write_close();
               return true;
            } else {
                return false;
            }
        } else {
            if(password_verify($password, $opassword)) {
               session_regenerate_id(true); // security purposes 
               $_SESSION['id'] = $oid;
               $_SESSION['username'] = $username;
               $_SESSION['permissions'] = $opermissions;
               session_write_close();
               return true;
            } else {
                return $opassword;
            }
        }
    }
    
    static public function getList($list = 'username', $type = PDO::FETCH_ASSOC) {
        $list = $GLOBALS['db']->prepare("SELECT `".$list."` FROM `users`");
        $list->execute();
        return $list->fetch($type);
    }
    
    
    /**
     * User Signup API
     * 
     * Allows for the creation of new users
     * and validated in an elegant manner.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * @todo add captcha validation
     * @todo add mailing & token validation
     * 
     * @param string $username unique identifier for the user
     * @param string $password login credential
     * @param string $rpassword repeated login credential
     * @param string $email communication access point
     * @param string $age age of the user
     * @param string $location location of the user
     * @param string $captcha to be added
     * @return boolean
     */
    static public function signup($username, $password, $rpassword, $email, $age, $location, $captcha) {
        
        // validate username
        if(!String::lengthInRange($username, 5, 15)) throw new UserException(UserException::SIGNUP_UNAME_LENGTH);
        if(!ctype_alnum($username)) throw new UserException(UserException::SIGNUP_UNAME_ALNUM);
        if(in_array($username, self::getList())) throw new UserException(UserException::SIGNUP_UNAME_AVAIL);
        
        // validate password
        if(!String::lengthInRange($password, 8, 18)) throw new UserException(UserException::SIGNUP_PASS_LENGTH);
        if(!String::hasUppercase($password)) throw new UserException(UserException::SIGNUP_PASS_UPPER);
        if(!String::hasLowercase($password)) throw new UserException(UserException::SIGNUP_PASS_LOWER);
        if(!String::hasInteger($password)) throw new UserException(UserException::SIGNUP_PASS_INT);
        if(!String::hasSpecial($password)) throw new UserException(UserException::SIGNUP_PASS_SPECIAL);
        if($password != $rpassword) throw new UserException(UserException::SIGNUP_PASS_MATCH);
        
        // validate email
        if(!String::isEmail($email)) throw new UserException(UserException::SIGNUP_EMAIL_VALID);
        if(in_array($email, self::getList('email'))) throw new UserException(UserException::SIGNUP_EMAIL_AVAIL);
        
        // validate age
        if(strtotime($age) > strtotime("-13 years")) throw new UserException(UserException::SIGNUP_AGE_LIMIT);
        
        // attempt to insert
        $insert = $GLOBALS['db']->prepare("CALL signup(?,?,?,?,?,?)");
        $insert->bindValue(1, $username);
        $insert->bindValue(2, password_hash($password, PASSWORD_DEFAULT));
        $insert->bindValue(3, $email);
        $insert->bindValue(4, $age);
        $insert->bindValue(5, $location);
        $insert->bindValue(6, hash('sha512', uniqid()));
        $insert->execute();
        
        return $insert;
    }
    
    public function validateToken($token) {
        $valid = ($this->code == $token && strtotime($this->codeGenerated) >= strtotime("-1 hours"));
        if($valid) {
            $this->code = '';
            $this->codeGenerated = '';
        }
        return $valid;
    }
    
    static function logout() {
       setcookie(session_id(), "", time() - 60 * 60 * 24 * 7);
       session_unset();
       session_destroy();
       session_write_close();
       return true;
    }
         
    static private function passwordCriteria($password) {
        if(!preg_match('/([a-z]+)/', $password)) return false;
        if(!preg_match('/([A-Z]+)/', $password)) return false;
        if(!preg_match('/([0-9]+)/', $password)) return false;
        if(!preg_match('/([^a-zA-Z\d])/', $password)) return false;
        return true;
    }
}


class UserException extends Exception {
    const SIGNUP_UNAME_LENGTH = 1;
    const SIGNUP_UNAME_ALNUM = 2;
    const SIGNUP_UNAME_AVAIL = 3;
    
    const SIGNUP_PASS_LENGTH = 4;
    const SIGNUP_PASS_UPPER = 5;
    const SIGNUP_PASS_LOWER = 6;
    const SIGNUP_PASS_INT = 7;
    const SIGNUP_PASS_SPECIAL = 8;
    const SIGNUP_PASS_MATCH = 9;
    
    const SIGNUP_EMAIL_VALID = 10;
    const SIGNUP_EMAIL_AVAIL = 11;
    
    const SIGNUP_AGE_LIMIT = 12;
}