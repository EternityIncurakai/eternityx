<?php
/**
 * Page Class
 * @todo document
 */

namespace EternityX;

class Page {
	
	private $tpl;
	private $db;
	private $user;
	private $theme;
	private $title;
	
	public function __construct($title) {
		$this->db = $GLOBALS['db'];
		$this->user = $GLOBALS['user'];
		$this->title = $title;
	}
	
	public function setTheme(Theme $theme) {
		$this->theme = $theme;
		$this->tpl = new TPLEX($this->theme->getTemplate());
        $this->tpl->title = $this->title;
	}
	
	public function setContent($content) {
		if(!$this->theme) throw new \Exception('Please set theme first.');
		$this->tpl->content = $content;
	}
	
	public function setAssets($assets) {
		if(!$this->theme) throw new \Exception('Please set theme first.');
		$this->tpl->assets = $assets;
	}
	
	public function setMetadata($metadata) {
		if(!$this->theme) throw new \Exception('Please set theme first.');
		$this->tpl->meta = $metadata;
	}
	
	private function setInterface() {
		// fixes
		$pdo = $this->db;
		$user = $this->user;
		
		if(!$this->theme) throw new \Exception('Please set theme first.');
	
        // substitute cdnjs
        ob_start();
        $cdnjsdeps = parse_ini_file(ENV_ROOT.'/configs/dependencies.ini');
        foreach($cdnjsdeps['cdnjs'] as $cdnjs) { echo "<script defer src=\"//cdnjs.cloudflare.com/ajax/libs/".$cdnjs."\"></script>".PHP_EOL; }
        $this->tpl->cdnjsdeps = trim(ob_get_clean());
        
		$this->tpl->baseurl = EX_URL;
		$this->tpl->hlogo = EX_URL.'/'.EX_LOGO_ALT;
		$this->tpl->hlogotxt = EX_TITLE;
		$this->tpl->logo = EX_URL.EX_LOGO;
		$this->tpl->logotxt = EX_TITLE;
		$this->tpl->copyright =  '&copy; Copyright ' . date('Y') . ' ' . EX_CREATOR . ', All Rights Reserved';
		
		
		// substitute admin links
		ob_start();
		if($this->user->permissions >= 3 && file_exists(APP_ROOT.'/includes/adminlinks.php')) include APP_ROOT.'/includes/adminlinks.php'; 
		$this->tpl->adminlinks = trim(ob_get_clean());
	}
	
	public function __toString() {
		global $dispatcher;
		
		if(!$this->theme) throw new \Exception('Please set theme first.');
		
        
		if(isset($_GET['_body'])) {
			header('Content-Type: text/plain');
			$this->tpl = preg_replace("%<\!DOCTYPE html>\\n<html lang=\"en\">\\n<head>(.*)<\/head>\\n<body>(.*)<\/body>\\n<\/html>%ms", '$2', $this->tpl);
			$this->tpl = preg_replace("%<script (.*)>(.*)<\/script>%ms", '', $this->tpl);
			die($this->tpl);
		}
		if($dispatcher->banned) $this->tpl = str_replace('<html lang="en">', '<html class="banned" lang="en">', $this->tpl);
		if(EX_MAINTENANCE) $this->tpl = str_replace('<html lang="en">', '<html class="maintenance" lang="en">', $this->tpl);
		$this->setInterface();
        
		return $this->tpl->__toString();
	}
}