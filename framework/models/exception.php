<?php
/**
 * Exception Package.
 * @todo document
 */

namespace EternityX;

abstract class Exception extends \Exception {
    public $code;
    public $message;
    public $category = 0;
    protected $exceptions = [];
    
    public function __construct($code) {
        if(!is_integer($code)) is_type_error(__FUNCTION__, $code, 1, 'int');
        
		// generate code
		$zeroes = 4 - strlen((string)$code);
		$this->code = 'exerr#'.$this->category.'.';
		for ($i=0; $i < $zeroes; $i++) $this->code .= '0';  
		
		$this->message = $this->exceptions[$code];
		parent::__construct($this->message, $code, null);
	}
}