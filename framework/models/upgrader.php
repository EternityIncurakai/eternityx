<?php

/* 
 * Short Description.
 *
 * Long Description
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version 2.1.0
 * @package EternityX
 */

namespace EternityX;

use \ZipArchive;

class Upgrader {
    use Traits\tProgress;
    use Traits\tCache;
    
    private $endpoint = 'http://upgrade.eternityincurakai.info/core';
    private $tmppoint = "/ex.zip";
    private $name;
    private $size = 0;
    private $progress = 0;
    private $version;
    
    public function __construct() {
        try {
            ignore_user_abort();
            set_time_limit(0);
            $this->cacheAddFile(ENV_ROOT.'/models/helpers/templates/upgrade/fail.tpl');
            $this->cacheAddFile(ENV_ROOT.'/models/helpers/templates/upgrade/success.tpl');
            
            // preparations
            $this->addStep(function() {
                if(!$this->prepare()) throw new \Exception;
            }, 'Downloading...');
            
            // download
            $this->addStep(function() {
                if(!$this->download()) throw new \Exception;
            }, 'Checking...');
            
            $this->addStep(function() {
                test_error("exu_check", function() { throw new \Exception; });
                if(!$this->writable() || !$this->readable() || (!disk_free_space('/') || disk_free_space('/') < $this->size)) {
                    throw new \Exception;
                }
            }, 'Deleting Files...');
            
            
            $this->addStep(function() {
                if(!$this->delete()) throw new \Exception;
            }, 'Extracting new files....');
            
            $this->addStep(function() {
                if(!$this->extract()) throw new \Exception;
            }, 'Putting on final touches');
            
            $this->addStep(function() {
                test_error("exu_final", function() { throw new \Exception; });
                include ENV_ROOT.'/upgrader.php';
                self::changedb();
                foreach(glob(ENV_ROOT.'/scripts/*.php') as $cron) addCron('0 0 * * *', $cron);
                if(file_exists(ENV_ROOT.'/data/temp/newdirs.csv')) {
                    foreach(parse_csv_file(ENV_ROOT.'/data/temp/newdirs.csv') as $dir) { if(!is_dir($dir)) mkdir($dir); }
                }
                
                $mail = new Mail(EX_WEBMASTER_EMAIL);
                $mail->subject = 'EternityX Upgraded!';
                $message = new TPLEX($this->cacheGetFile(ENV_ROOT.'/models/helpers/templates/upgrade/success.tpl'));
                $message->version = $this->version;
                $mail->message = $message;
                $mail->addHeader('Content-Type: text/html');
                $mail();
            }, 'Done', 5);
            
            $this->walk();
            
            
        } catch (\Exception $e) {
            
            file_put_contents(EX_ROOT.'/.progress', json_encode([
                'progress' => $this->progress,
                'message' => 'Uh oh...'
            ]));
            sleep(3);
            
            file_put_contents(EX_ROOT.'/.progress', json_encode([
                'progress' => $this->progress,
                'message' => 'Something went wrong :/',
                'debug' => $e->getMessage()
            ]));
            sleep(3);
            
            $mail = new Mail(EX_WEBMASTER_EMAIL);
            $mail->subject = 'EternityX Upgrade Error';
            $mail->message = new TPLEX($this->cacheGetFile(ENV_ROOT."/models/helpers/templates/upgrade/fail.tpl"));
            $mail->addHeader('Content-Type: text/html');
            $mail();
            
            send_stats(['file' => __FIlE__, 'line' => __LINE__, 'message' => $e->getMessage(), 'type' => 'Upgrade Error']);
        }
        unlink(EX_ROOT.'/.upgradelock');
        unlink(EX_ROOT.'/.progress');
    }
    
    public function extract() {
        test_error("exu_extract", function() { throw new \Exception; });
        
        $zip = new ZipArchive;
        $zip->open(EX_ROOT.$this->tmppoint);
        $ext = $zip->extractTo(EX_ROOT);
        $zip->close();
        $un = unlink(EX_ROOT.$this->tmppoint);
        
        return ($ext && $un);
    }
    
    public function prepare() {
        test_error("exu_prepare", function() { throw new \Exception; });
        $this->version = file_get_contents(ENV_ROOT.'/data/temp/upgradeavailable.txt');
        return (
                touch(EX_ROOT.'/.upgradelock')
            &&  file_put_contents(EX_ROOT.'/.upgradelock', file_get_contents(EX_URL.'/upgrade'))
            &&  rename(ENV_ROOT.'/data/.htaccess-upgrade', EX_ROOT.'/.htaccess')
            &&  ((EX_OS != 'Windows' && system('crontab -r')) || true)
        ); 
    }
    
    
    
    public function writable() {
        test_error("exu_writable", function() { throw new \Exception; });
            
        foreach(glob_recursive(EX_ROOT.'/*') as $filename) {
            if(!is_writable($filename)) {
                if(!chmod($filename, 0775))
                    return false;
            }
        }
        return true;
    }
    
    public function readable() {
        foreach(glob_recursive(EX_ROOT.'/*') as $filename) {
            if(!is_readable($filename)) {
                if(!chmod($filename, 0775))
                    return false;
            }
        }
        return true;
    }
    
    public function download() {
        test_error("exu_download", function() { throw new \Exception; });
        
        $copy = copy($this->endpoint, EX_ROOT.$this->tmppoint);
        
        $zip = new ZipArchive;
        $zip->open(EX_ROOT.$this->tmppoint);
        $this->name = $zip->getNameIndex(0);
        
        for($i = 1; $i < $zip->numFiles; $i++) {
            $this->size += $zip->statIndex($i)['size'];
            $name = $zip->getNameIndex($i);
            if(String::beginsWith($name, $this->name.'application') || String::beginsWith($name, $this->name.'customization')) {
                if(!$zip->deleteIndex($i)) throw new \Exception("Couldn't delete ".$name);
            } else {
                if(!$zip->renameIndex($i, substr($name, strlen($this->name)))) throw new \Exception("Couldn't rename ".$name);
            }
        }
        $del = $zip->deleteIndex(0);
        $zip->close();
        
        return ($copy && $del);
    }
    
    public function delete() {
        test_error("exu_delete", function() { throw new \Exception; });
        
        // files
        foreach(glob_recursive(EX_ROOT.'/*') as $file) {
            if(!String::beginsWith($file, EX_ROOT.'/application') && !String::beginsWith($file, EX_ROOT.'/customization') && is_file($file) && $file != EX_ROOT.'/ex.zip')
                if(!unlink($file)) throw new \Exception("Couldn't delete file <".$file.">");
        }
        
        // directories
        foreach(glob(EX_ROOT.'/*', GLOB_ONLYDIR) as $dir) {
            if(is_dir($dir) && !in_array($dir, [EX_ROOT.'/application', EX_ROOT.'/customization'])) 
                if(!rmdir_recursive($dir)) throw new \Exception("Couldn't delete directory <".$dir.">");
        }
        return true;
    }
    
    
   static public function changedb() {
        clearstatcache();
        $changes = parse_change_file(ENV_ROOT.'/data/database.change');
       
        // rename tables
        foreach($changes['TABLES'] as $table) {
            
            switch($table['type']) {
                case 'RENAME' :
                    $GLOBALS['db']->query("RENAME TABLE `".$table['from']."` TO `".$table['to']."`"); 
                    break;
                default: 
                case 'DELETE' :
                    $GLOBALS['db']->query("DROP TABLE `".$table['table']."`");
                    break;
            }
            
        }
        
        
        foreach($changes['COLUMNS'] as $table => $alters) {
            $query = "ALTER TABLE `$table` ";
            foreach($alters as $alter) {
                switch(strtolower($alter['type'])) {
                    case 'add' :
                        $GLOBALS['db']->query($query."ADD COLUMN `".$alter['column']."` ".stripslashes($alter['ctype']));
                        break;
                    case 'rename' :
                        $GLOBALS['db']->query($query."CHANGE COLUMN `".$alter['from']."` `".$alter['to']."` ".$alter['ctype'].";");
                        break;
                    case 'delete' :
                        $GLOBALS['db']->query($query."DROP COLUMN `".$alter['column']."`");
                        break;
                }
            }
            
        }
    }
}