<?php
/**
 * EternityX Mail API
 * 
 * This file houses EternityX's Mail API Package,
 * and its child classes, core, exception, stream,
 * etc.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since 2.1.0α3
 * @package EternityX\Mail
 */


namespace EternityX;

/**
 * The core mail class.
 * 
 * EternityX's core mail class. This is 
 * basically an object oriented wrapper for
 * PHP's mail function.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.1.0α3
 * @package EternityX\Mail
 */
class Mail {
    
    /**
     * Who to send it to
     * 
     * @var string
     */
    private $to = '';
    
    /**
     * list of headers to send
     * 
     * @var string
     */
    private $headers = '';
    
    /**
     * subject of the message
     * 
     * @var string
     */
    public $subject = '';
    
    /**
     * the message body
     * 
     * @var string
     */
    public $message = '';
    
    const FROM_NAME = EX_WEBMASTER_NAME;
    const FROM_EMAIL = EX_WEBMASTER_EMAIL;
    
    /**
     * Constructs a message to be sent trough smtp.
     * 
     * Constructs a new object representing an email 
     * message to be sent over PHP.  
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α3
     * 
     * @param string $to who to send the mail to
     * @param array $from who sent the message
     * @return void
     * @throws MailException
     */
    public function __construct($to, array $from = ['name' => self::FROM_NAME, 'email' => self::FROM_EMAIL]) {
        if(!String::isEmail($to)) throw new MailException(MailException::TO_ERROR);
        if(!String::isEmail($from['email'])) throw new MailException(MailException::FROM_ERROR);
        
        $this->to = $to;
        $this->addHeader("From: {$from['name']} <{$from['email']}>");
    }
    
    /**
     * Sends the message.
     * 
     * invokes the mail function and sends the 
     * finalized message.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α3
     * 
     * @return bool 
     */
    public function __invoke() {
        return mail($this->to, $this->subject, $this->message, $this->headers);
    }
    
    /**
     * Adds a new header to the message.
     * 
     * Adds a header to the message for when it 
     * is sent.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α3
     * 
     * @param string $header the header to add
     * @return void
     */
    public function addHeader($header) {
        $this->headers .= $header . "\r\n";
    }
}
