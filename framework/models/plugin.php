<?php
/**
 * Plugin Package
 * 
 * This file houses the plugin package, the
 * core class, as well as it's exception class.
 * Plugin package allows EternityX to have customized behavior.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.0.9
 * @package EternityX\Plugin
 * @todo finish documentation
 */

namespace EternityX;

/**
 * The core Plugin class.
 * 
 * The core Plugin class allowing for
 * loading of plugins, and customization, 
 * and management.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.9
 * @package EternityX\Plugin
 */
class Plugin {
	
    /**
     * Name of the plugin.
     * 
     * @var string 
     */
	public $name;
    
    /**
     * Manifest information for the plugin.
     * 
     * @var array
     */
	private $manifest = [];
	
	public function __construct($name) {
		if(DEBUG) {
			if(!is_dir(PLUGINS_ROOT.'/'.$name)) throw new PluginException(1, $name);
		} else {
			if(!file_exists(PLUGINS_ROOT.'/'.$name.'.phar')) throw new PluginException(1, $name);
		}
		$this->name = $name;
		$this->load();
	} 
	
	private function load() {
		if(DEBUG) {
			$manifestfile = PLUGINS_ROOT.'/'.$this->name.'/manifest.json';
			$eventstrapfile = PLUGINS_ROOT.'/'.$this->name.'/eventstrap.php';
		} else {
			$manifestfile = 'phar://'.PLUGINS_ROOT.'/'.$this->name.'.phar/manifest.json';
			$eventstrapfile = 'phar://'.PLUGINS_ROOT.'/'.$this->name.'.phar/evenstrap.php';
		}
		
		if(!file_exists($manifestfile)) throw new PluginException(2, $this->name);
		if(!file_exists($eventstrapfile)) throw new PluginException(3, $this->name);
		
		$this->manifest = json_decode(utf8_encode(file_get_contents($manifestfile)));
		if(!$this->manifest) throw new PluginException(4, $this->name);
		
		try {
			include $evenstrapfile;
		} catch(EventException $e) {
			throw new PluginException(5, $this->name);
		}
	}
	
	
	static public function loadAll() {
		foreach(glob(PLUGINS_ROOT.'/*') as $plugin) {
			/** Runtime fix for non-phar plugins in production mode **/
			if(!DEBUG && is_dir($plugin)) {
				$archive = new \Phar($plugin.'.phar');
				foreach(glob($plugin.'/*') as $pluginfile) {
					$pluginfilename = explode('/', $pluginfile);
					$pluginfilename = array_pop($pluginfilename);
					$archive->addFile($pluginfile, $pluginfilename);
					unlink($pluginfile);
				}
				$archive->compress();
				rmdir($plugin);
			}
			
			$plugin = explode('/', $plugin);
			$plugin = array_pop($plugin);
			if(!DEBUG) $plugin = rtrim($plugin, '.phar');
			new Plugin($plugin);
		}
	}
	
}


class PluginException extends \Exception {
	private $exceptions = array(
		1 => 'Plugin does not exist',
		2 => 'Missing Plugin manifest',
		3 => 'Missing Plugin Eventstrapper file',
		4 => 'Invalid Plugin Manifest',
		5 => 'Invalid declaration of Evenstrapper PHP code'
	);
	
	public $code;
	public $message;
	
	public function __construct($code, $name) {
		// generate code
		$zeroes = 4 - strlen((string)$code);
		$this->code = 'exerr#2.';
		for ($i=0; $i < $zeroes; $i++) $this->code .= '0';  
		
		$this->message = $this->exceptions[$code].' on theme "'.$name.'"';
		parent::__construct();
	}
}
