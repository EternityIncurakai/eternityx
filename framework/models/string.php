<?php

/*
 * String package for EternityX.
 * 
 * This file houses the string package
 * for EternityX, providing extended string
 * functionality for PHP.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since 2.1.0α2
 * @package EternityX\Strings
 */

namespace EternityX;

/**
 * The core String class.
 * 
 * The core string class of the EternityX
 * Strings package allowing for extended
 * functionality for strings from php.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since 2.1.0α2
 * @package EternityX\Strings
 */
class String {
    
    /**
     * String beginning displacement checker.
     * 
     * This determines if the beginning characters
     * specified as the needle are present at the 
     * beginning of the haystack string.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @param string $needle the string to search for
     * @return bool
     */
    static public function beginsWith($haystack, $needle) {
        return (substr($haystack, 0, strlen($needle)) == $needle);
    }
    
    /**
     * String end displacement checker.
     * 
     * This determines if the ending characters
     * specified as the needle are present at the
     * beginning of the haystack string.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @param sting $needle the string to search for
     * @return bool
     */
    static public function endsWith($haystack, $needle) {
        if(strlen($needle) == 0) return true; // bug fix
        return (substr($haystack, -strlen($needle)) == $needle);
    }
    
    /**
     * Determines if uppercase character is present.
     * 
     * Determines if an uppercase character is
     * present within the haystack string.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @return bool
     */
    static public function hasUppercase($haystack) {
        return (bool)preg_match('/([A-Z]+)/', $haystack);
    } 
    
    /**
     * Determines if lowercase character is present.
     * 
     * Determines if a lowercase character is
     * present within the haystack string.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @return bool
     */
    static public function hasLowercase($haystack) {
        return (bool)preg_match('/([a-z]+)/', $haystack);
    }
    
    /**
     * Determines whether an integer is present.
     * 
     * Determines whether or not an integer is
     * present within the haystack string.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @return bool
     */
    static public function hasInteger($haystack) {
        return (bool)preg_match('/([0-9]+)/', $haystack);
    }
    
    /**
     * Determines whether a special character is present.
     * 
     * Determines whether or not a special character
     * resides within the haystack string.  Special 
     * characters include !,$,#, etc. 
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $haystack the string to search in
     * @return bool
     */
    static public function hasSpecial($haystack) {
        return (bool)preg_match('/([^a-zA-Z\d])/', $haystack);
    }
    
    
    /**
     * Determines whether or not string length is in range.
     * 
     * Determines whether or not the length of
     * a string is within the specified range.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $string the string to use
     * @param int $min the minimum amount of chars
     * @param int $max the maximum amount of chars
     * @return bool
     */
    static public function lengthInRange($string, $min, $max) {
        return (strlen($string) <= $max && strlen($string) >= $min);
    }
    
    /**
     * Checks if string is a version.
     * 
     * This function determines whether or not
     * the presented string is a EternityX Standardized
     * version.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $string string to check against
     * @return bool
     */
    static public function isVersion($string) {
        // \b delimeters force exact match
        return (bool)preg_match('/\b([0-9])\.([0-9])\.([0-9])((α|ß|Γ)[0-9])?\b/', $string);
    }
    
    /**
     * Determines if string is a date.
     * 
     * Determines whether or not the provided
     * string is in date format. If strict mode is 
     * enabled, it checks against EternityX standard
     * dates - this is the default functionality.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $string the string to check against
     * @param bool $strict whether or not to use strict mode
     * @return bool
     */
    static public function isDate($string, $strict = true) {
        if($strict) return (bool)preg_match('/\b([0-9])?([0-9])\/([0-9])?([0-9])\/([0-9])([0-9])([0-9])([0-9])( ([0-9])?([0-9])\:([0-9])([0-9]) (AM|PM)?)?\b/', $string);
        else return (strtotime($string)) ? true : false;
    }
    
    /**
     * Determines if string is URL.
     * 
     * Determines whether or not the specified
     * string is in URL format or not.
     * 
     * @author XenoK <xenok@eternityinurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $string the string to check against
     * @return bool
     */
    static public function isURL($string) {
        return (bool)filter_var($string, FILTER_VALIDATE_URL);
    }
    
    /**
     * Checks if string is an email.
     * 
     * This function checks whether or not a string
     * is in email format, and then validates whether
     * or not it exists.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α2
     * 
     * @param string $string string to validate against
     * @return bool
     */
    static public function isEmail($string) {
        $record = explode("@", $string);
        $record = array_pop($record);
        return (bool)(filter_var($string, FILTER_VALIDATE_EMAIL) && checkdnsrr($record,"MX"));
    }
    
    
}

