<?php

/**
 * Dispatcher Class
 * @todo document
 * @todo cleanup
 */

namespace EternityX;

class Dispatcher {
	
	// Request Types
	const REQUEST_PAGES = 1;
	const REQUEST_ASSETS = 2;
	
	// Dynamics
	const DYNAMIC_INDEX = 3;
	const DYNAMIC_VIEW = 4;
    
    // authorizations
    const AUTH_GUEST = 0;
    const AUTH_NEW = 1;
    const AUTH_USER = 2;
    const AUTH_MOD = 3;
    const AUTH_ADMIN = 4;
    const AUTH_SUPER = 5;
	
	private $exts = [
		'css' => [ // Cascading StyleSheets 
			'cache' => false,
			'mimetype' => 'text/css'
		],
		'js' => [ // JavaScript
			'cache' => false,
			'mimetype' => 'text/javascript'
		],
		'png' => [ // PNG images
			'cache' => true,
			'mimetype' => 'image/png'
		],
		'jpg' => [ // JPG Images
			'cache' => true,
			'mimetype' => 'image/jpeg'
		],
		'jpeg' => [ // JPEG Images
			'cache' => true,
			'mimetype' => 'image/jpeg'
		],
		'ttf' => [ // TrueType Font
			'cache' => true,
			'mimetype' => 'font/ttf'
		],
		'woff' => [ // Woff Font
			'cache' => true,
			'mimetype' => 'application/font-woff'
		],
		'gif' => [ // Gif Images
			'cache' => true,
			'mimetype' => 'image/gif'
		],
		'tpl' => [
			'cache' => false,
			'mimetype' => 'text/template'
		],
		'xml' => [
			'cache' => false,
			'mimetype' => 'text/xml'
		],
        'html' => [
            'cache' => false,
            'mimetype' => 'text/html'
        ]
	];
	
	private $imports = ['classes', 'functions', 'traits', 'interfaces', 'startups'];
	public $request;
	private $user;
        private $controllers;
        private $ajax;
	private $apis;
	private $rest = false;
	public $banned;	
	private $dynamics;
	public $db;
	
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->getRequest();
		$this->headers();
        $this->setupStreams();
	}
	
    public function setType() {
        header('Content-Type: '.$this->exts[$this->request->ext]['mimetype']);
    }
	
	/**
	 * Checks for environment requirements
	 */
	public function requirements() {
		if(version_compare(PHP_VERSION, EX_REQPHPVERSION, '<')) die('<b>EXERR#0.0000:</b> Requirements not met');
		foreach(explode(',', EX_REQPHPMODULES) as $module) 
			if(!extension_loaded($module)) die('<b>EXERR#0.0000:</b> Requirements not met');
		
	}
	
    public function setupStreams() {
        stream_wrapper_register('eternityx', '\EternityX\Stream');
        $wrappers = [];
        foreach(get_declared_classes() as $class) {
            if(preg_match('/([a-zA-Z]+)Stream/', $class)) {
                $vars = get_class_vars($class);
                stream_wrapper_register(str_replace('://', '', $vars['protocol']), $class);
                $wrappers[] = $vars['protocol'];
            }
        }
        $GLOBALS['wrappers'] = $wrappers;
    }
    
    
	public function setCache($cache) {
		header_remove('Cache-Control');
		header_remove('Pragma');
		header_remove('Expires');
		
		if($cache) {	
			header('Cache-Control: public');
			header('Pragma: cache');
			header('Expires: Thu, 1 Jan 2015 14:19:41 GMT');
		} else {
			header('Expires: Fri, 30 Oct 1998 14:19:41 GMT');
			header('Cache-Control: no-cache');
			header('Pragma: no-cache');
		}
	}
	
	public function getCScripts() {
		return $this->cscripts;
	}
	
	public function deprecatedActions() {
		//if(!DEBUG) include $main;
		//include APP_ROOT.'/app_resources/deprecated/constants.php';
		foreach (glob(APP_ROOT . "/app_config/*.php") as $filename) include $filename;
	}
	
	private function headers() {
		header_remove('X-Powered-By');
		header('Powered-By: EternityX');
		header('EternityX-Version: ');
	}
	
	/**
	 * Sets up dynamic files
	 */
	public function setDynamics(array $dynamics) {
		$this->dynamics = $dynamics;
	}
	
	/**
	 * Sets up where imports are
	 */
	public function setImport($import, $value) {
		$this->imports[$import] = $value;
	}
	
	/** 
	 * Performs an Import
	 */
	public function import($import) {
		foreach(glob($this->imports[$import].'/*.php') as $filename) include $filename;
	}
	
	/**
	 * Sets up configurations
	 */
	public function setConfigs($configfile) {
		if(!file_exists($configfile)) return false;
		$configs = parse_ini_file($configfile);
		
		foreach($configs as $key => $value) define('EX_'.strtoupper($key), $value);
		if(defined('EX_TIMEZONE')) date_default_timezone_set(EX_TIMEZONE);
	}
	
	
	public function setupErrorHandler() {
		set_error_handler(function($no,$str,$file,$line,$context) {
            $err = new \EternityX\TPLEX(ENV_ROOT.'/models/helpers/templates/error.tpl');
            
            // determine error constant
            foreach(get_defined_constants() as $key => $val) 
                if(substr($key, 0, 2) == 'E_' && $val == $no) $err->error = $key;
                
            $err->date = date('Y-m-d@H:i:s');
            $err->file = $file;
            $err->line = $line;
            $err->message = $str;
            $err = PHP_EOL.$err.PHP_EOL;
            
			mail(EX_WEBMASTER_EMAIL,EX_TITLE.' Error Logged', $err);
			error_log($err,3,ENV_ROOT.'/data/logs/errors.log');			
			if(DEBUG) die(str_replace(PHP_EOL,"<br>",$err));
		}, E_ALL);		
	}
	
    public function setupExceptionHandler() {
        set_exception_handler(function($e) {
            $exc = new \EternityX\TPLEX(ENV_ROOT.'/models/helpers/templates/error.tpl');
            $exc->error = 'Uncaught Exception '.get_class($e);
            $exc->date = date('Y-m-d@H:i:s');
            $exc->file = $e->getFile();
            $exc->line = $e->getLine();
            $exc->message = $e->getMessage();
            $exc = PHP_EOL.$exc.PHP_EOL;
            mail(EX_WEBMASTER_EMAIL, EX_TITLE.' Error Logged', $exc);
            error_log($exc, 3, ENV_ROOT.'/data/logs/errors.log');
            if(DEBUG) die(str_replace(PHP_EOL, "<br>", $exc));
        });
    }
    
    public function getUser() {
        return $this->user;
    }
    
    public function getAuthLevel() {
        return $this->user->permissions;
    }
    
    public function authTo($lvl) {
        if($this->user->permissions < $lvl) throw new HTTP\Exception(403);
    }
    
	/**
	 * Gets request information
	 */
	private function getRequest() {
		$this->request = new \stdClass();
		$this->request->name = rawurldecode(ltrim(strtok($_SERVER['REQUEST_URI'], '?'), '/'));
        $this->request->query = strtok("?");
        $_SERVER['QUERY_STRING'] = $this->request->query;
        
		$this->request->ext = pathinfo($this->request->name, PATHINFO_EXTENSION);
		$this->request->type = ($this->request->ext == '') ? self::REQUEST_PAGES : self::REQUEST_ASSETS;
		$this->request->method = strtolower($_SERVER['REQUEST_METHOD']);
        
        $pieces = explode('.', $_SERVER['HTTP_HOST']);
        $this->request->app = array_shift($pieces);
		// method fixes
		if($this->request->method == 'put' || $this->request->method == 'delete') {
			parse_str(file_get_contents('php://input', false , null, -1 , $_SERVER['CONTENT_LENGTH'] ), $_REQUEST);
            parse_str(file_get_contents('php://input', false , null, -1 , $_SERVER['CONTENT_LENGTH'] ), $_FILES);
        }
        
        // name fix
        if($this->request->name == '') $this->request->name = 'index';
        $pieces = explode('.', str_replace(['http://', 'https://'], '', parse_ini_file(EX_ROOT.'/application/configs/application.ini')['url']));
        $url = array_shift($pieces);
        if($this->request->app != $url) define('APP_ROOT', EX_ROOT.'/application/apps/'.$this->request->app);
        else define('APP_ROOT', EX_ROOT.'/application');
        
	}
	
	/**
	 * Sets up Database information.
	 */
	public function setupDB($host, $name, $user, $pass) {
		$dsn = 'mysql:host='.$host.';dbname='.$name;
		$this->db = new \PDO($dsn, $user, $pass);
		$GLOBALS['db'] = $this->db;
		$pdo = $this->db;
	}
	
	private function specials() {
        if($this->request->ext == '') $this->request->name .= '.php';
		$file = ENV_ROOT.'/views/'.$this->request->name;
		if(file_exists($file)) include $file;
        else $this->request->name = str_replace('.php', '', $this->request->name);
	}
	
	public function setupSession() {
		$session = new session($this->db);
		session_set_save_handler($session) or die('<b>EX0001:</b> Unable to start session');
		session_start();
	}
	
	public function setupUser() {
		$id = null;
		if(!empty($_SESSION)) $id = $_SESSION['id'];
		$this->user = new User($this->db, $id);
		$GLOBALS['user'] = $this->user;
	}
	
	private function dispatchAssets() {
		$file = APP_ROOT.'/assets/'.$this->request->name;
		if(!file_exists($file)) throw new HTTPError(404);
		
		// get content
		ob_start();
		if($this->request->ext == 'js' || $this->request->ext == 'css') include $file;
		else echo file_get_contents($file);
		$content = trim(ob_get_clean());
		
		if($this->request->ext == 'js') $minify = new Minify($content, Minify::TYPE_JS);
		if($this->request->ext == 'css') $minify = new Minify($content, Minify::TYPE_CSS);
		if(isset($minify)) $content = $minify->minify();
		
		// determine whether or not to cache
		if($this->exts[$this->request->ext]['cache']) $this->setCache(true);
		else $this->setCache(false);
		
		header('Content-Type: '.$this->exts[$this->request->ext]['mimetype']);
		die($content);
	}
	
	private function dispatchPages() {
		if($this->request->name == '') $this->request->name = 'index';
      
		// setup bases
		$this->request->base = dirname($this->request->name);
		$this->request->base2 = dirname($this->request->base);
		
		if(in_array($this->request->base, $this->dynamics) || in_array($this->request->base2, $this->dynamics)) $pagetype = 'dynamic';
		else if(is_dir(APP_ROOT.'/views/pages/'.$this->request->name)) $pagetype = 'directory';
		else $pagetype = 'singular';
		$file = ['content', 'assets', 'meta'];
		// setup files
		switch($pagetype) {
			case 'singular' :
				$file['content'] = APP_ROOT.'/views/pages/'.$this->request->name.'.php';
				$file['assets'] = APP_ROOT . '/views/assets/'.$this->request->name.'.php';
				$file['meta'] = APP_ROOT . '/views/assets/'.$this->request->name.'.php';
				break;
			
			case 'directory' :
				$file['content'] = APP_ROOT . '/views/pages/'.$this->request->name.'/index.php';
				$file['assets'] = APP_ROOT . '/views/assets/'.$this->request->name.'/index.php';
				$file['meta'] = APP_ROOT . '/views/assets/'.$this->request->name.'/index.php';
				break;
			
			case 'dynamic' :
				// dynamics level 2 fix
				if(in_array($this->request->base2, $this->dynamics)) {
					$this->request->name = $this->request->base;
					$this->request->base = $this->request->base2;
				}
				
				// setup illusionary GET variable & determine dynamic page type (dypage)
				$_GET[$this->request->base] = str_replace($this->request->base . "/", '', $this->request->name);
				if($_GET[$this->request->base] == "") $dypage = self::DYNAMIC_INDEX;
				else $dypage = self::DYNAMIC_VIEW;
				
				$filex = ($dypage == self::DYNAMIC_VIEW) ? 'view' : 'index';
				$file['content'] = APP_ROOT . '/views/pages/'.$this->request->base.'/'.$filex.'.php';
			
				if($dypage == self::DYNAMIC_VIEW) {
					$file['assets'] = APP_ROOT . '/views/assets/'.$this->request->base.'_dynamic.php';
					$file['meta'] = APP_ROOT . '/views/meta/'.$this->request->base.'_dynamic.php';
				} else { // dypage is equal to index
					$file['assets'] = APP_ROOT . '/views/assets/'.$this->request->name.'.php';
					$file['meta'] = APP_ROOT . '/views/meta/'.$this->request->name.'.php';
				}
				break;
		}

		if(!file_exists($file['content']) || !isset($file['content'])) throw new HTTP\Exception(404);
		
		maintenance : {
			if(EX_MAINTENANCE === 'On') {
				if($this->request->name != 'maintenance' && (!isset($this->user) || $this->user->permissions <= 1)) { redirect('/maintenance'); die; }
				http_response_code(503);
				$file['content'] = APP_ROOT . '/views/specials/maintenance/index.php';
				$file['assets'] = APP_ROOT . '/views/specials/maintenance/assets.php';
				$file['meta'] = APP_ROOT . '/views/specials/maintenance/meta.php';
			}	
		}
		
		banned : {
			if($this->banned) {
				if($this->request->name != 'banned' && (!isset($this->user) || $this->user->permissions <= 1)) { redirect('/banned'); die; }
				$file['content'] = APP_ROOT . '/views/specials/banned/index.php';
				$file['assets'] = APP_ROOT . '/views/specials/banned/assets.php';
				$file['meta'] = APP_ROOT . '/views/specials/banned/meta.php';
			}
		}
		
		// Setup Vars
		$pdo = $this->db;
		$user = $this->user;
		
		// RETRIEVE CONTENT ==================
		ob_start();
		include $file['content'];
		$page['content'] = trim(ob_get_clean());
		// RETRIEVE CONTENT ================== 
		
		// RETRIEVE ASSETS ===================
		ob_start();
		if(file_exists($file['assets'])) include $file['assets'];
		$page['assets'] = trim(ob_get_clean());
		// RETRIEVE ASSETS ====================
		
		// RETRIEVE META ======================
		ob_start();
		if(file_exists($file['meta'])) include $file['meta'];
		$page['meta'] = trim(ob_get_clean());
		// RETRIEVE META ======================
		
		$page['title'] = (isset($title)) ? $title : ucfirst($this->request->name).' - '.EX_TITLE;
		
		$output = new Page($page['title']);
		$output->setTheme($GLOBALS['theme']);
		$output->setContent($page['content']);
		$output->setAssets($page['assets']);
		$output->setMetadata($page['meta']);
		
		die($output);
		
	}
	
	private function dispatchControllers() {
		foreach (glob(APP_ROOT . "/controllers/*.php") as $filename) include_once $filename;
		$this->controllers->call($_POST['action']);
		die;
	}
	
	private function dispatchAPI() {
		$apiname = explode("/", str_replace('api', 'controllers', preg_replace("/\/([0-9]+)/is", "", $this->request->name)))[2];
		$filename = ENV_ROOT.'/controllers/v2/'.$apiname.'.php';
		include $filename;
		$apicall = "\\EternityX\\Api\\v2\\".str_replace("/", "\\", ucfirst($apiname));
		$api = new $apicall($this->request->name);
		$api->call();
		die; 
	}
	
	public function dispatch() {
		global $infonetstart;
		$this->import('traits');
		$this->import('interfaces');
		$this->import('classes');
		$this->import('functions');
        
        $this->controllers = new Controller();
		$this->ajax = $this->controllers;
        
		$this->import('startups');
		if($infonetstart) return;
		$this->specials();
	
		if(isset($_GET['mode']) && $_GET['mode'] == 'api') $this->dispatchAPI();
		
		switch($this->request->method) {
			case 'get' :
				switch($this->request->type) {
					case self::REQUEST_PAGES :
						$this->dispatchPages();
						break;
					case self::REQUEST_ASSETS :
						$this->dispatchAssets();
						break;
				}
				break;
				
			case 'post' :
				if(is_array($_POST) && !empty($_POST)) {
					if($this->request->name == 'ajax') $this->dispatchControllers();
					
					// new to 2.0.7 : RESTful Models
					$this->rest = true;
					$this->dispatchPages();
				} 
				throw new HTTP\Exception(404);
				break;
				
			// new to 2.0.7 : RESTful Models
			case 'put' :
			case 'delete' :
				if(empty($_POST) || !isset($_POST) || !is_array($_POST)) throw new HTTP\Exception(404);
				$this->rest = true;
				$this->dispatchPages();
				break;
		}
	}
}
