<?php
/**
 * HTTP Package.  Uniform for making requests over HTTP/HTTPS
 * 
 * EternityX's HTTP Package.  This provides
 * a simple and uniform way for making requests
 * and recieving responses over HTTP/HTTPS.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.1.0α2
 * @package EternityX\HTTP
 */

namespace EternityX\HTTP;

use \EternityX\Events;

/**
 * The core request class.
 * 
 * The core request class, uses cURL to
 * manage request and return the output.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.1.0α1
 * @package EternityX\HTTP
 */
final class Request {
    use \EternityX\Traits\tRequire;
    
    /**
     * The URL to open
     * 
     * @var string
     */
    private $url;
    
    /**
     * The connection handle
     * 
     * @var resource 
     */
    private $cn;
    
    /**
     * Response from the connection handle.
     *
     * @var EternityX\HTTP\Response 
     */
    private $response;
    
    /**
     * Arguments to use in the request.
     * 
     * @var array
     */
    private $args = array(
        'method' => 'GET',
        'timeout' => 5,
        'redirects' => 5,
        'httpversion' => '1.1',
        'user-agent' => 'EternityX',
        'blocking' => true,
        'headers' => [],
        'cookies' => [],
        'body' => null,
        'compress' => false,
        'decompress' => true,
        'sslverify' => true,
        'sslcertificates' => "/data/ca-bundle.crt",
        'stream' => false,
        'filename' => null,
        'limit_response_size' => null,
        'filters' => ['trim']
    );
    
    /**
     * List of URLs to block.
     * 
     * @var array
     */
    static private $block = [];   
    
    /**
     * Amount of successful requests.
     * 
     * @since 2.1.0α2
     * @var int
     */
    static public $hits = 0;
    
    /**
     * Amount of unsuccessful requests.
     * 
     * @since 2.1.0α2
     * @var int
     */
    static public $misses = 0;
    
    
    /**
     * Request Constructor
     * 
     * This establishes the connection to the 
     * destination url with the specified arguments.
     *
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * @since v2.1.0α2 added the count parameter
     * 
     * @param string $url the url to open
     * @param array $args custom arguments
     * @param bool $count whether or not to count the request against a hit or miss
     * @return bool
     */
    public function __construct($url, array $args = [], $count = true) {
        Events::call('request', [$url, $args]);
        $this->requireExtension('cURL');
        $this->url = $url;
        $this->args = array_merge($this->args, $args);
        
        foreach($GLOBALS['wrappers'] as $wrapper) {
            if(\EternityX\String::beginsWith($url, $wrapper)) {
                $this->response = file_get_contents($url);
                Events::call('end:request', [$url, $args, $this->response]);
                if($count) self::$hits++;
                return true;
            }
        }
        
        if($this->args['blocking']) {
            foreach(self::$block as $block) {
                if(preg_match('/'.preg_quote($block).'/', $url)) return false;
            }
        }
        
        $this->cn = curl_init();
        curl_setopt($this->cn, CURLOPT_URL, $url);
        curl_setopt($this->cn, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->cn, CURLOPT_USERAGENT, $this->args['user-agent']);
        
        // Let this class handle redirection...
        // Doesnt work in safe mode, open_basedir or when post requests are made
        curl_setopt($this->cn, CURLOPT_FOLLOWLOCATION, false);
        
        // timeouts
        curl_setopt($this->cn, CURLOPT_TIMEOUT, $this->args['timeout']);
        curl_setopt($this->cn, CURLOPT_CONNECTTIMEOUT, $this->args['timeout']);
        
        // ssl
        curl_setopt($this->cn, CURLOPT_SSL_VERIFYHOST, ($this->args['sslverify']) ? 2 : 0);
        curl_setopt($this->cn, CURLOPT_SSL_VERIFYPEER, $this->args['sslverify']);
        curl_setopt($this->cn, CURLOPT_CAINFO, ENV_ROOT.$this->args['sslcertificates']);
        
        // cookies
        if(!file_exists(ENV_ROOT.'/data/temp/cookie.txt')) {
            if(!is_dir(ENV_ROOT.'/data/temp')) mkdir(ENV_ROOT.'/data/temp');
            file_put_contents(ENV_ROOT.'/data/temp/cookie.txt', '');
        }
        curl_setopt($this->cn, CURLOPT_COOKIESESSION, true);
        curl_setopt($this->cn, CURLOPT_COOKIEFILE, ENV_ROOT.'/data/temp/cookie.txt');
        curl_setopt($this->cn, CURLOPT_COOKIEJAR, ENV_ROOT.'/data/temp/cookie.txt');
        
        // head request fix
        if($this->args['method'] == 'HEAD') curl_setopt($this->cn, CURLOPT_NOBODY, true);
        else curl_setopt($this->cn, CURLOPT_POSTFIELDS, $this->args['body']);
        
        // determine http version
        switch($this->args['httpversion']) {
            case '1.0' :
                curl_setopt($this->cn, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                break;
            case '1.1' :
                curl_setopt($this->cn, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                break;
        }
        
        // headers
        if (!empty($this->args['headers'])) {
			$headers = [];
			foreach ($this->args['headers'] as $name => $value) $headers[] = "{$name}: $value"; 
			curl_setopt($this->cn, CURLOPT_HTTPHEADER, $headers);
		}
        
        $this->response = new Response($this->cn, $this->args);
        
        // Handle Redirection
        for($i=0; (($i <= $this->args['redirects']) && in_array($this->response->code, [301,302])); $i++) 
            $this->response = (new self($this->response->headers['location'], $this->args, false))->getResponse();
        
        Events::call('end:request', [$url, $args, $this->response]);
        
        // Determine whether or not we had a success or not.
        if($this->response->error || round($this->response->code / 100) != 2) {
            self::$misses++;
            return false;
        }
        
        if($count) self::$hits++;
        return true;
    }
    
    /**
     * Returns the request body.
     * 
     * Retrieves the resulting body returned
     * by the request.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return string
     */
    public function __toString() {
        return (string)$this->response;
    }
    
    /**
     * Returns the error code.
     * 
     * Returns the error code returned by 
     * the requested url.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return int
     */
    public function getError() {
        return $this->response->error;
    }
    
    /**
     * Returns the HTTP Code.
     * 
     * Returns the http code that resulted from the
     * connection handle response.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return int
     */
    public function getCode() {
        return $this->response->code;
    }
    
    /**
     * Returns the request's response.
     * 
     * Returns the response from the http request 
     * made in the constructor of this class.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.1.0α1
     * 
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }
}


/**
 * Response class for returning results of a request.
 * 
 * This is the class in which is returned by 
 * the request class after the request has been
 * made and a response has been given, thus this response.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since 2.1.0α2
 * @package EternityX\HTTP
 */
final class Response {
    
    /**
     * The HTTP Response code.  Defaults to 200.
     * 
     * @var int 
     */
    public $code = 200;
    
    /**
     * cURL error (if applicable)
     * 
     * @var string
     */
    public $error;
    
    /**
     * Content-Type of the response.
     * 
     * @var string
     */
    public $type;
    
    /**
     * body of the response.
     * 
     * @var string
     */
    public $body;
    
    /**
     * Headers returned by the request.
     * 
     * @var string|array
     */
    public $headers;
    
    /**
     * The connection handle.
     * 
     * @var resource
     */
    private $cn;
    
    /**
     * Class constructor.
     * 
     * Sets up the necessary variables for the class
     * and closes the connection handle freeing up
     * resources.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param resource $cn cURL connection handle
     * @return void
     */
    public function __construct($cn, array $args = []) {
        Events::call('response', [$cn, $args]);
        $this->cn = $cn;
        $this->args = $args;
        curl_setopt($this->cn, CURLOPT_HEADERFUNCTION, [$this, 'headers']);
        curl_setopt($this->cn, CURLOPT_HEADER, false);
        
        $this->body = ($this->args['limit_response_size']) ? substr(curl_exec($this->cn), 0, $this->args['limit_response_size']) : curl_exec($this->cn);
        $this->error = curl_error($this->cn);
        $this->code = curl_getinfo($this->cn, CURLINFO_HTTP_CODE);
        $this->type = curl_getinfo($this->cn, CURLINFO_CONTENT_TYPE);
        curl_close($this->cn);
        
        // Parse
        $this->headers = self::parseHeaders($this->headers);
        foreach($this->args['filters'] as $filter) 
            $this->body = call_user_func($filter, $this->body);
        
        Events::call('end:response', [$this]);
    }
    
    /**
     * Header Grabbing Function.
     * 
     * Custom header grabbing functionality to grab
     * the headers returned by a request.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param resource $cn unused but required by the curl function.
     * @param string $headers headers returned by the connection handle.
     * @return int
     */
    public function headers($cn, $headers) {
        $this->headers .= $headers;
        return strlen($headers);
    }
    
    /**
     * Header Parsing Function.
     * 
     * Parses encoded headers and returns
     * them in array format.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param string $headers list of headers
     * @return array
     */
    static public function parseHeaders($headers) {
        if(is_string($headers)) {
            $headers = str_replace("\r\n", "\n", $headers);
            $headers = preg_replace('/\n[ \t]/', ' ', $headers);
            $headers = explode("\n", $headers);
        }
        
        for ( $i = count($headers)-1; $i >= 0; $i-- ) {
			if ( !empty($headers[$i]) && false === strpos($headers[$i], ':') ) {
				$headers = array_splice($headers, $i);
				break;
			}
		}
        
        $tempheaders = $headers;
        $headers = [];
        foreach($tempheaders as $header) {
            $key = substr($header, 0, strpos($header, ":"));
            $val = str_replace($key.": ", "", $header);
            $headers[strtolower($key)] = $val;
        }
        
        return $headers;
    }
    
    /**
     * Gets information associated with an http code.
     * 
     * Gets information associated with an http
     * code from the http.types file in the framework's
     * data directory.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param int $code the code to retrieve
     * @return string
     */
    static function getErrorMessage($code = 200) {
        return parse_types_file(ENV_ROOT.'/data/http.types')[$code];
    }
    
    
    /**
     * Converts the class into a string 
     * (contents of the body property).
     * 
     * Converts the class into a string, in
     * other words the contents of the body
     * property.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @return string
     */
    public function __toString() {
        return (string)$this->body;
    }
}

/**
 * HTTP Exception to be thrown to imitate a filesystem error.
 * 
 * This is an exception that is to be thrown to
 * imitate a filesystem error and other related tasks.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since 2.1.0α2
 * @package EternityX\HTTP
 */
class Exception extends \Exception {
	
    /**
     * The HTTP Status code.
     * 
     * @var int
     */
	public $code = 200;
    
    /**
     * The HTTP Status Informational Representation.
     * 
     * @var string
     */
	public $message = '';
    
    /**
     * 
     * 
     * @var string
     */
	public $diagnosis = '';
    
    /**
     * Constructor.
     * 
     * This constructs the new HTTP exception.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.1.0α2
     * 
     * @param int $code
     * @return void
     */
	public function __construct($code = 200) {
        Events::call('httperror', [$code]);
		http_response_code($code);
		$this->code = $code;
		$this->message = \EternityX\HTTP\Response::getErrorMessage($code)[0];
		$this->diagnosis = \EternityX\HTTP\Response::getErrorMessage($code)[1];
		parent::__construct();
        Events::call('end:httperror', [$code]);
	}
	
}

class_alias("\EternityX\HTTP\Exception", "\EternityX\HTTPError");