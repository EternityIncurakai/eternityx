<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace EternityX\Widgets;

class Overlay extends \EternityX\TPLEX {
    public function __construct($id, $color) {
        parent::__construct(ENV_ROOT.'/models/helpers/templates/overlay.tpl');
        $this->__set('id', $id);
        $this->__set('color', $color);
    }
}

class_alias("\EternityX\Widgets\Overlay", "\EternityX\UI\Overlay");
