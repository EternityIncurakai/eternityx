<?php

/**
 * To Be Documented
 */

namespace EternityX\Widgets;

class Spinner extends \EternityX\TPLEX {
	public function __construct($id) {
		parent::__construct(ENV_ROOT."/models/helpers/templates/spinner.tpl");
		$this->__set('id', $id);
	}
}
