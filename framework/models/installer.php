<?php

/**
 * Short Description.
 *
 * Long Description
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version 2.1.0
 * @package EternityX
 */

namespace EternityX;

use \PDO;

class Installer {
    use Traits\tProgress;
    private $db;
    private $user;
    private $imgs;
    
    public function __construct() {
        try {
            $this->progress();
            
            $this->check();
            foreach(parse_csv_file(ENV_ROOT.'/data/newdirs.csv') as $dir) { if(!is_dir(EX_ROOT.'/'.$dir)) mkdir(EX_ROOT.'/'.$dir); }
            $this->progress(round((100/6)*1), 2, 'Setting up the Database');
            
            $this->setupDatabase();
            $this->progress(round((100/6)*2), 2, 'Setting up Admin Account');
            
            $this->setupAdmin();
            $this->progress(round((100/6)*3), 2, 'Setting up Images');
            
            $this->setupImages();
            $this->progress(round((100/6)*4), 2, 'Setting up Configs');
            
            $this->setupConfigs();
            $this->progress(round((100/6)*5), 2, 'Setting up Cron Jobs');
            
            $this->setupCron();
            $this->progress(100, 5, 'Done!');
        } catch (\Exception $ex) {
            $this->progress(0, 5, 'Uh oh...');
            $this->progress(0, 5, 'There was an error :/');
        }
    }
    
    public function check() {
        // admin
        $this->user = new stdClass();
        $this->user->name = $_POST['adminname'];
        $this->user->pass = $_POST['adminpass'];
        if(!String::lengthInRange($this->user->name, 5, 15)) throw new \Exception('bad username');
        if(!String::lengthInRange($this->user->pass, 8, 18)) throw new \Exception('bad password');
        if(!String::hasUppercase($this->user->pass) || !String::hasLowercase($this->user->pass) || !String::hasInteger($this->user->pass) || !String::hasSpecial($this->user->pass)) throw new \Exception('bad password');
        
        // images
        $this->imgs = new stdClass();
        $this->imgs->logo = $_FILES['logo'];
        $this->imgs->favicon = $_FILES['favicon'];
        if(!is_uploaded_file($this->imgs->logo['tmp_name']) || !is_uploaded_file($this->imgs->favicon['tmp_name'])) throw new \Exception('no files present');
        if(getimagesize($this->imgs->favicon['tmp_name'])[0] != getimagesize($this->imgs->favicon['tmp_name'])[1]) throw new \Exception('favicon must be a square');
        if(!in_array($this->imgs->logo['type'], ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'])) throw new \Exception('bad logo type');
        if(!in_array($this->imgs->favicon['type'], ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/x-icon'])) throw new \Exception('bad favicon type');
        if(!in_array(pathinfo($this->imgs->logo['name'], PATHINFO_EXTENSION), ['jpg', 'jpeg', 'png', 'gif'])) throw new \Exception('bad logo extension');
        if(!in_array(pathinfo($this->imgs->favicon['name'], PATHINFO_EXTENSION), ['jpg', 'jpeg', 'png', 'gif', 'ico'])) throw new \Exception('bad favicon extension');
    }
    
    public function setupDatabase() {
        $dsn = 'mysql:host='.$this->db->host.';dbname='.$this->db->name;
        $this->db->conn = new PDO($dsn);
        $this->db->conn->query(file_get_contents(EX_ROOT.'/maintenance/installation/utilities/database.sql'));
    }
    
    public function setupAdmin() {
        $this->user->pass = password_hash($this->user->pass, PASSWORD_DEFAULT);
        
        $this->db->conn->prepare("INSERT INTO `users` (`username`, `password`, `permissions`), VALUES (?,?,?)");
        $this->db->conn->execute([
           1 => $this->user->name,
           2 => $this->user->pass,
           3 => 5
        ]);
    }
    
    public function setupImages() {
        /**
         * @todo add better errors
         */
        if(!move_uploaded_file($this->imgs->logo['tmp_name'], APP_ROOT.'/assets/img/logo.png')) throw new \Exception('failed to move file');
        if(!move_uploaded_file($this->imgs->favicon['tmp_name'], APP_ROOT.'/assets/img/favicon.png')) throw new \Exception('failed to move file');
        
        $this->imgs->logo['location'] = APP_ROOT.'/assets/img/logo.png';
        
        if(in_array($this->imgs->logo['type'], ['image/jpeg', 'image/jpg'])) $img = imagecreatefromjpeg($this->imgs->logo['location']);
        else if($this->imgs->logo['type'] == 'image/png') $img = imagecreatefrompng($this->imgs->logo['location']);
        
        imagealphablending($img, false);
        imagesavealpha($img, true);
        imagepng($img, $this->imgs->logo['location'], 9);
    }
    
    public function setupConfigs() {
        define('URL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'On') ? 'https://' : 'http://').$_SERVER['HTTP_HOST']);
        $exini = new TPLEX(EX_ROOT.'/maintenance/installation/utilities/exskeleton.ini');
        file_put_contents(APP_ROOT.'/configs/application.ini', $exini);
    }
    
    public function setupCron() {
        foreach(glob(ENV_ROOT.'/scripts/*.php') as $cron) addCron('* * 0 0 0', $cron);
    }
    
}