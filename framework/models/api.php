<?php
/**
 * API Package.
 * @todo document
 */

namespace EternityX;

abstract class API {
	const VERSION = 2;
	const REQUEST_GET = 1;
	const REQUEST_POST = 2;
	const REQUEST_PUT = 3;
	const REQUEST_DELETE = 4;
	const REQUEST_HEAD = 5;
	const REQUEST_OPTIONS = 6;
	
	public $endpoint;
	public $verb;
	protected $args = array();
	protected $method;
	protected $db;
	
	public function __construct($request) {
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: *");
		
		// SETUP REQUEST
		$request = ltrim($request, "api/v".self::VERSION."/");
        $request = str_replace('.php', '', $request);
		$requestpieces = explode("/", $request);
		$this->endpoint = $requestpieces[0];
		array_shift($requestpieces);
		if(is_numeric($requestpieces[0])) { $this->args = array_shift($requestpieces); $this->verb = $requestpieces[0];  }
		else { $this->verb = $requestpieces[0];  array_shift($requestpieces); $this->args = $requestpieces; }
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->db = $GLOBALS['db']; 
	}
	
	public function call() {
		$this->{strtolower($this->method).ucfirst($this->verb)}();
	}
	
	public function get() {}
	public function post() {}
	public function put() {}
	public function delete() {}
	public function head() {}
	public function options() {} 
}
