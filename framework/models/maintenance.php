<?php
/**
 * Maintenance Package.
 * @todo document
 */

namespace EternityX {

class Maintenance {
	public $name;
	public $url;
	
	private $dir;
    private $admin;
    
	public function __construct($name, $url, $admin = false) {
		if(!is_dir(EX_ROOT.'/maintenance/'.$name)) throw new MaintenanceException(1);
		$this->name = $name;
		$this->url = $url;
		$this->dir = EX_ROOT.'/maintenance/'.$this->name;
        $this->admin = $admin;
	}
	
	public function check() {
		return include($this->dir.'/check.php');
	}
	
	public function render() {
		global $dispatcher;
		
        if(!($this->admin && $dispatcher->getAuthLevel() == dispatcher::AUTH_ADMIN)) 
            if($dispatcher->request->name != $this->url) \redirect('/'.$this->url);
		
		if($dispatcher->request->method == 'get') {
			$tpl = new \EternityX\TPLEX($this->dir.'/interface/page.tpl');
			die($tpl);
		} else if($dispatcher->request->method == 'post') {
			include $this->dir.'/utilities/'.$_POST['action'].'.php';
			die;
		}
	}
}


class MaintenanceException extends \Exception {
	
}
}

namespace EternityX\Maintenance {

class Page {
    private $args = [];
    private $dir = '';
    
    public function __construct(array $args = []) {
        $this->args = getJSON(EX_ROOT.'/.maintenance', true);
        $this->args = array_merge($args, $this->args);
        $this->dir = EX_ROOT.'/maintenance/'.$this->args['type'];
        $this->page = new \EternityX\TPLEX($this->dir.'/interface/page.tpl');
        $this->dispatch();
    }
    
    public function dispatch() {
        switch(strtolower($_SERVER['REQUEST_METHOD'])) {
            case 'get' :
                $this->redirect();
                $this->render();
                break;
            case 'post' :
                $this->action();
                break;
        }
    }
    
    private function redirect() {
        if(ltrim(strtok($_SERVER['REQUEST_URI'], '?'), '/') != $this->args['url']) 
            redirect('/'.$this->args['url']);
    }
    
    private function render() {
        die($this->page);
    }
    
    private function action() {
        if(file_exists($this->dir.'/utilities/'.$this->args['actions'][$_POST['action']]))
                include $this->dir.'/utilities/'.$this->args['actions'][$_POST['action']];
        die;
    }
}
}