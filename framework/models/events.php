<?php
/**
 * Events Package for EternityX.
 * 
 * This file houses the events package
 * for EternityX, providing a uniform, event driven
 * programmatical interface for it, making it 
 * easy for pluggable interfaces. 
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since 2.0.0
 * @package EternityX\Events
 */

namespace EternityX;

/**
 * The core Events Class.
 * 
 * This is the core events
 * class that houses core functionality
 * for calling and adding events.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since 2.0.0
 * @package EternityX\Events
 */
class Events {
	
    /**
     * Array containing a list of events and their handlers.
     * 
     * @var array
     */
	static private $events = [];
	
    /**
     * Calls an event
     * 
     * This dispatches the event to call
     * for the program.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.0.0
     * @since 2.1.0α2 renamed from callEvent to call
     * @since 2.1.0α2 added args parameter
     * 
     * @param string $event the name of the event
     * @param array $args an optional list of arguments
     * @return void
     */
	static function call($event, array $args = []) {
		if(array_key_exists($event, self::$events)) {
			foreach(self::$events[$event] as $callable) 
                call_user_func_array($callable, $args);
		} 
	}
	
    /**
     * Adds an event handler.
     * 
     * Adds a handler to the event
     * for it to be dispatched.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since 2.0.0
     * @since 2.1.0α2 renamed from addHandler to add
     * 
     * @param string $event the event to add
     * @param callable $function the function to add as a handler
     * @return void
     */
	static function add($event, callable $function) {
		self::$events[$event][] = $function;
	}
    
    /**
     * @deprecated in favor of add
     * @see add
     */
    static function addHandler($event, callable $function) { self::add($event, $function); }
    
    /**
     * @deprected in favor of call
     * @see call
     */
    static function callEvent($event) { self::call($event); }
}


