<?php
/**
 * Theme Package
 * 
 * This file houses the theme package, the
 * core class, it's exception class, and stream class.
 * Theme package allows EternityX to have appearance.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.0.9
 * @package EternityX\Theme
 */


namespace EternityX;


/**
 * The core theme class.
 * 
 * The class that makes the visible interface modular and easy
 * to read and deliver.  It also boosts the performance of 
 * EternityX rather than just serving static files each time.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.9
 * @package EternityX\Theme
 * 
 */
class Theme implements iCmztn {
    use tUpgrade;
   
    /**
     * The name of the Theme
     * 
     * @var string 
     */
    public $name;
    
    /**
     * Where to locate the Theme
     * 
     * @var string 
     */
    private $location;
    
    /**
     * The mode in which to open the Theme.
     * 
     * @var int 
     */
    private $mode = self::MODE_PHAR;
    	
    const DIRECTORY = THEMES_ROOT; 
	const VALID_ELEMS = 3;
    const VALID_ICON = 4;
    
    
    /**
     * Requirements that the theme has to meet.
     * 
     * @var array 
     */
	static private $reqs = [
        'files' => ['style.css','page.tpl','behavior.js','icon.png'],
        'keys' => ['name','description','author','copyright','url','version',"req_version"],
        'elems' => ['meta','title','assets','baseurl','hlogo','hlogotxt','/application/includes/header/navigation.php','/application/includes/header/announcement.php','content','logo','logotxt','copyright','/application/includes/footer/navigation.php'],
    ];
	
    /**
     * Constructs a new Theme object.
     * 
     * Determines the mode in which to open the theme, validates,
     * and if successful returns a Theme object in which EternityX
     * uses to render its appearance.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * 
     * @param string $name the name of the theme
     * @throws ThemeException
     * @return void
     */
	public function __construct($name) {
        events::callEvent('theme.startup');
        $this->name = $name;
        $this->setMode();
        $this->location = self::DIRECTORY.'/'.$this->name.(($this->mode == self::MODE_PHAR) ? '.phar' : '');
        clearstatcache();
        // unpack/pack based on retrieve type
        if($this->mode == self::MODE_PHAR && is_dir(rtrim($this->location, ".phar"))) $this->pack(); 
        else if($this->mode == self::MODE_DIR && file_exists($this->location.'.phar')) $this->unpack();
        
        
        // so we can be even more lazy
		if($this->mode == self::MODE_PHAR) $this->location = 'phar://'.$this->location;
        
        // Validate
        events::callEvent('theme.validation');
        if(!$this->valid()) throw new ThemeException(ThemeException::INVALID_THEME);
        if(!$this->valid(self::VALID_KEYS)) throw new ThemeException(ThemeException::INVALID_MANIFEST);
        if(!$this->valid(self::VALID_ELEMS)) throw new ThemeException(ThemeException::INVALID_TPL);
        if(!$this->valid(self::VALID_ICON)) throw new ThemeException(ThemeException::INVALID_ICON);
        if(version_compare($this->getManifest()['req_version'], EX_VERSION, ">")) throw new ThemeException(ThemeException::INCOMPATIBLE);
        events::callEvent('end:theme.validation');
        events::callEvent('end:theme.startup');
	}
    
    /**
     * Determines the open mode.
     * 
     * Determines the mode in which to open the 
     * theme based on environment factors, including
     * the phar.readonly value, and debug mode. Fixes 
     * an issue regarding the phar.readonly value.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @link http://repo.eternityincurakai.com/eternityx/issue/158/phar-readonly
     * @since v2.1.0α2
     * 
     * @return void
     */
    private function setMode() {
        if(!ini_get('phar.readonly')) {
            if((DEBUG && file_exists(self::DIRECTORY.'/'.$this->name.'.phar')) || (DEBUG && is_dir(self::DIRECTORY.'/'.$this->name))) $this->mode = self::MODE_DIR;
            else $this->mode = self::MODE_PHAR;
        } else {
            if(file_exists(self::DIRECTORY.'/'.$this->name.'.phar') && !DEBUG) $this->mode = self::MODE_PHAR;
            else $this->mode = self::MODE_DIR;
        }
    }
    
    /**
     * Validates different components of a theme.
     * 
     * Validates different components of a theme such
     * as existence of files, validity of manifest file,
     * or validity of template files.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * 
     * @param int $type the type to validate
     * @return boolean
     */
    private function valid($type = self::VALID_CORE) {
        switch($type) {
            case self::VALID_CORE : 
                events::callEvent('theme.validation.core');
                if(!file_exists($this->location)) return false;
                foreach(self::$reqs['files'] as $file) {
                    $file = $this->location.'/'.$file;
                    if(!file_exists($file)) return false;
                }
                events::callEvent('end:theme.validation.core');
                break;
                
            case self::VALID_KEYS :
                events::callEvent('theme.validation.keys');
                foreach(self::$reqs['keys'] as $key) { 
                    if(!(array_key_exists($key, $this->getManifest()))) return false;
                }
                events::callEvent('end:theme.validation.keys');
                break;
                
            case self::VALID_ELEMS :
                events::callEvent('theme.validation.elements');
                foreach(self::$reqs['elems'] as $elem) 
                    if(!preg_match('/\<\{\{tplex\.(sub|include|echo)\:\{'.preg_quote($elem,'/').'\}\/tplex\}\}\>/', $this->getTemplate())) return false;
                events::callEvent('end:theme.validation.elements');
                break;
            
            case self::VALID_ICON :
                events::callEvent('theme.validation.icon');
                $size = getimagesize($this->location.'/icon.png');
                if(!($size[0] == 100 && $size[1] == 100)) return false;
                events::callEvent('end:theme.validation.icon');
                break;
                
            default : return false;  
        }
        return true;
    }
    
    /**
     * Packs a directory into a theme pharchive.
     * 
     * Packs a theme's directory into a pharchive for
     * use in production mode.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @todo Check for errors, do not assume in production mode.
     * 
     * @return void
     */
    public function pack() {
        events::callEvent('theme.pack');
        $phar = new \Phar($this->location);
        $phar->buildFromDirectory(rtrim($this->location, ".phar"));

        foreach(glob(rtrim($this->location, ".phar")."/*") as $file) unlink($file);
        rmdir(rtrim($this->location, ".phar"));
        clearstatcache();
        
        // fix manifest
        $phar->setMetadata((array)json_decode(file_get_contents($phar["manifest.json"])));
        $phar->delete("manifest.json");
        events::callEvent('end:theme.pack');
    }
	
    /**
     * Unpacks a pharchive into a theme directory.
     * 
     * Unpacks a theme based pharchive into a theme
     * based directory for debug mode for editing purposes.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @todo Add error checking - do not assume in debug mode.
     * 
     * @return void
     */
    public function unpack() {
        events::callEvent('theme.unpack');
        $rootdir = THEMES_ROOT.'/'.$this->name.'.phar';
        $dest = THEMES_ROOT.'/'.$this->name.'/';
        mkdir($dest);
        
        $phar = new \Phar($rootdir);
        $phar->extractTo($dest);
        file_put_contents($dest.'manifest.json', stripslashes(json_encode($phar->getMetadata(), JSON_PRETTY_PRINT)));
        
        unset($phar);
        unlink($rootdir);
        \Phar::unlinkArchive($rootdir);
        clearstatcache();
        events::callEvent('end:theme.unpack');
    }
    
    /**
     * Gets manifest data about the theme.
     * 
     * This extracts information either from the manifest
     * file in the directory, or metadata from the archive
     * to array format.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @link ex.theme://current/manifest
     * 
     * @return array
     */
    public function getManifest() {
        switch($this->mode) {
            case self::MODE_DIR :
                if(!file_exists($this->location.'/manifest.json')) throw new ThemeException(ThemeException::INVALID_THEME);
                return (array)json_decode(file_get_contents($this->location.'/manifest.json'));
            
            case self::MODE_PHAR :
                return (new \Phar($this->location))->getMetadata();
        }
    }
    
    /**
     * Gets the theme's stylesheet.
     * 
     * Extracts the theme's stylesheet from the source 
     * whether it be from the phar archive or directory.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @link ex.theme://current/stylesheet
     * 
     * @return string
     */
	public function getStylesheet() {
        ob_start();
        include $this->location.'/style.css';
		$minify = new Minify(trim(ob_get_clean()), Minify::TYPE_CSS);
		return $minify->minify();
	}
	
    /**
     * Extracts the theme's template file.
     * 
     * Extracts the theme's template file from either
     * the directory or phar file for easy use.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @link ex.theme://current/template
     * 
     * @return string
     */
	public function getTemplate() {
        if(!file_exists($this->location.'/page.tpl')) throw new ThemeException(ThemeException::INVALID_THEME);
		return file_get_contents($this->location.'/page.tpl');
	}
    
    /**
     * Gets the theme's behavior file
     * 
     * Extracts the behavior (javascript file) from
     * a theme for easy use and access.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @link ex.theme://current/behavior
     * 
     * @return string
     */
    public function getBehavior() {
        if(!file_exists($this->location.'/behavior.js')) throw new ThemeException(ThemeException::INVALID_THEME);
        return file_get_contents($this->location.'/behavior.js');
    }
    
    /**
     * Gets the theme's icon 
     * 
     * Extracts the currently used theme's icon
     * and returns it in string format, ready for
     * output.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9 
     * @link ex.theme://current/icon
     * 
     * @return string
     */
    public function getIcon() {
        if(!file_exists($this->location.'/icon.png')) throw new ThemeException(ThemeException::INVALID_THEME);
        return file_get_contents($this->location.'/icon.png');
    }
    
    /**
     * Determines whether or not a theme exists.
     * 
     * Does a search in the themes directory for a 
     * pharchive or a directory that correlates to the 
     * theme specified.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * 
     * @param string $name name of the theme
     * @return boolean
     */
    static public function exists($name) {
        return (is_dir(self::DIRECTORY.'/'.$name.'/') || file_exists(self::DIRECTORY.'/'.$name.'.phar'));
    }
    
    /**
     * Returns a list of themes installed
     * 
     * Fetches and returns a complete list of themes
     * that are currently installed on EternityX in 
     * the customization directory.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.9
     * @link ex.theme://installed
     * 
     * @return array
     */
    static public function getList() {
        $themes = [];
        foreach(glob(self::DIRECTORY.'/*', GLOB_ONLYDIR) as $theme) $themes[] = basename($theme);
        return $themes;
    }
 
    
    /**
     * Gets a property defined in the manifest
     * 
     * Acts as a shorthand method for retrieving a property 
     * in the manifest.
     * 
     * @author XenoK <xenok@eterityincurakai.com>
     * @since v2.0.9  
     * 
     * @param string $name name of property from manifest to retrieve
     * @return mixed
     */
    public function __get($name) {
        return $this->getManifest()[$name];
    }
}

/**
 * Theme Exception Class
 * 
 * The custom exception class for the 
 * theme package - allows for an easy exception
 * management specifically for themes
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.9
 * @package EternityX\Theme
 */
class ThemeException extends Exception {
	
    const INVALID_THEME = 1;
    const INVALID_MANIFEST = 2;
    const INVALID_TPL = 3;
    const INCOMPATIBLE = 4;
    const INVALID_ICON = 5;
    const INVALID_MAN_KEY = 6;
    
    /**
     * Category to use in global exception management.
     * 
     * @var int 
     */
    public $category = 5;
    
    /**
     * List of exceptions and their descriptions
     * 
     * @var array 
     */
	protected $exceptions = array(
		1 => 'Theme does not contain all required files.',
		2 => 'Theme manifest is invalid/corrupt',
		3 => 'Theme template is invalid/corrupt',
        4 => 'Theme is incompatible with this version of EternityX',
        5 => 'Theme icon is incompatible.',
        6 => 'Invalid manifest key'
	);
}

/**
 * Stream Wrapper Implementation Class for Themes.
 * 
 * This allows easy access to manage themes through
 * a stream wrapper, in this case ex.theme:// - subject
 * to change later on.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.1.0
 * @package EternityX\Theme
 */
class ThemeStream extends Stream {
    
    /**
     * The protocol to use.
     * 
     * @var string 
     */
    public $protocol = 'ex.theme://';
    
    /**
     * The different routes to use.
     * 
     * @var array
     */
    protected $routes = [
        'current' => [
            'routes' => [
                'manifest' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getManifest']
                ],

                'icon' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getIcon']
                ],

                'template' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getTemplate']
                ],
                
                'stylesheet' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getStylesheet']
                ],
                
                'behavior' => [
                    'type' => self::ROUTE_VAR,
                    'route' => ['theme', 'getBehavior']
                ]
            ],
        ],
        
        'repo' => [
            'type' => self::ROUTE_FGC,
            'route' => 'http://eternityx.eternityincurakai.com/themes'
        ],
        
        'installed' => [
            'type' => self::ROUTE_DELEGATE,
            'route' => '\EternityX\Theme::getList'
        ]
    ];
}
