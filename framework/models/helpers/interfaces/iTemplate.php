<?php

interface iTemplate {
	//private $tpl;
	
	public function __get($var);
	public function __set($key, $val);
	
	public function addSection($parent, $section, $type, $value);
}
