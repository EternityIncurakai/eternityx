<?php

interface iCRUD {
	
	public function create(); 
	public function read();
	public function update();
	public function delete();
}
