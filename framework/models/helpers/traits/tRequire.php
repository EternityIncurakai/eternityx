<?php
/** 
 * Short Description.
 *
 * Long Description
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version 2.1.0
 * @package EternityX
 */

namespace EternityX\Traits;

trait tRequire {
    private function requireExtension($ext) {
        if(!extension_loaded($ext)) 
            trigger_error(__CLASS__.": Extension {$ext} must be installed and enabled.", E_USER_ERROR);
    }
    
    private function requireFile($file) {
        if(!file_exists($file))
            trigger_error(__CLASS__.": File {$file} must be present.", E_USER_WARNING);
    }
}