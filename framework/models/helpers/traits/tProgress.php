<?php

/* 
 * Short Description.
 *
 * Long Description
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version 2.1.0
 * @package EternityX
 */

namespace EternityX\Traits;

trait tProgress {
    private $steps = [];
    
    private function addStep(callable $function, $message = 'Getting Ready', $sleep = 1) {
        $this->steps[] = [
            'function' => $function,
            'message' => $message,
            'sleep' => $sleep
        ];
    }
    
    private function walk() {
        file_put_contents(EX_ROOT.'/.progress', json_encode([
            'progress' => 0,
            'message' => 'Getting Ready...'
        ]));
        sleep(1);
        foreach($this->steps as $key => $step) {
            $step['function']();
            file_put_contents(EX_ROOT.'/.progress', json_encode([
                'progress' => round((100 / count($this->steps)) * ($key+1)), 
                'message' => $step['message']
            ]));
            $this->progress = round((100 / count($this->steps)) * ($key+1));
            clearstatcache();
            sleep($step['sleep']);
        }
    }
}