<?php

trait tGetConfig {
	public function getConfig($section = null, $config) {
		return $this->config[$section][$config];
	}
}
