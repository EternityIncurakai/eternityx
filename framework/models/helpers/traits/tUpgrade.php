<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace EternityX;

trait tUpgrade {
   
    static public function checkForUpgrades() {
        foreach(self::getList() as $update) {
            $class = new self($update);
            $args = [ 'version' => $class->version, 'ex_version' => EX_VERSION ];
            
            $req = new HTTP\Request(rtrim($class->url, '/').'/check', $args);
            $response = $req->getResponse()->body;
            
            if($response == '1') {
                self::doUpgrade($update);
            }
        }
    }
    
    
    static public function doUpgrade($update) {
        $class = new self($update);
        $url = $class->url;
        unset($class);
        if(is_dir(self::DIRECTORY.'/'.$update)) {
            foreach(glob(self::DIRECTORY.'/'.$update.'/*') as $file) unlink($file);
            rmdir(self::DIRECTORY.'/'.$update);
        }
        
        if(copy(rtrim($url, '/').'/download', self::DIRECTORY.'/'.$update.'.phar')) {
            $class = new self($update);
            mail(EX_WEBMASTER_EMAIL, 'Upgrade Notice', "Successfully updated theme: {$update} to version {$class->version}!", "From: EternityX Mailer\n\n");
        }
    }
}