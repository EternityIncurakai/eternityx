<?php
trait tDebug {
	public function dumpObj() {
		$class = get_class($this);
		$classMethods = get_class_methods($this);
		$classAttributes = get_object_vars($this);

		echo "<h1>Debugging Object from class: ".$class."</h1>";

		// Attributes
		echo "<h2>Attributes</h2>";
		echo "<ul>";
		foreach($classAttributes as $key => $val) echo "<li><strong>{$key}</strong>: {$val}</li>";
		echo "</ul>";

		// Methods
		echo "<h2>Methods</h2>";
		echo "<ul>";
		foreach($classMethods as $key => $val) echo "<li><strong>{$key}</strong>: {$val}</li>";
		echo "</ul>";

		die;
	}
}
