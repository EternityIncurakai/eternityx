<?php

/* 
 * Short Description.
 *
 * Long Description
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version 2.1.0
 * @package EternityX
 */

namespace EternityX\Traits;

trait tCache {
    private $cache = [];
    
    private function cacheAddFile($file) {
        if(!array_key_exists($file, $this->cache))
                $this->cache[$file] = file_get_contents($file);
    }
    
    private function cacheGetFile($file) {
        if(array_key_exists($file, $this->cache))
                return $this->cache[$file];
    }
    
    private function cacheDelFile($file) {
        if(array_key_exists($file, $this->cache)) 
                unset($this->cache[$file]);
    }
    
    private function cacheClear() {
        unset($this->cache);
    }
}
