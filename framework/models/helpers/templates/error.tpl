=============================================================
EternityX v<{{tplex.echo:{EX_VERSION}/tplex}}> Internal Error
-------------------------------------------------------------
Level Raised: <{{tplex.sub:{error}/tplex}}> on <{{tplex.sub:{date}/tplex}}>
File: <{{tplex.sub:{file}/tplex}}> on line <{{tplex.sub:{line}/tplex}}>
-------------------------------------------------------------
Begin Debug Message:
<{{tplex.sub:{message}/tplex}}>
=============================================================
