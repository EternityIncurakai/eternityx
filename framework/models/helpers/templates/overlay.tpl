<div class="exw exw-overlay exw-<{{tplex.sub:{color}/tplex}}>" id="<{{tplex.sub:{id}/tplex}}>">
    <div class="exw-content">
        <h2 class="exw-head">
            <i class="fa fa-arrow-circle-o-left"></i> <{{tplex.sub:{heading}/tplex}}>
	</h2>
	<{{tplex.sub:{content}/tplex}}>
    </div>
</div>