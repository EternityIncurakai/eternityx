<a id="<{{tplex.sub:{id}/tplex}}>" class="exw exw-btn exw-<{{tplex.sub:{color}/tplex}}>" href="<{{tplex.sub:{href}/tplex}}>">
    <i class="fa fa-<{{tplex.sub:{icon}/tplex}}>"></i>
    <{{tplex.sub:{text}/tplex}}>
</a>