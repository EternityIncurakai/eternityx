<div style="width: 100% !important; min-width: 480px; color: #61B329; font-size: 50px; font-family: Tahoma; text-align: center;">EternityX Upgraded!</div>
<div style="width: 100% !important; min-width: 480px; color: #61B329; font-size: 30px; font-family: Tahoma; text-align: center;">EternityX has finished upgrading to v<{{tplex.sub:{version}/tplex}}></div><br><br>
<div style="text-align: center;">
    <div style="min-width: 480px; text-align: left; width: 50%; font-family: Tahoma; line-height:30px;">
        Your installation of EternityX just finished upgrading itself to version <{{tplex.sub:{version}/tplex}}>.  You've either upgraded manually or it has completed an automatic check and upgraded itself.  Should you encounter any problems with this version, please shoot us an email.  
        <br><br>Thanks, and Live the Eternity!<br> - Eternity Incurakai Studios<br>EternityX Development Team<br><a href="mailto:support@eternityincurakai.com">support@eternityincurakai.com</a></div>
</div>