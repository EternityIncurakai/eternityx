<?php

/**
 * Controller Package.
 * @todo document
 */

namespace EternityX;

class Controller { 
	public $callables = array();
	
	// register an ajax callback
	public function register($handler, $hook, $function) {
		$this->callables[strtolower($handler.".".$hook)] = $function;
	}
	
	// check if ajax hook exists
	public function exists($hook) {
		if(array_key_exists(strtolower($hook), $this->callables)) return true;
		else return false;
	}
	
	static function sanitize(&$var) {
		$var = trim($var);
		$var = htmlentities($var);
	}
	
	// call callback function
	public function call($hook, $returner = false) {
		if($this->exists(strtolower($hook))) {
			
			// setup response
			$return = array(
				'success' => '',
				'error' => array(
					'code' => '',
					'message' => '',
				),
			);
			
			try {
				$parameters = array();
				foreach($_POST as $key => $var) {
					if($key != "action") $parameters[$key] = $var;
				}
				$return['response'] = call_user_func_array($this->callables[strtolower($hook)], $parameters);
				$return['success'] = 'true';
			} catch(EXexception $e) {
				$return['success'] = 'false';
				$return['error']['code'] = $e->exEndCode;
				$return['error']['message'] = $e->exCodeExplain();
			}
			
			if(!$returner) {
				header('Content-Type: application/json');
				die(json_encode($return));
			} else {
				return $return;
			}
		}
		else die('no actions');
	}
	
}
