<?php
/**
 * Sessions Package
 * 
 * This file houses the sessions package, 
 * the core class and its respective future
 * exception class.  Used to maintain data across
 * requests.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 * 
 * @since v2.0.0
 * @package EternityX\Sessions
 */

namespace EternityX;

/**
 * The core sessions class.
 * 
 * This is the core sessions class, used to 
 * maintain and persist data relevant to a 
 * user across requests.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * @since v2.0.0
 * @package EternityX\Sessions
 */
final class Session implements \SessionHandlerInterface {
    
    /**
     * The key used to prevent CSRF attacks.
     * 
     * @var string
     */
	private $key;
    
    /**
     * Salt used in constructing the key.
     * 
     * @var string
     */
    static private $salt = 'ex2afd3-';
    
    /**
     * Constructs a new Session object.
     * 
     * This is the constructor for the session
     * class, it sets up keys to prevent CSRF 
     * attacks, etc.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @return void
     */
	public function __construct() {
        if(!array_key_exists('HTTP_USER_AGENT', $_SERVER)) $_SERVER['HTTP_USER_AGENT'] = 'eternity';
		$this->key = $_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].self::$salt.$_SERVER['REMOTE_ADDR'];
        $this->key = hash('sha512', $this->key);
	}
	
    /**
     * Required by the interface, however unnecessary
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @param string $savePath
     * @param string $sessionName
     * @return boolean
     */
	public function open($savePath, $sessionName) { 
        return true; 
    }
    
    /**
     * Required by the interface, however unnecessary
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @return boolean
     */
	public function close() { 
        return true; 
    }
	
    /**
     * Reads from the session
     * 
     * This function reads session data from the 
     * datastore and returns it according to the 
     * specified id.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @param string $id the id of the session
     * @return string
     */
	public function read($id) {
		$data = $GLOBALS['db']->prepare("SELECT `data` FROM `sessions` WHERE `id`=? AND `eKey`=?");
		$data->bindValue(1, $id);
		$data->bindValue(2, $this->key);
		$data->execute();
		return base64_decode($data->fetchColumn());
	}
	
    /**
     * Writes to the session.
     * 
     * This function writes data to the session
     * according to the specified id.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @param string $id the id of the session
     * @param string $data the session data
     * @return boolean
     */
	public function write($id, $data) {
		$write = $GLOBALS['db']->prepare("REPLACE INTO `sessions` (`id`, `data`, `date`, `eKey`, `userID`) VALUES (?,?,NOW(), ?, ?)");
		$write->bindValue(1, $id);
		$write->bindValue(2, base64_encode($data));
		$write->bindValue(3, $this->key);
		$write->bindValue(4, (isset($_SESSION['id'])) ? $_SESSION['id'] : null);
		$write->execute();
		
		if($write) return true;
		else return false;
	}
	
    /**
     * Destroys the current session.
     * 
     * This function destroys the current session
     * data from the datastore.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @param string $id the id of the session
     * @return boolean
     */
	public function destroy($id) {
		$delete = $GLOBALS['db']->prepare("DELETE FROM `sessions` WHERE `id`=?");
		$delete->bindValue(1, $id);
		$delete->execute();
		
		if($delete) return true;
		else return false;
	}
	
	/**
     * Garbage Collection function.
     * 
     * Called internally, this function removes
     * unnecessary and/or outdated session records 
     * from the database.
     * 
     * @author XenoK <xenok@eternityincurakai.com>
     * @since v2.0.0
     * 
     * @param int $seconds minimum number of seconds used to destroy the session
     * @return boolean
     */
	public function gc($seconds) {
		$delete = $GLOBALS['db']->prepare("DELETE FROM `sessions` WHERE `date` < (NOW() - INTERVAL ? SEC)");
		$delete->bindValue(1, $seconds);
		$delete->execute();
		
		if($delete) return true;
		else return false;
	}
}