DROP PROCEDURE IF EXISTS `signup`;

CREATE PROCEDURE `signup`(IN `$username` VARCHAR(15), IN `$password` VARCHAR(128), IN `$email` VARCHAR(128), IN `$age` TIMESTAMP, IN `$location` LONGTEXT, IN `$token` VARCHAR(128))
BEGIN


DECLARE $id BIGINT(20) UNSIGNED;

INSERT INTO `users` (`username`, `password`, `email`) VALUES ($username, $password, $email);
SET $id = LAST_INSERT_ID();

UPDATE `users_meta` SET `age`=$age, `location`=$location WHERE `id`=$id;
UPDATE `users_meta-email` SET `code`=$token, `codeGenerated`=NOW() WHERE `id`=$id;

END;
