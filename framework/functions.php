<?php

/**
 * Redirects the browser to the specified page.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.0 "Sunbeam"
 * 
 * @param string $page the page to redirect to.
 * @return void
 */
function redirect($page = '/') {
    if (!str_begins($page, '/'))
        $page = '/' . $page;
    header('Location: ' . $page);
    die;
}

function convert_links($string) {
    $string = preg_replace('%\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))%s', '<a href="$0">$0</a>', $string);
    $string = preg_replace("\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b", '<a href="mailto:$0">$0</a>', $string);
    return $string;
}

function test_error($error, callable $function) {
    if (DEBUG && isset($_REQUEST["test_error"]) && ($_REQUEST["test_error"] == $error || (is_array($_REQUEST["test_error"]) && in_array($error, $_REQUEST["test_error"])))) {
        return $function();
    } else
        return true;
}

function getJSON($file, $assoc = false, $depth = 512, $flags = null) {
    if (!file_exists($file)) {
        trigger_error('File ' . $file . ' does not exist.', E_USER_NOTICE);
        return;
    }

    return json_decode(file_get_contents($file), $assoc, $depth, $flags);
}

function send_stats($args) {
    $req = new \EternityX\HTTP\Request("http://stats.eternityx.eternityincurakai.com/new", ['type' => 'POST', 'body' => http_build_query([
            'version' => EX_VERSION,
            'system' => EX_TITLE . ' <' . EX_URL . '>',
            'owner' => EX_WEBMASTER_NAME . ' <' . EX_WEBMASTER_EMAIL . '>',
            'issue' => [
                'file' => $args['file'],
                'line' => $args['line'],
                'message' => $args['message'],
                'type' => $args['type']
            ]
    ])]);
}

function parse_change_file($file) {
    clearstatcache();
    $file = preg_replace("/#(.*)(\r)?\n/", "", file_get_contents($file));

    $changes = explode("\n", $file);
    $nchanges = [];
    foreach ($changes as $key => $val) {
        static $ukey = '';
        if ($ukey == '') {
            $ukey = $changes[$key];
        } else if (isset($changes[$key + 1]) && preg_match('/\[([a-zA-Z0-9]+)\]/', $changes[$key + 1])) {
            $ukey = $changes[$key + 1];
        } else if ($ukey != $val) {
            $result = [];
            // grouped syntaxes
            if (preg_match("/([a-zA-Z0-9]+)\:/", $val)) {
                $result['category'] = substr($val, 0, strpos($val, ":"));
                $result['command'] = substr($val, strpos($val, ":") + 1);
                $result['type'] = substr($result['command'], 0, strpos($result['command'], " "));
                $result['rest'] = substr($result['command'], strpos($result['command'], $result['type']) + strlen($result['type']) + 1);

                // grouped rename syntax
                if (strtolower($result['type']) === 'rename') {
                    $result['from'] = trim(trim(substr($result['rest'], 0, strpos($result['rest'], " ")), "\r"));
                    $result['to'] = trim(trim(substr($result['rest'], strpos($result['rest'], $result['from']) + strlen($result['from']) + 4, strpos(substr($result['rest'], strlen($result['from']) + 4), " ")), "\r"));
                    $result['ctype'] = trim(trim(substr($result['rest'], strpos($result['rest'], $result['to']) + strlen($result['to'])), "\r"));
                }

                // grouped add syntax
                if (strtolower($result['type']) === 'add') {
                    $result['column'] = trim(substr($result['rest'], 0, strpos($result['rest'], " ")), "\r");
                    $result['ctype'] = trim(substr($result['rest'], strpos($result['rest'], $result['column']) + strlen($result['column']) + 1), "\r");
                }

                // grouped delete syntax
                if (strtolower($result['type']) === 'delete') {
                    $result['column'] = $result['rest'];
                }
                unset($result['rest']);
                foreach ($result as $key => $val)
                    $result[$key] = str_replace(["\r", "\n"], "", $val);
                $category = $result['category'];
                unset($result['category']);
                unset($result['command']);
                $nchanges[str_replace(["[", "]", "\r", "\n"], "", $ukey)][$category][] = $result;
                // ungrouped syntaxes
            } else {
                $result['type'] = substr($val, 0, strpos($val, " "));

                // rename syntax
                if (strtolower($result['type']) === 'rename') {
                    $result['from'] = trim(trim(substr($val, strlen($result['type']) + 1, strpos($val, " TO ") - strlen($result['type'])), "\r"));
                    $result['to'] = trim(trim(substr($val, strlen($result['from'] . $result['type']) + 4), "\r"));
                }

                // delete syntax
                if (strtolower($result['type']) === 'delete') {
                    $result['table'] = trim(substr($val, strlen($result['type']) + 1), "\r");
                }
                $nchanges[str_replace(["[", "]", "\r", "\n"], "", $ukey)][] = $result;
            }
        }
    }
    return $nchanges;
}

function parse_types_file($file) {
    if (!file_exists($file) && !is_file($file))
        trigger_error('Invalid file presented to parse types file function.', E_USER_WARNING);

    $types = file_get_contents($file);
    $types = preg_replace(["/#(.*)\n/", "/\n\n/", "/\t/"], "", $types);
    $types = explode("\n", $types);

    foreach ($types as $type) {
        $type = trim($type);
        $key = substr($type, 0, strpos($type, " "));
        $val = substr($type, strpos($type, " "));
        $val = explode("|", trim($val));
        foreach ($val as $vala)
            $vala = trim($vala);
        if (count($val) == 1)
            $val = trim($val[0]);

        $atypes[$key] = $val;
    }
    return $atypes;
}

function parse_csv_file($file) {
    $file = file_get_contents($file);
    return explode(",", $file);
}

function ex_version_compare($version1, $version2, $operator) {
    $search = ['α', 'β', 'γ'];
    $replace = ['a', 'b', 'rc'];
    $version1 = str_replace($search, $replace, $version1);
    $version2 = str_replce($search, $replace, $version2);
    return version_compare($version1, $version2, $operator);
}

function addCron($frq, $cmd, $name = null, $amount = null) {
    if (EX_OS == 'Windows') {
        system('schtasks /create /tn "' . $name . '" /tr "' . $cmd . '" /sc ' . $frq . ' /mo ' . $amount);
    } else {
        $output = shell_exec('crontab -l');
        file_put_contents(ENV_ROOT . '/data/temp/crontab.txt', $output . $frq . ' ' . $cmd . PHP_EOL);
        system('crontab ' . ENV_ROOT . '/data/temp/crontab.txt');
        unlink(ENV_ROOT . '/data/temp/crontab.txt');
    }
}

function ex_check_upgrade() {
    $check = (bool) @file_get_contents(EX_UPGRADE_URL . EX_VERSION);
    $latests = (array) json_decode(file_get_contents(EX_UPGRADE_VER_URL));
    $latest = str_replace(['a', 'b'], ['α', 'ß'], $latests['version']);
    if ($check) {
        file_put_contents(ENV_ROOT . '/data/temp/upgradeavailable.txt', $latest);
        return true;
    }
}

/**
 * Determines whether or not a variable starts with the specified string.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $haystack the string to search through
 * @param string $needle the string to search for
 * @return boolean
 */
function str_begins($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 * Determines whether or not if the haystack ends with the specified character
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param type $haystack
 * @param type $needle
 * @return boolean
 */
function str_ends($haystack, $needle) {
    $len = strlen($haystack);
    if ($len == 0)
        return true;
    return (substr($haystack, -$length) === $needle);
}

/**
 * Determines whether or not a string has a lowercase char.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $haystack the string to check for a lowercase character
 * @return boolean
 */
function str_haslowchar($haystack) {
    if (!is_string($haystack)) {
        is_type_error(__FUNCTION__, $haystack, 1);
        return false;
    }
    return (preg_match("/([a-z]+)/", $haystack));
}

/**
 * @deprecated 2.0.12 Removed in favor of str_haslowchar
 * @see str_haslowchar() function documentation
 */
function hasLowerChar($haystack) {
    deprecated(__FUNCTION__, 'str_haslowchar', '2.0.12');
    return str_haslowchar($haystack);
}

/**
 * Determines whether or not a string has a uppercase char.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $haystack
 * @return boolean
 */
function str_hasupchar($haystack) {
    if (!is_string($haystack)) {
        is_type_error(__FUNCTION__, $haystack, 1);
        return false;
    }
    return (preg_match("/([A-Z]+)/", $haystack));
}

/**
 * @deprecated 2.0.12 Removed in favor of str_hasupchar
 * @see str_hasupchar() Function documentation
 */
function hasUpperChar($haystack) {
    deprecated(__FUNCTION__, 'str_hasupchar', '2.0.12');
    return str_hasupchar($haystack);
}

/**
 * Determines whether or not a string contains an integer-like value.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam"
 * 
 * @param string $haystack the string to search through
 * @return boolean
 */
function str_hasinteger($haystack) {
    if (!is_string($haystack)) {
        is_type_error(__FUNCTION__, $haystack, 1);
        return false;
    }
    return (preg_match("/([0-9]+)/", $haystack));
}

/**
 * @deprecated 2.0.12 In favor of str_hasinteger
 * @see str_hasinteger() function documentation
 */
function hasNumber($haystack) {
    deprecated(__FUNCTION__, 'str_hasinteger', '2.0.12');
    return str_hasinteger($haystack);
}

/**
 * Determines whether or not a string has a special character.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $haystack the string to search through
 * @return boolean
 */
function str_hasspecial($haystack) {
    if (!is_string($haystack)) {
        is_type_error(__FUNCTION__, $haystack, 1);
        return false;
    }
    return (preg_match('/([^a-zA-Z\d])/', $haystack));
}

/**
 * @deprecated 2.0.12 in favor of str_hasspecial
 * @see str_hasspecial() function documentation
 */
function hasSpecialChar($haystack) {
    deprecated(__FUNCTION__, 'str_hasspecial', '2.0.12');
    return str_hasspecial($haystack);
}

/**
 * Determines whether or not the length of a string or an integer is inbetween or equal
 * to two values.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param mixed $haystack the integer to check
 * @param int $min the minimum value
 * @param int $max the maximum value
 * @return boolean
 */
function in_range($haystack, $min, $max) {
    // check the haystack
    if (is_string($haystack))
        $haystack = strlen($haystack);
    if (!is_integer($haystack)) {
        is_type_error(__FUNCTION__, $haystack, 1, 'integer/string');
        return false;
    }

    // check mins and maxes
    if (!is_integer($min)) {
        is_type_error(__FUNCTION__, $min, 'integer');
        return false;
    }
    if (!is_integer($max)) {
        is_type_error(__FUNCTION__, $max, 'integer');
        return false;
    }
    if ($min > $max) {
        trigger_error('<strong>' . __FUNCTION__ . '():</strong> parameter 1 cannot be greater than parameter 2');
        return false;
    }

    return (($haystack >= $min) && ($haystack <= $max));
}

/**
 * @deprecated 2.0.12 in favor of in_range
 * @see in_range() function documentation
 */
function isRange($haystack, $min, $max) {
    deprecated(__FUNCTION__, 'is_range', '2.0.12');
    return in_range($haystack, $min, $max);
}

/**
 * @deprecated 2.0.12 the same can be accomplished with pre established methods in php
 * @see http://www.php.net/manual/en/language.types.type-juggling.php#language.types.typecasting php type casting
 */
function toBool($number) {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    if ($number === "0" || $number === "false" || $number === "error")
        return false;
    else
        return true;
}

/**
 * Deprecates a function
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $func the deprecated function
 * @param string $newfunc the replaced function
 * @param string $version the version in which it was deprecated
 */
function deprecated($func, $newfunc, $version) {
    $message = "<strong>{$func}</strong> has been deprecated in favor of <strong>{$newfunc}</strong> as of version {$version}";
    trigger_error($message, E_USER_DEPRECATED);
}

/**
 * Triggers a uniform type error.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.12 "Sunbeam 12"
 * 
 * @param string $func the calling function
 * @param string $param the invalid parameter
 * @param int $num the index of the parameter
 * @param string $type the type expected
 */
function is_type_error($func, $param, $num, $type = 'string') {
    $message = "<strong>{$func}():</strong> expects parameter {$num} to be of type {$type}. " . gettype($param) . " was given";
    trigger_error($message, E_USER_WARNING);
}

/**
 * @deprecated 2.0.12 outdated
 */
function ex_construction(callable $devsonly = null) {
    if (eAuth::authorize()->isRank(eUser::RANK_ADMIN) && isset($devsonly))
        $devsonly();
    else {
        ?>
        <div class="construction">
            <i class="fa fa-warning"></i>
            <div class="construction-content">
                <h1>Under Construction</h1>
                <p>This area is currently not available as it is being worked on by one of our developers.</p>
            </div>
        </div>
        <?php
    }
}

/**
 * Similar to file_get_contents(), but instead with the post method.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.0 "Sunbeam"
 * 
 * @param string $url
 * @param array $vars
 * @return mixed
 */
function file_post_contents($url, array $vars) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

/**
 * @deprecated 2.0.12 outdated
 */
function getuser() {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    global $user;
    return $user;
}

/**
 * @deprecated 2.0.12 outdated
 */
function getuserID() {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    global $user;
    return $user->id;
}

/**
 * @deprecated 2.0.12 outdated
 */
function htmlist($list) {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    echo '<ul>' . PHP_EOL;
    foreach ($list as $key => $value) {
        echo '<li>' . $value . '</li>' . PHP_EOL;
    }
    echo '</ul>' . PHP_EOL;
}

/**
 * Searches a multidimensional array for a key.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.0 "Sunbeam"
 * 
 * @param string $needle the key to search for
 * @param array $haystack the array to search through
 * @param boolean $strict whether or not to use strict mode
 * @return boolean
 */
function in_array_r($needle, array $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

/**
 * @deprecated outdated
 */
function ob_store($content) {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    ob_start();
    echo $content->returner();
    $return = ob_get_clean();
    return $return;
}

/**
 * @deprecated 2.0.12 outdated
 */
function online() {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    global $user, $pdo;

    if (isset($user)) {
        $seen = $pdo->prepare("UPDATE `users` SET `lastseen`=NOW() WHERE `id`=?");
        $seen->bindValue(1, $user->id);
        $seen->execute();
    }
}

/**
 * Recursively stripslashes() the values of a multidimensional array.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.0 "Sunbeam"
 * 
 * @param array $arr the array to stripslash through
 */
function stripslashes_array(array &$arr) {
    foreach ($arr as $k => &$v) {
        $nk = stripslashes($k);
        if ($nk != $k) {
            $arr[$nk] = &$v;
            unset($arr[$k]);
        }
        if (is_array($v)) {
            stripslashes_array($v);
        } else {
            $arr[$nk] = stripslashes($v);
        }
    }
}

/**
 * Recursively trims the values of an object.
 * 
 * @author XenoK <xenok@eternityincurakai.com>
 * 
 * @since 2.0.0 "Sunbeam"
 * 
 * @param object $obj the object to trim
 * @return object
 */
function trim_object($obj) {
    foreach ((array) $obj as $key => $value) {
        $obj->{$key} = trim($value);
    }
    return $obj;
}

/**
 * @deprecated 2.0.12 outdated
 */
function var_check($var, $replacement) {
    deprecated(__FUNCTION__, 'none', '2.0.12');
    if ($var)
        return $var;
    else
        return $replacement;
}

function glob_recursive($pattern, $flags = 0) {
    $files = glob($pattern, $flags);

    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, glob_recursive($dir . '/' . basename($pattern), $flags));
    }

    return $files;
}

function rmdir_recursive($dir) {
    foreach (scandir($dir) as $file) {
        if ('.' === $file || '..' === $file)
            continue;
        if (is_dir("$dir/$file"))
            rmdir_recursive("$dir/$file");
        else {
            if (!unlink("$dir/$file"))
                return false;
        }
    }
    rmdir($dir);
    return true;
}

function session_force_var($key, $val) {
    $_SESSION[$key] = $val;
    session_write_close();
    session_start();
}
