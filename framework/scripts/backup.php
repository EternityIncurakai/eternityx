#!/usr/bin/php
<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define('ROOT', dirname(dirname(__DIR__)));
$settings = parse_ini_file(ROOT.'/application/configs/application.ini');
$filename = ROOT.'/framework/data/backups/'.date('Y-m-d').'.gz';
$command = 'mysqldump --opt -h '.$settings['dbhost'].' -u '.$settings['dbuser'].' --password='.$settings['dbpass'].' '.$settings['dbname'].' | gzip > '.$filename;
system($command);
