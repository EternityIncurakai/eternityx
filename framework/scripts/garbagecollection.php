#!/usr/bin/php

<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('ROOT', dirname(dirname(__DIR__)));
define('TMPDIR', ROOT.'/framework/data/temp');
define('BACKUPDIR', ROOT.'/framework/data/backups');
define('BACKUPINT', strtotime('-1 month'));

// remove temporary files
foreach(glob(TMPDIR.'/*.php') as $tempfile) unlink($tempfile); 

// remove backups over a month old
foreach(glob(BACKUPDIR.'/*.gz') as $backup) {
    if(strtotime(basename($backup, ".gz")) < BACKUPINT) unlink($backup); 
}

// clean error log
unlink(ROOT.'/framework/data/logs/errors.log');
file_put_contents(ROOT.'/framework/data/logs/errors.log', 'Cleaned on: '.date('Y-m-d').PHP_EOL);