#!/usr/bin/php
<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$_ENV['scriptstart'] = true;
$_SERVER['HTTP_HOST'] = str_replace(['http://', 'https://'], '', parse_ini_file(dirname(dirname(__DIR__)).'/application/configs/application.ini')['url']);
$_SERVER['REMOTE_ADDR'] = '127.0.0.0';
$_SERVER['REQUEST_URI'] = '/';
$_SERVER['REQUEST_METHOD'] = 'POST';
include dirname(dirname(__DIR__)).'/index.php';
if(ex_check_upgrade() && EX_AUTOUPDATE && (!preg_match('/(α|ß|Γ)/', file_get_contents(ENV_ROOT.'/data/temp/upgradeavailable.txt')) || DEBUG)) {
    include EX_ROOT.'/maintenance/upgrading/utilities/setup.php';
} else if(ex_check_upgrade()) {
    $endpoint = 'http://upgrade.eternityincurakai.info/core';
    $tmppoint = ENV_ROOT.'/data/temp/ex.zip';
    copy($endpoint, $tmppoint);
}

EternityX\Theme::checkForUpgrades();

die;