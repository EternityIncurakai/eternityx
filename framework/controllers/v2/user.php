<?php

namespace EternityX\Api\v2;

class User extends \EternityX\API {
	private function validUser($id) {
		if(!filter_var($id, FILTER_VALIDATE_INT)) return false;
		$exists = $this->db->prepare("SELECT COUNT(`id`) FROM `users` WHERE `id`=?");
		$exists->bindValue(1, $id);
		$exists->execute();
		$exists->bindColumn(1, $exists, \PDO::PARAM_BOOL);
		$exists->fetchAll();
		
		if(!$exists) return false;
		return true;
	}
	
	public function __construct($request) {
		parent::__construct($request);
		header('Content-Type: application/json');
	}
	
	public function get() {
		if(!$this->validUser($this->args[0]) && $this->args[0] !== 0) die('null');
		
		if($this->args[0] == 0 && !empty($_SESSION)) $this->args[0] = $_SESSION['id']; 
		else die(json_encode(null)); 
		
		$data = $this->db->prepare("SELECT * FROM `users` WHERE `id`=?");
		$data->bindValue(1, $this->args[0]);
		$data->execute();
		$data = $data->fetch(\PDO::FETCH_ASSOC);
		unset($data['password']);
		die(json_encode($data));
	}
    
    public function getAvatar() {
        header('Content-Type: image/png');
        $user = new \EternityX\User(null, $this->args);
        die($user->avatar); 
    }
    
    public function postAvatar() {
        $GLOBALS['user']->uploadAvatar();
    }
	
	public function postLogin() {
        header('Content-Type: application/json');
        $response = array();
        $response['response'] = \EternityX\User::login($_POST['username'], $_POST['password']);
        $response['pass'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
        die(json_encode($response));
    }

    public function postLogout() {
        header('Content-Type: application/json');
        $response = array();
        $response['response'] = \EternityX\eUser::logout();
        die(json_encode($response));
    }
    
    public function postSignup() {
        header('Content-Type: application/json');
       $response['response'] = \EternityX\User::signup($_POST['username'], $_POST['password'], $_POST['rpassword'], $_POST['email'], $_POST['age'], $_POST['location'], null);
       die(json_encode($response));
               
    }
}
