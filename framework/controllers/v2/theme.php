<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace EternityX\Api\v2;

class Theme extends \EternityX\API {
    private $theme;
    public function __construct($request) {
        parent::__construct($request);
        $this->theme = new \EternityX\Theme(EX_THEME);
    }
    
    public function getStyle() {
        header('Content-Type: text/css');
        die($this->theme->getStylesheet());
    }
    
    public function getTpl() {
        header('Content-Type: text/template');
        die($this->theme->getTemplate());
    }
    
    public function getJs() {
        header('Content-Type: text/javascript');
        die($this->theme->getBehavior());
    }
    
    public function getManifest() {
        header('Content-Type: application/json');
        die(json_encode($this->theme->getManifest()));
    }
    
    public function getIcon() {
        header('Content-Type: image/png');
        die($this->theme->getIcon());
    }
}

