<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Upgrading EternityX</title>
        <style><{{tplex.include:{/maintenance/upgrading/interface/style.css}/tplex}}></style>
    </head>
    <body>
        <div id="container">
            <h1>Upgrade Maintenance</h1>
            <p><{{tplex.echo:{EX_TITLE}/tplex}}> is currently undergoing filesystem maintenance as EternityX, it's backing framework is recieving an upgrade.  Please attempt to reload the page you requested within a couple minutes.  If this message persists for more than fifteen minutes, please notify <a href="mailto://<{{tplex.echo:{EX_WEBMASTER_EMAIL}/tplex}}>"><{{tplex.echo:{EX_WEBMASTER_EMAIL}/tplex}}></a></p>
        </div>
        <div class="dots">
            <div class="blinkeydot"></div>
            <div class="blinkeydot"></div>
            <div class="blinkeydot"></div>
        </div>
    </body>
</html>