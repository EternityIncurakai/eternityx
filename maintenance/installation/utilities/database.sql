
CREATE TABLE IF NOT EXISTS `adminchat` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS `bans` (
  `banID` bigint(20) NOT NULL,
  `bannerID` bigint(20) NOT NULL,
  `reason` text NOT NULL,
  `endDate` date NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `changepassword_codes` (
  `username` text NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `connect_ebox` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to` text NOT NULL,
  `from` text NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `read` set('yes','no') NOT NULL DEFAULT 'no',
  `type` set('message','friend') NOT NULL DEFAULT 'message',
  `replyID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `message` (`message`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=210 ;

CREATE TABLE IF NOT EXISTS `connect_ebox_reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` bigint(20) NOT NULL,
  `reason` set('inappropriate','harassment','negativity','violation of rules') NOT NULL,
  `details` text NOT NULL,
  `ruID` bigint(20) NOT NULL,
  `ruMessage` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `connect_echat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `content` (
  `name` text NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `eauthkeys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `eKey` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `errors` (
  `error` text NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL,
  `sentID` bigint(20) unsigned NOT NULL,
  `replyID` bigint(20) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`,`sentID`,`replyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `maintenance` (
  `key` text NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  `to` text NOT NULL,
  `from` text NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `reply` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;

CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authorID` bigint(20) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category` enum('new-releases','developments','announcements') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'announcements',
  PRIMARY KEY (`id`),
  KEY `userID` (`authorID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=116 ;

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(128) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` longtext NOT NULL,
  `eKey` char(128) NOT NULL,
  `userID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `last_updated` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `support` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` set('open','closed','answered','pending deletion') NOT NULL DEFAULT 'open',
  `title` text NOT NULL,
  `description` text NOT NULL,
  `category` text NOT NULL,
  `answerID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

CREATE TABLE IF NOT EXISTS `support_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `questionID` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the id of the user',
  `username` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'the user''s name',
  `password` char(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'the user''s password',
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'the user''s email',
  `permissions` tinyint(5) NOT NULL DEFAULT '1' COMMENT 'user permissions determined for rankings',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'the date the user registered',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `login` (`username`,`password`),
  KEY `Alternate Login` (`email`,`password`),
  KEY `userID` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;



CREATE TABLE IF NOT EXISTS `users_friends` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `requesterID` bigint(20) unsigned NOT NULL,
  `friendID` bigint(20) unsigned NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

CREATE TABLE IF NOT EXISTS `users_ips` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastAccessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `count` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

CREATE TABLE IF NOT EXISTS `users_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `age` datetime NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` longblob NOT NULL,
  `website` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=157 ;

CREATE TABLE IF NOT EXISTS `users_meta-email` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `welcomed` tinyint(1) NOT NULL DEFAULT '0',
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `code` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `codeGenerated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `announcements` tinyint(1) NOT NULL DEFAULT '1',
  `connections` tinyint(1) NOT NULL DEFAULT '1',
  `posts` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=157 ;

CREATE TABLE IF NOT EXISTS `users_meta-password` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'user''s ID',
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'the last time the user updated their password',
  `count` bigint(20) NOT NULL DEFAULT '0' COMMENT 'how many times the user updated their password',
  `temp` char(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tempStamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=157 ;

CREATE TABLE IF NOT EXISTS `users_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=265 ;

CREATE TABLE IF NOT EXISTS `users_posts_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `postID` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `comment` (`comment`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

CREATE TABLE IF NOT EXISTS `users_reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ruID` bigint(20) NOT NULL,
  `userID` bigint(20) NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE PROCEDURE `preauth`(IN `$pUsername` VARCHAR(40))
BEGIN
	
	DECLARE pUsername2 VARCHAR(15);
	DECLARE pID BIGINT(20);
	DECLARE pEnabled TINYINT(1);
	DECLARE pPermissions BIGINT(20);
	DECLARE $password VARCHAR(128);
	DECLARE $throttled TIMESTAMP;
	SET pID = (SELECT `id` FROM `users` WHERE `username`=$pUsername OR `email`=$pUsername);
	SET pUsername2 = (SELECT `username` FROM `users` WHERE `id`=pID);
	SET pEnabled = (SELECT `enabled` FROM `users` WHERE `id`=pID);
	SET pPermissions = (SELECT `permissions` FROM `users` WHERE `id`=pID);
	SET $password = (SELECT `password` FROM `users` WHERE `id`=pID);
	SET $throttled = (SELECT `throttled` FROM `users` WHERE `id`=pID);
	
	SELECT pUsername2, pID, pEnabled, pPermissions, $password, $throttled;
	
	
END;

CREATE PROCEDURE `signup`(IN `$username` VARCHAR(15), IN `$email` VARCHAR(128), IN `$location` LONGTEXT, IN `$age` TIMESTAMP, IN `$avatar` LONGBLOB, IN `$token` CHAR(128))
BEGIN

# USER ID
DECLARE $id BIGINT(20) UNSIGNED;

# ONLY INSERT IF USER DOESNT ALREADY EXIST
IF (SELECT COUNT(`id`) FROM `users` WHERE `username`=$username) = 0 THEN
	IF (SELECT COUNT(`id`) FROM `users` WHERE `email`=$email) = 0 THEN
		
		# main insertion
		INSERT INTO `users` (`username`, `email`) VALUES ($username, $email);
		
		SET $id = LAST_INSERT_ID();
		
		# CREATE META ROWS
		UPDATE `users_meta` SET `location`=$location, `age`=$age, `avatar`=$avatar WHERE `id`=$id;
		UPDATE `users_meta-email` SET `code`=$token, `codeGenerated`=NOW() WHERE `id`=$id;
		SELECT 1, $id;
	ELSE
        	SELECT 0;
	END IF;
ELSE
	SELECT 0;
END IF;
END;

CREATE FUNCTION `online`(`$uid` BIGINT(20) UNSIGNED) RETURNS tinyint(1)
    NO SQL
BEGIN
DECLARE $time TIMESTAMP;
SET $time = DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 2 MINUTE);

IF (SELECT `date` FROM `sessions` WHERE `userID`=$uid ORDER BY `date` DESC LIMIT 1) > $time THEN
	RETURN TRUE;
ELSE 
	RETURN FALSE;
END IF;
END;

CREATE TRIGGER `meta_insert` AFTER INSERT ON `users`
 FOR EACH ROW BEGIN
INSERT INTO `users_meta` (`id`) VALUES (NEW.`id`);
INSERT INTO `users_meta-password` (`id`) VALUES (NEW.`id`);
INSERT INTO `users_meta-email` (`id`) VALUES (NEW.`id`);
END;

CREATE PROCEDURE `login`(IN `pUsername` VARCHAR(40), IN `pPassword` CHAR(128), IN `pIP` TEXT)
BEGIN

DECLARE `pUid` BIGINT(20);
SET `pUid` = (SELECT `id` FROM `users` WHERE `username`=pUsername OR `email`=pUsername);

IF (SELECT `id` FROM `users_meta-password` WHERE `temp`=pPassword AND `tempStamp`>DATE_SUB(NOW(), INTERVAL 1 DAY) AND `id`=pUid) != '' THEN
	UPDATE `users` SET `password`=pPassword WHERE `id`=pUid;
        UPDATE `users_meta-password` SET `temp`='', `tempStamp`='' WHERE `id`=pUid;
END IF;

IF (SELECT `id` FROM `users` WHERE (`username`=pUsername OR `email`=pUsername) AND `password`=pPassword) != '' THEN
	SELECT 1 AS `bool`;
	
	IF (SELECT COUNT(*) FROM `users_ips` WHERE `userID`=pUid AND `address`=pIP LIMIT 1) = 0 THEN
		INSERT INTO `users_ips` (`userID`, `address`) VALUES (pUid, pIP);
	ELSE
		UPDATE `users_ips` SET `count`=count + 1 WHERE `userID`=pUid AND `address`=pIP;
	END IF;
ELSE	
	UPDATE `users` SET `throttled`=NOW() WHERE `id`=pUid; 
        SELECT 0 AS `bool`;	
END IF;
END;