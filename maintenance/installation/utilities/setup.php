<?php

// Setup Database
database : {
	try {
		$db = new stdClass();
	
		$db->host = $_POST['dbhost'];
		$db->name = $_POST['dbname'];
		$db->user = $_POST['dbuser'];
		$db->pass = $_POST['dbpass'];
		
		
		// attempt to connect
		$dsn = 'mysql:host='.$db->host.';dbname='.$db->name;
		$pdo = new \PDO($dsn, $db->user, $db->pass);
		
		$pdo->query(file_get_contents(EX_ROOT.'/maintenance/installation/utilities/database.sql'));
		
		// setup admin account
		$usr = new stdClass();
		$usr->username = $_POST['adminname'];
		$usr->password = $_POST['adminpass'];
		
		// check ranges
		if(!filter_var(strlen($usr->username), FILTER_VALIDATE_INT, array( 'options' => array('min_range' => 5, 'max_range' => 15)))) throw new Exception('Invalid Username Length');
		if(!filter_var(strlen($usr->password), FILTER_VALIDATE_INT, array( 'options' => array('min_range' => 8, 'max_range' => 18)))) throw new Exception('Invalid Password Length');
		
		// check chars
		if(!ctype_alnum($usr->username)) throw new Exception('Username contains invalid chars');
		if(!preg_match('/([a-z]+)/', $usr->password)) throw new Exception('Password must contain 1 lowercase char');
		if(!preg_match('/([A-Z]+)/', $usr->password)) throw new Exception('Password must contain 1 uppercase char');
		if(!preg_match('/([0-9]+)/', $usr->password)) throw new Exception('Password must contain 1 number');
		if(!preg_match('/([^a-zA-Z\d])/', $usr->password)) throw new Exception('Password must contain 1 special char');
		
		// setup password
		$usr->password = password_hash($usr->password, PASSWORD_DEFAULT, array('cost' => 13));
		
		// signup
		$signup = $pdo->prepare("INSERT INTO `users` (`username`, `password`, `permissions`) VALUES (?,?,5)");
		$signup->bindValue(1, $usr->username);
		$signup->bindValue(2, $usr->password);
		$signup->execute();	
	} catch(PDOException $e) {
		die('Database error: '.$e->message);
	} catch(Exception $e) {
		die($e->getMessage());
	}
}


images : {
    @mkdir(dirname(dirname(APP_ROOT)));
    @mkdir(dirname(APP_ROOT));
    @mkdir(APP_ROOT);
    @mkdir(APP_ROOT.'/assets');
    @mkdir(APP_ROOT.'/assets/img');
	$exts = array('png','gif','jpg','jpeg','bmp','ico');
	$types = array('image/png', 'image/gif', 'image/jpg', 'image/jpeg', 'image/bmp', 'image/x-icon', 'image/vnd.microsoft.icon');
	
	logo : {
		try {
			$logo = $_FILES['logo'];
			if(!in_array(pathinfo($logo['name'], PATHINFO_EXTENSION), $exts)
			|| !in_array($logo['type'], $types)) {throw new Exception('Image type not supported');}
		
			// attempt to move the file
			//unlink(APP_ROOT.'/assets/img/logo.png');
			if(!move_uploaded_file($logo['tmp_name'], APP_ROOT.'/assets/img/logo.png')) throw new Exception('Could not move image');
		
			// compress 
			if($logo['type'] == 'image/jpeg' || $logo['type'] == 'image/jpg') $image = imagecreatefromjpeg(APP_ROOT.'/assets/img/logo.png');
			else if($logo['type'] == 'image/png') $image = imagecreatefrompng(APP_ROOT.'/assets/img/logo.png');
			imagealphablending($image, false);
			imagesavealpha($image, true);
			imagepng($image, APP_ROOT.'/assets/img/logo.png', 9);
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}
	
	favicon : {
		try {
			$favicon = $_FILES['favicon'];
			if(!in_array(pathinfo($favicon['name'], PATHINFO_EXTENSION), $exts)
			|| !in_array($favicon['type'], $types)) throw new Exception('Image type not supported');
			
			//unlink(APP_ROOT.'/assets/img/favicon.png');
			if(!move_uploaded_file($favicon['tmp_name'], APP_ROOT.'/assets/img/favicon.png')) throw new Exception('Could not move favicon');
			
			// compression not needed
		} catch(Exception $e) {
			die($e->getMessage());
		}
	}
}


exconfigs : {
	
	define('URL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'On') ? 'https://' : 'http://').$_SERVER['HTTP_HOST']);
	$exini = new \EternityX\TPLEX(EX_ROOT.'/maintenance/installation/utilities/exskeleton.ini');
	mkdir(APP_ROOT.'/configs');
	if(!@file_put_contents(APP_ROOT.'/configs/application.ini', $exini))
	{
		echo "Sorry, I wasn't allowed to save the application script.  Save the following to the file \"".APP_ROOT."/configs/application.ini\":".PHP_EOL."<textarea>".$exini."</textarea>";
	}
}


cronjobs : {
    @mkdir(ENV_ROOT.'/data/temp');
        
        foreach(glob(ENV_ROOT.'/scripts/*.php') as $filename)
            addCron('0 0 * * *', $filename);
}

mkdir(APP_ROOT.'/assets/css');
mkdir(APP_ROOT.'/controllers');
mkdir(APP_ROOT.'/models');


die('Installed! <a href="/">Click here to get started!</a>');
