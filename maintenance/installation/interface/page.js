$(function() {
	$("fieldset").hide();
	$("fieldset:first").show();
	
	$("nav ul li:first").addClass('active');
	
	
	
	$("fieldset button").click(function(e) {
		e.preventDefault();
		var valid = true;
		
		$(this).parent().find("input,select,textarea").each(function() {
			$(this).keyup();
			if($(this).data("valid") === "no") { valid = false; }
		});
		
		if(valid) {
			$(this).parent().hide();
			$(this).parent().next().show();
			$("li[data-value='"+$(this).parent().next().data('value')+"']").addClass('active');
			$(this).parent().next().find('.field:first-child input, .field:first-child select, .field:first-child textarea').focus();
			if($(this).parent().data('value') == 4) {
				var form = new FormData();
				form.append("action", 'setup');
				$("input:not([type='file']),textarea,select").each(function() {
					form.append($(this).attr('id'), $(this).val());
				});
				
				$("input[type='file']").each(function() {
					form.append($(this).attr('id'), this.files[0]);
				});
				
				$.ajax({
				    url: "",
				    type: "POST",
				    data: form,
				    processData: false,
				    contentType: false,
				    success: function (res) {
				      $("fieldset:last").html(res); 
				    }
				  });
			}
		}
	});
	
	$("input,textarea,select").keyup(function() {
		var length = $(this).val().length;
		var minlength = $(this).data('minlength');
		var maxlength = $(this).data('maxlength');
		
		if($(this).attr('id') == 'adminpass') {
			if(!$(this).val().match(/([a-z]+)/g)) $(this).data('valid', 'no').trigger('changeData');
			else if(!$(this).val().match(/([A-Z]+)/g)) $(this).data('valid', 'no').trigger('changeData');
			else if(!$(this).val().match(/([0-9]+)/g)) $(this).data('valid', 'no').trigger('changeData');
			else if(!$(this).val().match(/([^a-zA-Z\d])/g)) $(this).data('valid', 'no').trigger('changeData');
			else if(length > maxlength || length < minlength) $(this).data('valid', 'no').trigger('changeData');
			else $(this).data('valid', 'yes').trigger('changeData');
		} else {
		
			if(length > maxlength || length < minlength) {
				$(this).data('valid', 'no').trigger('changeData');
			} else {
				$(this).data('valid', 'yes').trigger('changeData');
			}
		}
	});
	
	$("input,textarea,select").on("changeData", function() {
		$(this).attr("data-valid", $(this).data('valid'));
	});
	$("#test").unbind();
	$("#test").click(function(e) {
		e.stopPropagation();
		e.preventDefault();
		
		
		$.post("", {
			action:"dbtest",
			name:$("#dbname").val(),
			host:$("#dbhost").val(),
			user:$("#dbuser").val(),
			pass:$("#dbpass").val()
		}, function(data, status) {
			alert(data);
		});
	});
	
	$("#logo,#favicon").change(function(e) {
		var img = document.createElement("img");
		var file = document.getElementById($(this).attr('id')).files[0];
		img.file = file;
		$("label[for='"+$(this).attr('id')+"']").html(img);
		var reader = new FileReader();
    	reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
    	reader.readAsDataURL(file); 
	});
});
