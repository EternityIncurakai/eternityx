<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Install</title>
	
	<style>
		<{{tplex.include:{/maintenance/installation/interface/page.css}/tplex}}>
	</style>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script>
		<{{tplex.include:{/maintenance/installation/interface/page.js}/tplex}}>
	</script>
</head>
<body>
	<header>
		<h1>Installing EternityX</h1>
		<p>You're installing the world's newest, most advanced web framework.</p>
	</header>
	
	<form enctype="multipart/form-data">
		<nav>
			<ul>
				<li data-value="1">Step One: Branding</li>
				<li data-value="2">Step Two: Database</li>
				<li data-value="3">Step Three: Images</li>
				<li data-value="4">Step Four: Misc</li>
				<li data-value="5">Step Five: Install!</li>
			</ul>
		</nav>
		<fieldset data-value="1">
			<div class="field">
				<label for="name">Name:</label>
				<input data-minlength="3" data-maxlength="30" type="text" placeholder="Eternity Incurakai" id="name" />
			</div>
		
			<div class="field">
				<label for="description">Description:</label>
				<textarea data-minlength="5" data-maxlength="250" id="description" placeholder="Eternity Incurakai is the next generation platform of the modern web...."></textarea>
			</div>
			
			<div class="field">
				<label for="creator">Creator:</label>
				<input type="text" id="creator" placeholder="Eternity Incurakai Studios">
			</div>
			<button>Next</button>
		</fieldset>
		
		<fieldset data-value="2">
			<div class="field">
				<label for="dbhost">Host:</label>
				<input type="text" id="dbhost" placeholder="localhost" />
			</div>
			
			<div class="field">
				<label for="dbname">Database:</label>
				<input type="text" id="dbname" placeholder="eternityx">
			</div>
			
			<div class="field">
				<label for="dbuser">Username:</label>
				<input type="text" id="dbuser" placeholder="xenok">
			</div>
			
			<div class="field">
				<label for="dbpass">Password:</label>
				<input type="password" id="dbpass" placeholder="dbpass123">
				<button id="test">Test Connection</button>
			</div>
			
			<button>Next</button>
		</fieldset>
		
		<fieldset data-value="3">
			<div class="field">
				<label for="logo" class="img"><img alt="logo"></label>
				<div>
					<h3>Logo</h3>
					<input type="file" id="logo" />
				</div>
			</div>
			
			<div class="field">
				<label for="favicon" class="img"><img alt="favicon"></label>
				<div>
					<h3>Favicon</h3>
					<input type="file" id="favicon">
				</div>
			</div>
			
			<button>Next</button>
		</fieldset>
		
		<fieldset data-value="4">			
			<div class="field">
				<label for="adminname">Admin Username:</label>
				<input placeholder="XenoK" id="adminname" type="text" data-minlength="5" data-maxlength="15">
			</div>
			
			<div class="field">
				<label for="adminpass">Admin Password:</label>
				<input placeholder="Password123!" id="adminpass" type="password" data-minlength="8" data-maxlength="18">
			</div>
			
			<button>Next</button>
		</fieldset>
		<fieldset data-value="5">
		</fieldset>
	</form>
</body>
</html>