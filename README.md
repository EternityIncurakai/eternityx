[![Build Status](https://drone.io/bitbucket.org/EternityIncurakai/eternityx/status.png)](https://drone.io/bitbucket.org/EternityIncurakai/eternityx/latest)

EternityX v2.1.0α3 "Viperid Alpha 3"
================================

EternityX now in it's fifteenth incarnation is the world's next generation open-source web framework.  Currently only for PHP, EterntyX makes it easy for web developers to develop enterprise web applications.  EternityX is written, and mantained by Eternity Incurakai Studios.  It was originally used as solely the underlying platform in which the studios run their site on.  

You may only use the framework under the terms of the GPL v3 license, details available in the LICENSE.md file found in this directory.  
  
## Installation
To install, simply extract all files to the desired directory, and then open the respective URL in your preferred web browser and follow the on-screen instructions to perform a successful installation.

## Upgrading
Upgrading has not yet been added programmatically.  To upgrade, simply delete all directories and files except for the application directory, and copy all new files besides the application directory to the root.  
 
## Release History
### Sunbeam  (v2.0.x)
* Sunbeam    (v2.0.0)  - 11/1/2013
* Sunbeam 1  (v2.0.1)  - 11/25/2013
* Sunbeam 2  (v2.0.2)  - 12/1/2013
* Sunbeam 3  (v2.0.3)  - 12/25/2013
* Sunbeam 4  (v2.0.4)  - 1/1/2014
* Sunbeam 5  (v2.0.5)  - 1/15/2014
* Sunbeam 6  (v2.0.6)  - 2/10/2014
* Sunbeam 7  (v2.0.7)  - Skipped
* Sunbeam 8  (v2.0.8)  - 2/17/2014
* Sunbeam 9  (v2.0.9)  - 2/23/2014
* Sunbeam 10 (v2.0.10) - 3/10/2014
* Sunbeam 11 (v2.0.11) - 3/17/2014
* Sunbeam 12 (v2.0.12) - 3/24/2014 