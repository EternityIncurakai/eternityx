v2.1.0α3 Viperid Alpha 3
==============
This update includes enhancements to the upgrader, introducing a number of new backend features.  

Core
======
- Added the send_stats function
- Added the getJSON function
- Added the Mailing Package


Upgrader
=====
- Mails webmaster when an upgrade is available and when EternityX has finished upgrading itself. "Mailing Package Integration"
- Refactored the Upgrader Class
- Restyled Progress Bar
- Restyled Readme section, added ability to collapse it
- Security enhancements
- Database restructuring
- Crontab restructuring
- Converted upgrader to object orientation