<?php
/**
 * Testing...
 *
 * Testing again.
 *
 * @author XenoK <xenok@eternityincurakai.com>
 * @copyright (C) Copyright 2014 Eternity Incurakai Studios, All Rights Reserved.
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @version v2.1.0
 * @package EternityX
 */
if(!is_file("application/apps/eternityincurakai/configs/application.ini"))
{
	if(strpos($_SERVER["REQUEST_URI"],"install")===false)
	{
		die("Please click <a href=\"install\">here</a> to install");
	}
	else
	{
		if(!@mkdir("application/apps")) die("Please chmod the application folder up.  I need to access it for installation.");
		if(!@mkdir("application/apps/eternityincurakai")) die("Please chmod the application/apps folder up.  I need it for installation.");
		if(!@mkdir("application/apps/eternityincurakai/configs")) die("Please chmod the application/apps/eternityincurakai/configs folder up.  I need to access it for installation");
		if(!@touch("application/apps/eternityincurakai/configs/application.ini")) die("Please chmod the application/apps/eternityincurakai/configs folder up.  I need it for installation.");
	}
}
if(!is_file("application/configs/application.ini"))
{
	if(strpos($_SERVER["REQUEST_URI"],"install")===false)
	{
		die("Please click <a href=\"install\">here</a> to install");
	}
	else
	{
		if(!@mkdir("application/configs")) die("Please chmod the application folder up.  I need to access it for installation");
		if(!@touch("application/configs/application.ini")) die("Please chmod the application/configs folder up.  I need it for installation.");
	}
}
if(file_get_contents("application/apps/eternityincurakai/configs/application.ini")=="")
{
	if(strpos($_SERVER["REQUEST_URI"],"install")===false)
	{
		die("Please click <a href=\"install\">here</a> to install");
	}
	else
	{
		if(!is_writable("application/apps/eternityincurakai/configs/application.ini"))
		{
			echo "Please chmod application/configs/application.ini so that apache can write to it.  Otherwise, you will have to write to the file manually.";
		}
		if(!is_writable("framework/data"))
		{
			die("Please chmod framework/data so that apache can write to it.");
		}
		if(!is_writable("framework/data/logs/errors.log"))
		{
			die("Please chmod framework/data/logs/errors.log so that apache can write to it.");
		}
	}
}
if(is_writable("application/apps/eternityincurakai/configs/application.ini")&&strpos($_SERVER["REQUEST_URI"],"install")===false)
{
	die("Error - application/configs/application.ini is writable.  Please chmod it down.");
}
$startTime = microtime(); // start time of script
if(!isset($_ENV['unittesting'])) $_ENV['unittesting'] = false;
if(!isset($_ENV['scriptstart'])) $_ENV['scriptstart'] = false;
ini_set('short_open_tag', 1);
ini_set('phar.readonly', 0);
date_default_timezone_set("UTC");
// setup constants
define('EX_ROOT', __DIR__);
define('ENV_ROOT', EX_ROOT.'/framework');
define('DOCS_ROOT', 'zip://'.ENV_ROOT.'/data/documentation.zip#');
define('CMZ_ROOT', EX_ROOT.'/customization');
define('THEMES_ROOT', CMZ_ROOT.'/themes');
define('PLUGINS_ROOT', CMZ_ROOT.'/plugins');

// setup bootstrapper
require ENV_ROOT.'/bootstrap.php';

$bs = new Bootstrapper();
$bs->setupMode();
$bs->mapFunctions(ENV_ROOT.'/functions.php');
$bs->mapDirs(['interfaces' => ENV_ROOT.'/models/helpers/interfaces', 'traits' => ENV_ROOT.'/models/helpers/traits', 'models' => ENV_ROOT.'/models', 'libraries' => ENV_ROOT.'/library' ]);
$bs->mapAliases([ '\EternityX\User' => '\EternityX\eUser' ]);
$bs->dispatch();
foreach(get_declared_classes() as $class) if(\EternityX\String::beginsWith($class, 'EternityX') && method_exists($class, 'init')) { $class::init(); }


// work within the EternityX namespace
use \EternityX;
use \EternityX\events;
use \EternityX\dispatcher;
use \EternityX\HTTP\Exception;
use \EternityX\Page;
use \EternityX\Theme;
use \EternityX\ThemeException;
use \EternityX\Plugin;
use \EternityX\Controller;
use \EternityX\Minify;
use \EternityX\User;

define('DATEFORMAT', 'F d, Y');
define('EX_OS', (strtolower(substr(PHP_OS, 0, 3)) == 'win') ? 'Windows' : PHP_OS);

try {

	$dispatcher = new dispatcher();
	$dispatcher->setConfigs(ENV_ROOT.'/configs/framework.ini');
    if($dispatcher->request->app == 'docs') {
        if($dispatcher->request->name == 'index') $dispatcher->request->name .= '.html';
        $dispatcher->setType();
        die(file_get_contents(DOCS_ROOT.$dispatcher->request->name));
    }

    if($_ENV['unittesting'] == false ) {
        $install = new \EternityX\Maintenance('installation', 'install');
        if(!$install->check()) $install->render();

        $upgd = new \EternityX\Maintenance('upgrading', 'upgrade');
        if($upgd->check()) $upgd->render();




    } else return;

    $dispatcher->setConfigs(APP_ROOT.'/configs/application.ini');
	$dispatcher->setupErrorHandler();
    $dispatcher->setupExceptionHandler();

	// SETUP PLUGINS
	try {
		Plugin::loadAll();
	} catch(EternityX\PluginException $e) {
		trigger_error($e->code.': '.$e->message.PHP_EOL, E_USER_WARNING);
	}

    // SETUP THEME
    events::callEvent('theme');
    try { $GLOBALS['theme'] = new Theme(defined("EX_THEME")?EX_THEME:"exmodern"); }
    catch (ThemeException $e) { trigger_error($e->code.': '.$e->message.PHP_EOL, E_USER_WARNING);}
    events::callEvent('end:theme');

	events::callEvent('dispatcherStartup');
	$dispatcher->deprecatedActions('/home4/xenok/import.php');
	$dispatcher->setDynamics(explode(",",defined("EX_DYNAMICS")?EX_DYNAMICS:""));
	$dispatcher->requirements();
	events::callEvent('dispatcherStartup-End');


	// Setup imports
	events::callEvent('importsSetup');
	$dispatcher->setImport('classes', APP_ROOT.'/models');
	$dispatcher->setImport('functions', APP_ROOT.'/functions');
	$dispatcher->setImport('traits', APP_ROOT.'/models/helpers/traits');
	$dispatcher->setImport('interfaces', APP_ROOT.'/models/helpers/interfaces');
	$dispatcher->setImport('startups', APP_ROOT.'/startup');
	events::callEvent('importsSetup-End');

        try {
            events::callEvent('databaseSetup');
            $dispatcher->setupDB(EX_DBHOST, EX_DBNAME, EX_DBUSER, EX_DBPASS);
            events::callEvent('databaseSetup-End');
        } catch(PDOException $e) {
            die('The database failed to be setup');
        }

	events::callEvent('sessionSetup');
	$dispatcher->setupSession();
	$dispatcher->setupUser();
	events::callEvent('sessionSetup-End');
    if($_ENV['scriptstart'] == true) return;
    $upgd = new \EternityX\Maintenance('upgrading', 'upgrade', true);
    if($upgd->check()) $upgd->render();

	events::callEvent('dispatchStart');
	$dispatcher->dispatch();
	events::callEvent('dispatchEnd');

} catch(\EternityX\HTTP\Exception $e) {
	ob_start(); ?>
	<div class="httperror">
		<h1><i class="fa fa-warning"></i> <?= $e->code.' '.$e->message; ?></h1>
		<p><?= $e->diagnosis; ?></p>
	</div>
	<?php $content = trim(ob_get_clean());
	$page = new Page($e->code.' '.$e->message);
	$page->setTheme(new Theme(defined("EX_THEME")?EX_THEME:"exmodern"));
	$page->setContent($content);
	$page->setMetadata(null);
	$page->setAssets(null);

	die($page);
}
