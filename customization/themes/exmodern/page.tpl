<!DOCTYPE html>

<!--[IF IE]><html class="ie" lang="en"><![endif]-->
<!--[IF !IE]><!--><html lang="en"><!--<![endif]-->

<head>
	<meta charset="UTF-8" />
        <{{tplex.include:{/application/includes/meta.php}/tplex}}>
	<{{tplex.sub:{meta}/tplex}}>
	<title><{{tplex.sub:{title}/tplex}}></title>
        <{{tplex.include:{/application/includes/assets.php}/tplex}}>
        <{{tplex.sub:{cdnjsdeps}/tplex}}>
        <script defer src="<{{tplex.echo:{EX_URL}/tplex}}>/min/js"></script>
        <script defer src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
	<{{tplex.sub:{assets}/tplex}}>
</head>
<body>
	<div id="outerwrap">
	<div id="innerwrap">

	<header role="banner">
		<div class="brand">
			<a href="<{{tplex.sub:{baseurl}/tplex}}>">
				<img src="<{{tplex.sub:{hlogo}/tplex}}>" alt="<{{tplex.sub:{hlogotxt}/tplex}}>" />
			</a>
		</div>
			
		<nav role="navigation">
			<div class="collapse">	
				<span class="fa fa-bars"></span>
			</div>
			<ul class="nav">
				<{{tplex.include:{/application/includes/header/navigation.php}/tplex}}>
			</ul>
		</nav>
		
		<!-- Notifications -->
		<div class="saved">Saved</div>
		<div class="error">Error: <span class="details"></span></div>
		
		<{{tplex.include:{/application/includes/header/announcement.php}/tplex}}>
	</header>	
	
	<div id="container" role="main">
		<{{tplex.sub:{content}/tplex}}>
	</div>
	
	<{{tplex.sub:{adminlinks}/tplex}}>
	
	<div class="clearfix" id="clearer"></div>
	
	<footer role="contentinfo">
		<div class="logo">
			<img src="<{{tplex.sub:{logo}/tplex}}>" alt="<{{tplex.sub:{logotxt}/tplex}}>" />
			<small class="copyright"><hr><{{tplex.sub:{copyright}/tplex}}><br><br>Powered by EternityX</small>
		</div>
		
		<{{tplex.include:{/application/includes/footer/navigation.php}/tplex}}>
	</footer>
	
	
	</div>
	</div>
</body>
</html>