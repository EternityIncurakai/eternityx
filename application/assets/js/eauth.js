/**
 * eAuth Client Script
 * by: XenoK Xihelien / Talen Fisher
 * (C) Copyright 2013 Eternity Incurakai Studios
 */

// setup vars
var eAuth = eAuth || {};
eAuth.login = eAuth.login || {};
eAuth.logout = eAuth.logout || {};
eAuth.signup = eAuth.signup || {};
eAuth.forgot = eAuth.forgot || {};

$(function () {
	
    eAuth = emodel({
        functions: {
            _init: function () {
            	
            	var $this = this;
            	this.ajaxURL = "<?=EX_URL;?>/ajax";
                if (localStorage.loggedIn == "true") {
                    $.post($this.ajaxURL, {
                        action: "eAuth.getUser",
                        ekey: localStorage.ekey
                    }, function (data, status) {
                        if (data["success"] === "true") {
                            sessionStorage.userID = data["response"]["id"];
                            sessionStorage.userName = data["response"]["username"];
                            sessionStorage.userPermissions = data["response"]["permissions"];
                            
                            $this.user = {};
                            $this.user.id = sessionStorage.userID;
                            $this.user.name = sessionStorage.userName;
                            $this.user.permissions = sessionStorage.userPermissions;
                            if($this.user.id == 'null') {
                            	delete localStorage.loggedIn;
                            	window.location.reload();
                            }
                        }
                    });
                }
                return true;
            }
        }
    });
    
   
    
    
    
    
    eAuth.signup = emodel({
        functions: {
        	
            _init: function () {
            	if(localStorage.loggedIn == "true") {
            		delete this;
            		return false;
            	}
                var $this = this;
                this.throttle = 15;
                $.getJSON("/apis/listUsers", {}, function (data, status) {
                    $this.userlist = data;
                });
                Recaptcha.create($("#recaptcha").data("publickey"), "recaptcha", {
                    theme: "clean",
                    callback: Recaptcha.focus_response_field
                });
                $("noscript").remove();
                $(".tip").click(function () {
                    $(this).parent().find("input").focus();
                    $(this).parent().find("select").click();
                });
                $("body").append('<div class="loader"><i class="icon-spinner spin"></i></div>');
                $(".loader").hide();
                if($('#signup_username').length > 1) {
                	$(".available").attr("style", "right: "+ ($(window).width() - ($("#signup_username").offset().left + $("#signup_username").outerWidth()))+"px");
                }
                return true;
            },
            fieldError: function (field) {
                field = this[field];
                field.attr("style", "box-shadow: 0 0 5px red");
                field.validated = "false";
                field.parent().parent().find("button").attr("disabled", "disabled");
                field.parent().parent().find("button").attr("aria-disabled", "true");
            },
            fieldSuccess: function (field) {
                var $this = this;
                var field = this[field];
                field.removeAttr("style");
                field.validated = "true";
                var valid = true;
                $($this[field.fieldset].elements).each(function (key, value) {
                    if ($this[value].validated == "false") {
                        valid = false;
                    }
                });
                if (valid) {
                    field.parent().parent().find("button").removeAttr("disabled");
                    field.parent().parent().find("button").removeAttr("aria-disabled");
                }
            }
        },
        
        elements: {
            username: {
                element: "#signup_username",
                validated: "false",
                fieldset: "fieldset1",
                events: {
                    keyup: function () {
                        var $this = $(this);
                        var $this2 = this;
                        var $value = $this.val();
                        $value = $value.toLowerCase();
                        if ($.inArray($value, eAuth.signup.userlist) > -1) {
                            $(".available").removeClass("active");
                            $(".available").html("unavailable");
                            this.model.fieldError("username");
                        } else {
                            $(".available").addClass("active");
                            $(".available").html("available");
                            if ($this.val().length < $this.data("minlength") || $this.val().length > $this.data("maxlength")) {
                                this.model.fieldError("username");
                            } else {
                                if ($this.val().match(/([^a-zA-Z\d])/)) {
                                    this.model.fieldError("username");
                                } else {
                                    this.model.fieldSuccess("username");
                                }
                            }
                        }
                    }
                }
            },
            password: {
                element: "#signup_password",
                validated: "false",
                fieldset: "fieldset1",
                events: {
                    keyup: function () {
                        var $this = $(this);
                        if ($this.val().length < $this.data("minlength") || $this.val().length > $this.data("maxlength")) {
                            this.model.fieldError("password");
                        } else {
                            if (!($this.val().match(/([a-z])/g) && $this.val().match(/([^a-zA-Z\d])/g) && $this.val().match(/([A-Z])/g) && $this.val().match(/([0-9]+)/g))) {
                                this.model.fieldError("password");
                            } else {
                                this.model.fieldSuccess("password");
                            }
                        } if ($this.val() !== this.model.passwordRetype.val()) {
                            this.model.fieldError("passwordRetype");
                        } else {
                            this.model.fieldSuccess("passwordRetype");
                        }
                    }
                }
            },
            passwordRetype: {
                element: "#signup_passwordretype",
                validated: "false",
                fieldset: "fieldset1",
                events: {
                    keyup: function () {
                        var $this = $(this);
                        if ($this.val() !== this.model.password.val()) {
                            this.model.fieldError("passwordRetype");
                        } else {
                            this.model.fieldSuccess("passwordRetype");
                        }
                    }
                }
            },
            age: {
                element: "#signup_birth",
                validated: "false",
                fieldset: "IDfieldset",
                events: {
                    change: function () {
                        var $this = $(this);
                        var $value = $(this).val();
                        var $date = new Date;
                        $date.setFullYear($date.getFullYear() - $this.data("minage"));
                        if (new Date($value) > $date) {
                            this.model.fieldError("age");
                        } else {
                            this.model.fieldSuccess("age");
                        }
                    }
                }
            },
            location: {
                element: "#signup_location",
                validated: "false",
                fieldset: "IDfieldset",
                events: {
                    change: function () {
                        var $value = $(this).val();
                        if ($value == "Select One:") {
                            this.model.fieldError("location");
                        } else {
                            this.model.fieldSuccess("location");
                        }
                    }
                }
            },
            email: {
                element: "#signup_email",
                validated: "false",
                fieldset: "IDfieldset",
                events: {
                    keyup: function () {
                        var $value = $(this).val();
                        if ($value.length < $(this).data("minlength") || !$value.match(/^(([^<>()[\]\\.,;:\s@\\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
                            this.model.fieldError("email");
                        } else {
                            this.model.fieldSuccess("email");
                        }
                    },
                    blur: function () {
                        var $this = $(this);
                        var $this2 = this;
                        var $value = $(this).val();
                        $.post(eAuth.ajaxURL, {
                        	action : 'eAuth.checkEmail',
                            email: encodeURIComponent($value)
                        }, function (data, status) {
                            if (data == "true") {
                                $this.parent().find(".fielderror").html("This email is already taken.");
                                $this2.model.fieldError("email");
                            } else {
                                $this.parent().find(".fielderror").html("");
                                $this2.model.fieldSuccess("email");
                            }
                        });
                    }
                }
            },
            IDfieldset: {
                element: "#idcontinue",
                validated: "true",
                elements: ["age", "location", "email"],
                events: {
                    click: function () {
                        var $this = this;
                        var valid = true;
                        var element = new Array;
                        var r = null;
                        $($this.elements).each(function (key, value) {
                            $this.model[value].keyup();
                           	$this.model[value].blur();
                            if ($this.model[value].validated === "false") {
                                element.push(value);
                                valid = false;
                            }
                        });
                        $($this.model[element[0]]).focus();
                        if (valid) {
                            $(this).parent().toggleClass("active");
                            $(this).parent().next().toggleClass("active");
                        }
                    }
                }
            },
            fieldset1: {
                element: "#login_cred button",
                validated: "true",
                elements: ["username", "password", "passwordRetype"],
                events: {
                    click: function () {
                        var $this = this;
                        var valid = true;
                        var errors = new Array;
                        $($this.elements).each(function (key, value) {
                            if ($this.model[value].validated === "false") {
                                $this.model.fieldError(value);
                                errors.push(value);
                                valid = false;
                            }
                        });
                        $($this.model[errors[0]]).focus();
                        if (valid) {
                            $(this).parent().toggleClass("active");
                            $(this).parent().next().toggleClass("active");
                        }
                    }
                }
            },
            response: {
                element: "#recaptcha_response_field",
                value: function () {
                    return Recaptcha.get_response();
                },
                events: {}
            },
            challenge: {
                element: "#recaptcha_challenge_field",
                value: function () {
                    return Recaptcha.get_challenge();
                },
                events: {}
            },
            
            
            
            resend : {
            	element : "button#resend",
            	events : {
            		"click" : function() {
            			$.post(eAuth.ajaxURL, {action:'eAuth.signup.resend'}, function(data, status) {
            				if($(".success").length !== 1) {
            					$("body").append('<div class="success">Successfully resent the confirmation email.</div>');
            					$(".success").fadeIn();
            					setTimeout(function() {
            						$(".success").fadeOut();
            						setTimeout(function() {
            							$(".success").remove();	
            						}, 1000);            						
            					}, 3000);
            				}
            				
            			});
            		}
            	}
            }
        },
        form: {
            element: "#signup",
            action: "eAuth.signup",
            elements: {},
            values: { signup : 'EAUTHSIGNUP' },
            success: function () {
               var $this = this;
                localStorage.loggedIn = true;
                $("body").css({cursor:'progress'});
                $("form").disable();
                $.get("https://eternityincurakai.com/signup/confirm?_body", function(data, status) {
                	$("body").html(data);
                	history.replaceState(null, 'Confirm Signup - Eternity Incurakai', "/signup/confirm");
                	document.title = 'Confirm Signup - Eternity Incurakai';
                	$("body").css({cursor:'default'});
                });
                
            },
            error: function () {
            	
                var $this = this;
                console.error("Login Error: " + this.code + "\n" + this.string);
                if (this.code == "EX0014") {
                    console.log('test');
                } else {
                	// other errors
                    switch (this.code) {
                    	case "EX0001":
                    	case "EX0002":
                    	case "EX0003":
                    		$("fieldset").removeClass("active");
                    		$("#login_cred").addClass("active");
                    		this.model.username.focus();
                    		this.model.username.keyup();
                    		break;
                    	case "EX0004":
                    	case "EX0005":
                    	case "EX0006":
                    	case "EX0007":
                    	case "EX0008":
                    		$("fieldset").removeClass("active");
                    		$("#login_cred").addClass("active");
                    		this.model.password.focus();
                    		this.model.password.keyup();
                    		break;
                    	case "EX0009":
	                    	$("fieldset").removeClass("active");
                    		$("#login_cred").addClass("active");
                    		this.model.passwordRetype.focus();
                    		this.model.passwordRetype.keyup();
                    		break;
                    	case "EX0010":
                    	        $("fieldset").removeClass("active");
                        	$("#identity_info").addClass("active");
                	      	this.model.age.blur();
                        	//this.model.age.focus();
                        	break;

                    	case "EX0011":
                    	case "EX0012":
							$("fieldset").removeClass("active");
                        	$("#identity_info").addClass("active");
                	      	this.model.location.blur();
                        	this.model.location.focus();
                        	break;

                    	case "EX0013":
                        	$("fieldset").removeClass("active");
                        	$("#identity_info").addClass("active");
                	      	this.model.email.blur();
                        	this.model.email.focus();
                        	break;
                        	
                        case "EX0014":
                        	Recaptcha.reload();
                        	Recaptcha.focus();
                    }
                }
            }
        }
    });
    
    
	eAuth.forgot = emodel({
		functions : {
			_init:function() {return true;}
		},
		
		elements : {
			email : {
				element : "#email"
			} 
		},
		
		form : {
			element : "form#forgot",
			action: "eAuth.login.forgot",
			values : {},
			elements : {},
			success : function() {
				$("form#forgot").html('success');
			}
			
		} 
	});
   
   
    
});