﻿
$(function() {
	sessionStorage.title = document.title;
	if($(location).attr("hash").match("/")) {
		history.pushState(null, null, $(location).attr("href").replace($(location).attr("hash"), "")+"/"+$(location).attr("hash"));
	}
	
	$(document).on('click', '.exw.exw-btn', function() {
		var target = $('#'+$(this).data('target'));
		
		// inlays
		if(target.hasClass('exw-inlay')) {
			target.slideToggle();
		}
		
		// overlays
		if(target.hasClass('exw-overlay')) {
			target.toggleClass('active');
		}
		
		
		target.find('i').click(function() {
			target.toggleClass('active');
			target.find('i').unbind();
		});
		
		target.click(function() {
			target.toggleClass('active');
			target.find('i').unbind();
			target.unbind();
		});
		
		target.find('.exw-content').click(function(e) {
			e.stopPropagation();
			return false;
		});
	});
	
	/** Buttons **/
	$(document).on('click', '.btn', function() {
		//window.onhashchange = function(e) { e.preventDefault(); }
		
			if($(this).data("href") !== undefined) {
				if($(this).data("type") != "link") {
					var str = $(location).attr("href").replace($(location).attr("hash"), "");
					if(str.substring(str.length - 1, str.length) != "/") { // if there is already a hash in place
						var hash = $(location).attr("hash").replace("#!/", "");
						history.pushState(null, null, $(location).attr("href")+"/"+"#!"+$(this).data("href"));
						document.title = $(location).attr("hash").replace("#!/", "") + ' / ' + sessionStorage.title;
					} else { // there isnt a hash in place already
						history.pushState(null, null, $(location).attr("href").replace("/"+$(location).attr("hash"), "")+"/"+"#!"+$(this).data("href"));
						document.title = $(location).attr("hash").replace("#!/", "") + ' / ' + sessionStorage.title;
					}
				}
				// overlays
				if($(this).data("type") == "overlay") {
					var href = $(this).data("href").replace("/", "");
					href = href.replace("/", "-");
					if(href.match(/\-/g)) {
						var substring = href.substring(0, href.indexOf("-"));
						var returner = true;
						$("[data-href='/"+substring+"'][data-type='tab']").click();
						if(str.substring(str.length - 1, str.length) != "/") {
							history.pushState(null, null, $(location).attr("href")+"/");
						}
						history.pushState(null, null, $(location).attr("href").replace("/"+$(location).attr("hash"), "")+"#!/"+$(this).data("href"));
					}
					var selector = $(".overlay#"+href);
					selector.fadeIn();
					var lightbox = selector.find('.lightbox');
					if(lightbox.length == 0) { $(selector).append('<div class="lightbox"></div>'); }
					lightbox.show();
					lightbox.fadeIn();
					lightbox.click(function() {
						if($(this).data("active") != "true") {
							selector.fadeOut();	
							history.go(-1);
						}
					});
				// tabs
				} else if($(this).data("type") == "tab") {
					var tab = $("#"+$(this).data("href").replace("/", ""));
					$("#"+$(this).data("tab")).html(tab);
					
					$(".btn[data-type='tab']").click(function() {
						$("body").append(tab);
					});
				} else if($(this).data('type') == "extend") {
					var title = document.title;
					history.pushState(null, null, $(location).attr("href")+"/"+$(this).data("extend"));
					extend = $(this).data('extend');
					var extender = $("#extend"+extend);
					var extender2 = extender.clone();
					var replacer = $("#"+$(this).data("href").replace('/', ''));
					var btntag = $(extender).find('.btn');
					var btntext = $(extender).find('.btn').text();
					document.title = btntext; 
					if($(this).data("href") == "/stream") {
						postcomments(gethashID());
					}
					$("#"+$(this).data("href").replace("/","")+"extendtemplate .content").html($(extender2));
					$("#"+$(this).data("href").replace("/","")+"extendtemplate .content").find('.btn').replaceWith($("#"+$(this).data("href").replace("/","")+"extendtemplate").find('.btn').text());
					$("#"+$(this).data("href").replace("/","")+"extendtemplate").fadeIn();
					
					$("#"+$(this).data('href').replace('/','')+"extendtemplate .lightbox").click(function() {
						if($(this).data("active") != "true") {
							$(this).parent().fadeOut();
							
							base = $(location).attr("hash").replace("#!", "");
							base = base.replace('/'+gethashID(), '');
							console.log(base.replace('/', ''));
							history.go(-1);
						}
					});
				} else if($(this).data("type") == "link") {
					$(location).attr("href", $(this).data("href"));
				}
			}
		
	});	
	
	
	
	/** comment heads (expanding as necessary) **/
	$(".info").each(function() {
		$(this).attr("style", "height: "+($(this).parent().height() - 10) +"px !important;");
	});
	
	// ajax links 
	$(document).on('click', '.ajaxlink', function(e) {
		e.preventDefault();
		// variables
		var href = $(this).data("href");
		var value = $(this).data("value");
		var value2 = $(this).data("value2");
		var $this = $(this);
		var $replace = $(this).data("replace");
		if($(location).attr("hash") == "") {
			replace = "";
		} else {
			replace = '/'+$(location).attr("hash");
		}
		// ajax
		$.post($(location).attr('href').replace(replace, ""), { ajax:href, value:value, value2:value2 }, function(data, status) {
			if($replace !== undefined) {
				$($replace).replaceWith(data);
			} else {
				$this.replaceWith(data); // replace
			}
		});
	});
	

	if($(location).attr("hash") != "") {
		if(gethashID() == null) {
			$('*[data-href="'+$(location).attr("hash").replace("#!", "")+'"]:not([data-type="extend"])').click();
		} else {
			base = $(location).attr("hash").replace("#!", "");
			base = base.replace('/'+gethashID(), '');
			id = gethashID();
			$('[data-href="'+base+'"][data-type="tab"]').click();
			$(document).find('[data-extend="'+id+'"]:first').click();
		}
	}
	
});

window.onhashchange = function(e) {
		
		if($(location).attr("hash") != "") {
			$('.overlay').each(function() {
				if($(this).attr("style") == "display: block;") {
					$(this).find('.lightbox').click();
				}		
			});
			
			if(gethashID() == null) {
				$('*[data-href="'+$(location).attr("hash").replace("#!", "")+'"]:not([data-type="extend"])').click();
			} else {
				base = $(location).attr("hash").replace("#!", "");
				base = base.replace('/'+gethashID(), '');
				id = gethashID();
				$('[data-href="'+base+'"][data-type="tab"]').click();
				$(document).find('[data-extend="'+id+'"]:first').click();
			}
			sessionStorage.lastItem = $(location).attr("hash").replace("#!/", "");
			sessionStorage.lastType = $('*[data-href="'+$(location).attr("hash").replace("#!", "")+'"]').data("type");
		}
	};

