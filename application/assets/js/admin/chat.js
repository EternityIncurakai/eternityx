/**
* Admin Chatting
* This controls the chatting system used by admins.
*/
 


// controller functions
$(function() {
	
	// variables
	var source = new EventSource('https://eternityincurakai.com/apis/sse/chat'); 
	 

	// on message
	source.onmessage = function(e) {
		if(e.lastEventId !== sessionStorage.lastID) { 
			$(".bottom").append(e.data);
			$(".bottom").scrollTop($(".bottom:first").scrollHeight);
		}
		
		sessionStorage.lastID = e.lastEventId;
		
	};
	// clearing the chat
	$("#clear").click(function(e) {
		e.preventDefault();
		$.post("https://eternityincurakai.com/apis/sse/chat", { type:'clear' }, function(data, status) {
			$(".bottom").html("");
		});
	});
	
	
	// sending a message
	$("#chat").submit(function(ef) {
		ef.preventDefault();
		$.post("https://eternityincurakai.com/apis/sse/chat", {type:'write',message:$("#chatter").val()});
		$("#chatter").val('');
	});
});
