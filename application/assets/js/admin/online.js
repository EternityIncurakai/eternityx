$(function() {
	$(".user").each(function() {
		var username = $(this).data("user");
		var source = "/apis/sse/online/"+username;
		var online = new EventSource(source);
		var $this = $(this);
		online.onmessage = function(e) {
			if(e.data == 'Online') {
				data = '<i class="icon-circle" style="color: green"></i>';
			} else {
				data = '<i class="icon-circle" style="color: gray"></i>';
			}
			$this.find('.icon').html(data);
		};
	});
});