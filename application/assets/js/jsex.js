/**
 * 
 */
jQuery.fn.extend({
	
	disable : function() {
		return this.each(function() {
			$(this).attr("disabled", "disabled");
			$(this).attr("aria-disabled", "true");
		});
	},
	
	enable : function() {
		return this.each(function() {
			$(this).removeAttr("disabled");
			$(this).removeAttr("aria-disabled");
		});
	},	
	
	saved : function() {
		return this.each(function() {
			var $this = $(this);
			$(this).attr('style', 'transition: background 1s;');
			setTimeout(function() { $this.css({background:'yellow'}); setTimeout(function() { $this.css({background:'none'}); }, 2000); }, 100);
			
		});	
	},
	
	success : function(message) {
		return this.each(function() {
			$(this).html(message);
			$(this).addClass('active');
			$(this).fadeIn(400, function() {
				var $this = $(this);
				setTimeout(function() {
					$this.fadeOut();
				},5000);
			});
		});
	},
	
	error : function(message) {
		return this.each(function() {
			$(this).html(message);
			$(this).removeClass('active');
			$(this).fadeIn(400, function() {
				var $this = $(this);
				setTimeout(function() {
					$this.fadeOut();
				},5000);
			});
		});
	}
});


jQuery.fn.selectText = function(){
   var doc = document;
   var element = this[0];
   console.log(this, element);
   if (doc.body.createTextRange) {
       var range = document.body.createTextRange();
       range.moveToElementText(element);
       range.select();
   } else if (window.getSelection) {
       var selection = window.getSelection();        
       var range = document.createRange();
       range.selectNodeContents(element);
       selection.removeAllRanges();
       selection.addRange(range);
   }
};