/**
* Avatar Upload Script
* this script uploads and handles avatar files from the
* dropbox and uploads them to the server, then 
*/


$(function() {
	$(function() { $("#avatar-edit").attr("style", "margin-left: 0;"); });
	// set vars
	var UTPadding = (($('.user-top').innerWidth() - $('.user-top').width()) / 2) - 5; // user top padding 
	
	/**==================
	 *  SET WIDTHS 
	 * set the widths of different elements to 
	 * coincide directly with other preset elements 
	 * using percentages.
	 */
	
	// button
	
	// online
	$(".online").width($(".user-top img").width() - 10);	
	$(".online").height(UTPadding);
	
	// dropbox
	$("#dropbox").height($("#avatar form").height() / 25);
	
	
	/** FIXES FOR RESIZE **/
	$(window).resize(function() {
		// reset padding var
		UTPadding = (($('.user-top').innerWidth() - $('.user-top').width()) / 2) - 5; // user top padding
		
		// button
				
		// online
		$(".online").width($(".user-top img").width() - 10);
		$(".online").height(UTPadding);
		
		// dropbox
		$("#dropbox").height($("#avatar form").height() / 25);
	});
	$(".lightbox").click(function() { $(this).parent().find('form').resetForm(); });
	document.title.onchange = function(e) {
		e.preventDefault();
		return;
	};
});



/**
 * FILE HANDLER
 */
function xhr2Upload(file) {
	
	// make sure the file isnt undefined 
	if(typeof(file) === undefined) {
		$("#avatar .lightbox").click();
		return;
	}
	
	
	// set vars
    xhr = new XMLHttpRequest();
    var aUpload = getTemplate("users/avatarUpload");	
    var url = $(location).attr("href").replace("/"+$(location).attr("hash"), "");
    var fd = new FormData();
    var errors = new Array();
    
    // setup errors
    errors[0] = 'Cancelled';
    errors[1] = 'File is not an image';
    errors[2] = 'Invalid File Type';
    errors[3] = 'Unknown error';
	localStorage.errors = JSON.stringify(errors);  
    
    // setup elems
	$("#avatar .lightbox").data("active", "true");
    $("#avatar form").append(aUpload);
    $("#avatar form #dropbox").hide();
    $("body").css({cursor:"progress"});
    
    xhr.open("POST", url, true);
    // setup xhr functions
    xhr.upload.onprogress = function(e) { uploadProgress(e); };
    xhr.onload = function(e) { uploadLoad(e, xhr, file); };
    xhr.onerror = function(e) { uploadError(e, xhr, null); };
    xhr.onabort = function(e) { uploadAbort(e); };
    $("#cancel").click(function() { xhr.abort(); });    
	
    
	fd.append('file', file);
	fd.append('ajax', 'users/avatar/edit');
    // Initiate a multipart/form-data upload
    xhr.send(fd);
}

function uploadError(e, xhr, error) {
	
	// fetch error
	
}

function uploadAbort(e) {
	}


function uploadLoad(e, xhr, file) {
	
}

 function uploadProgress(e) {
   
}
    


$(function() {
	if(sessionStorage.userName === $(".user-top h1").html()) {
	    
	    
		    
	    	}
});