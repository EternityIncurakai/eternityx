$(function() {
	
	$(".tip").click(function() { $("textarea.description").focus();});
	$(document).on('click', ".user-top img", function() { $(document).find(".user-top .btn").click(); });
	
	/**---------------
	User description
	*/
	$('textarea.description').change(function() {
		type = $(this).data("type");
		$.post($(location).attr('href').replace("/"+$(location).attr("hash"), ""), {
			ajax:"users/description",
			description:$('textarea.description').val(),
			type:type,
			user:$(".user-top h1").html()
		}, function(data, status) {
			saved();
			$('textarea.description').css({
				background:"yellow"
			});
			setTimeout(function() {
			$('textarea.description').css({
				background:"none"
			});
			}, 1000);
		});
	});
	
	$('textarea.description').keydown(function(e) {
	   if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey))
	    {
	        e.preventDefault();
	       	$(this).change();
	    }
	    return true;
	});
		
	$('#new').click(function() {
		$("#postnew").click();
	});
	
	$(document).on('click', "#reporticon", function() {
		$("#reportbutton").click();
	});
	if($(location).attr("hash") == "") {
		$("[data-href='/stream']:not([data-type='extend'])").click();
	}
	var content;
	$("#website").focus(function() {
		content = $(this).html();
		if($(content).text() == "No Website Yet") {
			$(this).html(' ');
			$(this).attr("style", "display: inline-block; min-width: 50px; border: 1px solid #000;");
			$(this).click();
		}
	});
	$("#website").blur(function() {
		if($("#userdata").data('username') === $("#username").html() && content != $(this).html()) {
			$(this).attr("href", $(this).html());
			$this = $(this);
			$.post($(location).attr("href").replace('/'+$(location).attr("hash"), ""), {ajax:"users/website", website:$this.html()}, function(data, status) {
				saved();
				$this.attr("style", "background: yellow");
				setTimeout(function() { $this.removeAttr("style"); }, 1000);
			});
		}
		
	});
	
	$(document).on('click', '.delete', function(e) {
		e.preventDefault();
		var $id = $(this).attr("data-target");
		$.post($(location).attr("href").replace("/"+$(location).attr("hash"), ""), {
			ajax:"users/posts/delete",
			id:$id,
			user:$('.user-top h1').html()
		}, function(data, status) {
			$(".posts").html(data);
			$(".lightbox").click();
			$("[data-href='/stream'][data-type='tab']").click();
		});
	});
});