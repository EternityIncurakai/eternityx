$(function() {
	var textarea = $("#stream-post:first form textarea");
	$(document).on('click', "#bold", function() { $(textarea).val($(textarea).val()+"$(bold)(/bold)"); $(textarea).selectRange(length(7), length(7)); });
	$(document).on('click', "#italic", function() { $(textarea).val($(textarea).val()+"$(italic)(/italic)"); $(textarea).selectRange(length(9), length(9)); });
	$(document).on('click', "#strike", function() { $(textarea).val($(textarea).val()+"$(strike)(/strike)"); $(textarea).selectRange(length(9), length(9)); });
	$(document).on('click', "#paragraph", function() { $(textarea).val($(textarea).val()+"$(p)(/p)"); $(textarea).selectRange(length(4), length(4));});
	$(document).on('click', "#heading", function() { $(textarea).val($(textarea).val()+"$(heading=1)(/heading)"); $(textarea).selectRange(length(10), length(10));});
	$(document).on('click', "#link", function() { $(textarea).val($(textarea).val()+"$(url)(/url)"); $(textarea).selectRange(length(6), length(6)); });
	$(document).on('click', "#image", function() { $(textarea).val($(textarea).val()+"$(img)(/img)"); $(textarea).selectRange(length(6), length(6)); });
	$(document).on('click', "#break", function() { $(textarea).val($(textarea).val()+"$(br /)"); $(textarea).selectRange(length(0), length(0)); });
	
	$(document).on('click', ".close", function(e) { e.preventDefault(); $(this).parent().parent().find('.lightbox').click(); $(textarea).val(''); });
	
	$(document).on('keydown', ".post-comment", function(e) {
		if(e.which == 13) {
			e.preventDefault();
			postID = $(this).data("target");
			comment = $(this).val();
			$.post("", { ajax:"users/posts/comment", id:postID, comment:comment});
			$(this).val('');
		}
	});
});

function length(num) {
	var textarea = $(" #stream-post form textarea");
	return $(textarea).val().length - num;
}


function change(pos)
{
	var ctrl = $(" #stream-post form textarea");	
    if(ctrl.setSelectionRange)
    {
        ctrl.focus();
        ctrl.setSelectionRange(pos,pos);
    }
    else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}

function postcomments(id) {
	var source = "/apis/sse/stream-comments/"+id;
	var events = new EventSource(source);
	if($("#extend"+id+" .comments .commentarea").html().trim() !== "<i>No Comments.  Be the first!</i>") {
		console.log($("#extend"+id+" .comments .commentarea").html().trim());
		var oldata = $("#extend"+id).find('.comment').last().get(0).outerHTML;
		oldata = oldata.replace(/[\n\r\t]/g, "");
	} else {
		var oldata = '<div class="comment" id=""><div class="comment-top"><img src="http://www.petermanseye.com/images/default_avatar.jpg?1360733630" role="img" alt="default" /><a href="/users/"></a></div><p></p></div>';
	}
	$("#extend"+id+" .comments .commentarea").scrollTop($("#extend"+id+" .comments .commentarea"));
	events.onmessage = function(e) {
		
		if(oldata != e.data) {
			if($("#extend"+id+" .comments .commentarea").html().trim() === "<i>No Comments.  Be the first!</i>") {
				$("#extend"+id+" .comments .commentarea").html(e.data);
				$("#extend"+id+" .comments .commentarea").scrollTop($("#extend"+id+" .comments .commentarea")[0]);
			} else {
				$("#extend"+id+" .comments .commentarea").append(e.data);
				$("#extend"+id+" .comments .commentarea").scrollTop($("#extend"+id+" .comments .commentarea")[0]);
			}
		}
		oldata = e.data;
	};
};
(function($) {
	$.fn.selectRange = function(start, end) {
	    if(!end) end = start; 
	    return this.each(function() {
	        if (this.setSelectionRange) {
	            this.focus();
	            this.setSelectionRange(start, end);
	        } else if (this.createTextRange) {
	            var range = this.createTextRange();
	            range.collapse(true);
	            range.moveEnd('character', end);
	            range.moveStart('character', start);
	            range.select();
	        }
	    });
	};
})(jQuery);
