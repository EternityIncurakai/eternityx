$(document).ready(function () {
    var lastmsg = "";
    var t = new EventSource("https://eternityincurakai.com/apis/sse/stream-posts/" + $("#report form input:first").val());
    t.onmessage = function (e) {
        if (lastmsg !== e.data) {
            $("#stream").html(e.data);
            lastmsg = e.data
        }
    };
    var auth = "";
    $(".icon-edit").addClass("fa fa-plus");
    $("#new").click(function () {
        var e = 0;
        $("#overlay").css("display", "inline");
        cssInterval = setInterval(function () {
            $("#overlay").css("backgroundColor", "rgba(" + ["0", "0", "0", (e / 500).toString()].join(",") + ")");
            e++;
            if (e > 250) {
                clearInterval(cssInterval);
            };
        }, 1);
        $("#underlay").css("marginTop", $("#underlay").height() / -2);
        $("#underlay").css("marginLeft", $("#underlay").width() / -2);
    });
    $("body").prepend('<div id="overlay" style="display:none;position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: rgba(0,0,0,0);z-index: 10000;"><div id="underlay" style="position: fixed;top: 50%;left: 50%;text-align:center;background: rgba(255,255,255,1);"><label for="title">Title: </label><input type="text" id="title" /><textarea id="content" cols="50" rows="20"></textarea><button id="streamSubmit">Submit</button></div></div>');
    $("#overlay").click(function () {
        $("#overlay").css("display", "none");
    });
    $("#underlay").click(function (e) {
        e.stopPropagation();
    });
    $.post("https://eternityincurakai.com/apis/generate_auth_token", function (e) {
        auth = e;
    });
    $("#streamSubmit").click(function () {
        $.post("https://eternityincurakai.com/apis/sse/stream-post/" + $("#title").val() + ":" + $("#content").val() + ":" + auth, function () {
            $("#title").add("#content").val("");
        });
        $("#overlay").css("display", "none");
    });
});