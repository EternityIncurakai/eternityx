$(function() {
	$("#avatar-edit").attr("style", "margin-left: 0;");
});
var eUser = eUser || {};
eUser.admin = eUser.admin || {};
eUser.meta = eUser.meta || {};

$(function() {
	
	eUser = emodel({
		functions : {
			_init : function() {
				this.ajaxURL = "https://eternityincurakai.com/ajax";
				return true;
			}
		}
	});
	
	
	eUser.admin = emodel({
		functions : {
			_init : function() {
				
			}
		},
		
		elements : {
			news : {
				element : "#news",
				events : {
					"change" : function() {
						var $value = $(this).val();
						$.post("/ajax", {action:"eUser.admin.news", news:$value}, function(data, status) {
							console.log(data);
							$("#news").attr("style", "background: yellow");
								setTimeout(function() {
									$("#news").removeAttr("style");
								}, 1000);
						});
					}
				}
			}
		}
	});
	
	
	
	eUser.meta = emodel({
		functions : {
			_init : function() {
				
			}
		},
		elements : {
			
			description : {
				element : 'textarea.description',
				events : {
					'change' : function() {
						var $value = $(this).val();
						$.post(eUser.ajaxURL, { action:'eUser.update.description', description:$value}, function(data, status) { console.log(data); });
					}
				}
			}

		},		
	});
	
	eUser.avatar = emodel({
		functions : {
			_init: function() {
				var dropzone = document.getElementById("dropbox");
				var $this = this;
				
				if(!$(location).attr("href").match(/https\:\/\/eternityincurakai\.com\/users\/(.*)/g)) {
					delete this;
				} 
				
				this.errors = new Array();
			    
			    // setup errors
			    this.errors['EX7001'] = 'Cancelled';
			    this.errors['EX7002'] = 'File is not an image';
			    this.errors['EX7003'] = 'Invalid File Type';
			    this.errors['EX7004'] = 'Unknown error';
				
				this.screens = {};
				this.screens.upload = getTemplate('users/avatarUpload');
				
				this.ajax = "https://eternityincurakai.com/ajax";
				this.action = "eUser.upload";
				return true;
			},
						
			upload: function(file) {
				// make sure the file isnt undefined 
				if(typeof(file) === undefined) {
					this.lightbox.click();
					return;
				}

				// set vars
			    xhr = new XMLHttpRequest();
			    var fd = new FormData();
			   	var $this = this;			
			    
			    // setup elems
				$(this.lightbox).data("active", "true");
			    $(this.form).append(eUser.avatar.screens.upload);
			    this.dropbox.hide();
			    $("body").css({cursor:"progress"});
			    
			    xhr.open("POST", eUser.avatar.ajax, true);
			    // setup xhr functions
			    
			    xhr.upload.onprogress = function(e) { $this.progress(e); };
			    xhr.onload = function(e) { $this.load(e, xhr, file); };
			    xhr.onerror = function(e) { $this.error(e, xhr, null); };
			    xhr.onabort = function(e) { $this.abort(e); };
			    $("#cancel").click(function() { xhr.abort(); });    
			
			    
				fd.append('file', file);
				fd.append('action', $this.action);
				
			    xhr.send(fd);
			},
			
			progress : function(e) {
				var percentComplete = e.loaded / e.total;
			    percent = percentComplete * 100;
			    $("#progress").attr("value", percent);
			    $("#progress").html(percent);
			    percent = Math.round(percent) + "%";
			    if($(".progressind").html() == '<i class="icon-spinner icon-spin"></i>') {
			    	$(".progressind").append(percent);
			    } else {
			    	$('.progressind').contents().filter(function() { return this.nodeType === 3; }).remove();
			    	$(".progressind").append(percent);	
			    }	
			},
			
			load : function(e, xhr, file) {
				console.log(xhr);
				var response = JSON.parse(xhr.response);
				
				$("body").css({cursor:"auto"});
				$("#progress").css({background:"green"});
				$("#progress").html(100);
				$("#progress").attr("value", 100);
				$("#cancel").remove();
			    $(".progressind").html(response.response);
				if(response.success == 'true') {
					$(".progressind").prepend('<i class="icon-check"></i> ');
				} else {
					this.error(e, xhr, response);
					return;
				}
				$("#avatar_overlay .lightbox").data("active", "");
			    setTimeout(function() {
					
					$("#avatar_overlay .lightbox").click();
					var img = document.createElement('img');
					img.file = file;
					img.id = "avatar";
					$(".user-top img").replaceWith(img);
					var reader = new FileReader();
					reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
					reader.readAsDataURL(file);
					$("#file").val('');
					$("#avatar_overlay form #dropbox").show();
					$('#uploading').remove();
				}, 5000);
			},
			
			abort : function(e) {
				this.error(e, null, 0);
			},
			
			error : function(e, xhr, error) {
				error = parseInt(error);
				// show error
				if(error === null) {
					errorx = '<i class="icon-remove"></i> ' + xhr.status + ": " + xhr.statusText;
				} else {
					errorx = '<i class="icon-remove"></i> ' + eUser.avatar.errors[error];	
				}
				$(".progressind").html(errorx);
				
				// close everything
				$("#close").remove();
				setTimeout(function() {
					$("#avatar .lightbox").data("active", "");
					$("#avatar .lightbox").click();
					setTimeout(function() {
						$("#avatar form #dropbox").show();
						$('#uploading').remove();
					}, 1000);
				}, 5000);
			}	
		},
		
		elements : {
		
			
			image : {
				element: "#avatar",
				events : {
					click : function() {
						this.model.edit.click();
					}
				}
			},
			
			edit : {
				element : "#avatar_edit",
				events : {
					click : function() {
						this.model.overlay.fadeIn();
					}
				}
			},
			
			overlay : {
				element : "#avatar_overlay",
				events : {
					
				}
			},
			
			form: {
				element : "#avatar_overlay form",
				
			},
			
			lighbox : {
				element : "#avatar_overlay .lightbox",
				events : {
					click: function() {
						this.model.overlay.fadeOut();
					}
				}
			}, 
			dropbox : {
				element : "#dropbox",
				events : {
					click : function() {
						this.model.file.click();
					},
					
					dragover : function() {
						$(this).addClass('dragover');
					},
					
					dragenter : function() {
						$(this).addClass('dragover');
					},
					
					dragleave : function() {
						$(this).removeClass('dragover');
					},					
					
					drop : function(event) {
						var imageType = /image.*/;
				        var file = event.originalEvent.dataTransfer.files[0];
			    		$(this).removeClass('dragover');
						this.model.upload(file);
					}
				}
			},
			
			file : {
				element : "#file",
				events : {
					change : function() {
						var files = document.getElementById("file").files[0]; // fix
						
						if(typeof(files) === undefined || !files.type.match(/image.*/)) {
							$("#avatar .lightbox").click();
							if(!files.type.match(/image.*/)) { error('Please upload an image'); }
						} else {
					    	this.model.upload(files);	
						}
					}
				}
			}
		}
	});
});



