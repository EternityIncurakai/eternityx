function exError(string) {
	
	// setup different parts of the string
	this.errorString = string;
	this.errorCode = this.errorString.replace(/EX([0-9])([0-9])([0-9])([0-9])\:(.*)/g, 'EX$1$2$3$4');
	this.errorDetails = this.errorString.replace(/EX([0-9])([0-9])([0-9])([0-9])\:(.*)/g, '$5').trim();
	
	
}