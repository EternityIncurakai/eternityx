function loggedIn() {
	$.post($(location).attr("href"), { ajax:"login" }, function(data, status) {
		if(data == "Current") {
			return true;
		} else {
			return false;
		}
	});
}
function myIP() {
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.open("GET","http://api.hostip.info/get_html.php",false);
    xmlhttp.send();

    hostipInfo = xmlhttp.responseText.split("\n");

    for (i=0; hostipInfo.length >= i; i++) {
        ipAddress = hostipInfo[i].split(":");
        if ( ipAddress[0] == "IP" ) return ipAddress[1];
    }

    return false;
}
function FileUpload(img, file) {
	var reader = new FileReader();  
	this.ctrl = createThrobber(img);
	var xhr = new XMLHttpRequest();
	this.xhr = xhr;
	  
	var self = this;
	this.xhr.upload.addEventListener("progress", function(e) {
	if (e.lengthComputable) {
	  var percentage = Math.round((e.loaded * 100) / e.total);
	  self.ctrl.update(percentage);
	}
	}, false);
	  
	xhr.upload.addEventListener("load", function(e){
		self.ctrl.update(100);
		 var canvas = self.ctrl.ctx.canvas;
		  canvas.parentNode.removeChild(canvas);
	}, false);
	xhr.open("POST", '/upload.php');
	xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
	reader.onload = function(evt) {
		xhr.sendAsBinary(evt.target.result);
	};
	reader.readAsBinaryString(file);
}

function updateCache() {} // obsolete function

function getTemplate(template) {
	return $("[data-template='"+template+"']").html();
}

function buttonInit() {
	var popper = $("#popper").val();  
	
	sessionStorage.originalTitle = $("title").html();
	sessionStorage.originalURL = $(location).attr("href");
	
	if(sessionStorage.originalURL.match(popper) && popper !== "") {
		sessionStorage.originalURL = sessionStorage.originalURL.replace("/"+popper, "");	
	}
	
	if(!sessionStorage.originalURL.match(/http:\/\//g)) {
		sessionStorage.originalURL = "http://" + sessionStorage.originalURL;
	} 
	
	if(popper !== "") {
		$('[data-href="/'+popper+'"]').click();
	}
	
	window.onpopstate = function(event) {
		console.log('tesst');
		if(event.state !== null) {
			console.log('[data-href="/'+event.state.popper+'"]');
			$('[data-href="/'+event.state.popper+'"]').click();
		}		
	};
	
	$(".btn").each(function() {
		new EButton("#"+$(this).attr("id"));
	});
}



