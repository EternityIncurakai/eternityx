/**
 * 
 */
function emodel(data) {
	
	//data._init(data);
	var model = data;
	
	// predefined function
	
	

	
	// elements 
	if(model.elements) {
		$.each(model.elements, function(key, value) {
			var object = $(value.element);
			object.element = value.element;
			object.validated = value.validated;
			if(value.fieldset) { object.fieldset = value.fieldset; }
			if(value.elements) { object.elements = value.elements; }
			// setup events 
			if(value.events) {
				$.each(value.events, function(key2, value2) {
					object.model = model;
					var functionref = _.bind(value2, object);
					
					
					if(value.element == "#idcontinue") {
						console.log('test')
					}
					$(document).on(key2, value.element, function(e) { if(key2 != "keydown") { e.preventDefault(); }  if(key2 == "drop" || key2 == "keydown") { functionref(e); } else { functionref(e); } });
				});
			}
			
			data[key] = object;
		});
	}	
	$.each(model.functions, function(key, value) {
		model[key] = _.bind(value, model);
	});
	
	model._validated = _.bind(function() {
		var valid = true;
		$.each(this, function(key, value) {
			if(value.validated == "false") {
				valid = false;
			}
		});
		if(valid == true) {
			return true;
		} else {
			return false;
		}
	}, data);
	
	
	if(!model._init()) {
		return;
	}
		
		
	// form
	if(data.form) {
		console.log(data.form);
		$(document).on("submit", data.form.element, function(returner) {
			returner.preventDefault();
			returner.stopPropagation();
			
			$(data.form.element).parent().disable();
			$(data.form.element).prepend('<i style="display:none;" class="fa fa-spinner fa-spin"></i>');
			$(data.form.element).find("i").fadeIn();
			
			var ajaxdata = {
				action : data.form.action,
			};
			
			$.each(model.elements, function(key, value) {
				
				if(value.value) {
					ajaxdata[key] = value.value();
				} else {
					ajaxdata[key] = $(value.element).val();
				}
			});
			
			if(data.form.values) {
				$.each(data.form.values, function(key, value) {
					ajaxdata[key] = value;
				});
			}
			
			
			$.post("<?= EX_URL; ?>/ajax", ajaxdata, function(data2, status) {
				$(data.form.element).find('i').fadeOut(400,function() {$(this).remove()});
				$(data.form.element).parent().enable();
				
				console.log(data2);
				var obj = {};
				if(data2['error']) {
					obj.code = data2['error']['code'];
					obj.string = data2['error']['message'];	
				} else {
					obj.code = '';
					obj.string = '';
				}
				
				
				obj.element = data.form.element;
				obj.model = data;
				
				$.each(data.form.elements, function(key, value) {
					obj[key] = $(value);
				});
				
				
				
				
				if(data2['success'] == 'true') {
					obj.response = data2['response'];
					var func = _.bind(data.form.success, obj);
				} else {
					var func = _.bind(data.form.error, obj);
				}
				func();
			});
		});
	}
		
		
		
	model._init();
	return data;
	/*
	// setup fields
	$.each(model.fields, function(key, value) {
		if(typeof(value) !== 'object') {
			model.fields[key] = $(value);
		} else {
			$.each(value, function(key2, value2) {
				model.fields[key][key2] = $(value2);
			});
		}
	});
	
	$.each(model.functions, function(key, value) {
		model[value] = _.bind(model[value], model);
	});
	
	
	
	$.each(data.events, function(key, value) {
		
		
		var functionref = _.wrap(data[value[1]], function(func) {
			var object = $(key);
			object.model = data;
			func2 = _.bind(func, object);
			func2();
		});
		
		$(document).on(value[0], key, functionref);
	});
	
	
	
	return data;*/
}
