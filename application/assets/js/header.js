$(function() {

	// dropdowns
	$(document).on('click', "[data-toggle='dropdown']", function(e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$(this).parent().find('.dropdown').slideToggle();
	});
	
	$(document).on('click', '.collapse', function() {
		$('body').toggleClass('offcanvas');
		$("html, body").animate({ scrollTop: 0 });
	});
	
	// mobile footer list
	$(".link-list").each(function() {
		var label = $(this).children('h3').html();
		$("#footnav").append("<optgroup label='"+label+"'></optgroup>");
		$(this).find('ul li a').each(function() {
			var href = $(this).attr("href");
			var text = $(this).html();
			$('[label="'+label+'"]').append('<option value="'+href+'">'+text+'</option>');
		});		
	});
	
	// footer navigation link sort of thing.
	$("#footnav").change(function() {
		if($(this).val() != "Jump to") {
			$(location).attr("href", $(this).val());	
		}
	});
	
	
	/** FOOTER FIX **/
	$("footer").attr("style", "margin-top: "+($(document).height() - (($("footer").offset().top + 175) + ($("footer").outerHeight() - $("footer").innerHeight())))+"px;");
	
	
	/** UTM CAMPAIGN FIX **/ 
	if(localStorage.utmdismiss === 'true') {	$(".utm").remove(); }
	$(".utm .dismiss").click(function() {
		localStorage.utmdismiss = true;
		$(".utm").fadeOut(400, function() { $(this).remove(); });
	});
	
	// form controls - tab fix (2.0.4 fix)
	$("header input").keydown(function(e) {
		if(e.keyCode == 9) {
			e.preventDefault();
			$(this).next().next().focus();
		}
	});
	
        $("#container, #adminlinks").swipe({
           swipeLeft: function() {
               $("#adminlinks").addClass('open');
           },
           
           swipeRight: function() {
               $("#adminlinks").removeClass('open');
           }
        });
        
});

function saved() {
	$('.saved').slideDown(function() {
		setTimeout(function() {
			$('.saved').slideUp();
		}, 1000);
	});
}

function error(details) {
	$('.details').html(details);
	$('.error').slideDown(function() {
		setTimeout(function() {
			$('.error').slideUp();
		}, 1000);
	});
}