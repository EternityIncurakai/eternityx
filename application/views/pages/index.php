<? if(empty($_SESSION)) { ?>
	
<div class="hero">
	<h1>Welcome to Eternity Incurakai.</h1>
	<p>Welcome to Eternity Incurakai - your place for social interactions, finding entertainment, - and much more using the Eternity Incurakai interfaces to integrate technology into your life. <a href="/about">Learn more about Eternity Incurakai!</a></a></p>
	<a class="button" href="/signup">Signup for an Incurakai!</a>
</div>


<div class="logo">
	<img src="/img/logo.png" width="111">
</div>
<? } else { 
$news = $pdo->prepare("SELECT `content`, `id` FROM `news` ORDER BY `id` DESC LIMIT 1");
$news->execute();
$newsc = substr(strip_tags(html_entity_decode($news->fetchColumn(0))), 0, 300) . '...';
$news->execute();
$id = $news->fetchColumn(1);
?>
<div class="news">
	<h2>News</h2>
	<p><?= $newsc; ?></p>
	<a href="<?=BASEURL;?>/news#<?=$id;?>">Read More</a>
</div>
<a class="profile" href="/users/<?= $user->username; ?>"><i class="icon-user"></i> Go to My Incurakai</a>


<a href="#" class="mail"><i class="icon-envelope"></i>Incurakai Mail</a>

<? } ?>



<!-- Here for development purposes only -->
<a style="display: none;" href="https://plus.google.com/114522933652045226081" rel="publisher">Google+</a>
