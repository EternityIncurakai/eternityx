<div id="maintenance-hero">
	<h1><?= _("Maintenance Mode"); ?></h1>
	<p>EternityX, the underlying platform and framework running the entire Eternity Incurakai site is being worked on, and that work has been deemed major enough to enforce a site-wide shutdown.  Fear not, this is temporary and will be back up as soon as possible.  More Information:</p>
	<blockquote><?= $configs['maintenance_content']; ?></blockquote>
	<p>We have estimated to be back up at <?= $configs['maintenance_uptime']; ?></p>
	<p><em>Thanks, and Live the Eternity!</em> - XenoK &amp; Eternity Incurakai Studios</p>
</div>

<img src="/img/favicon.png" alt="Eternity Incurakai" id="maintenance-image" />
