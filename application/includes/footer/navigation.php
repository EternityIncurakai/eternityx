<div class="link-list">
	<h3>Navigation</h3>
	<ul>
		<li><a href="<?= EX_URL; ?>">Home</a></li>
		<li><a href="<?= EX_URL; ?>/news">News</a></li>
		<li><a href="https://bitbucket.org/XenoK/eternityx2/issues?status=new&status=open">Bugs/Glitches</a></li>
	</ul>
</div>

<div class="link-list">
	<h3>Support</h3>
	<ul>
		<li><a href="<?= EX_URL; ?>/about">About</a></li>
		<li><a href="<?= EX_URL; ?>/contact">Contact</a></li>
		<li><a href="<?= EX_URL; ?>/jobs">Jobs</a></li>
	</ul>
</div>

<div class="link-list">
	<h3>Legal</h3>
	<ul>
		<li><a href="<?= EX_URL; ?>/policies/terms">Terms and Conditions</a></li>
		<li><a href="<?= EX_URL; ?>/policies/privacy">Privacy Policy</a></li>
		<li><a href="<?= EX_URL; ?>/policies/copyright">Copyright</a></li>
	</ul>
</div><br /><br />

<select id="footnav"></select>
