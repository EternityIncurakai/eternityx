<?
global $dispatcher;

if(isset($GLOBALS['user']) && $GLOBALS['user']->loggedin) { $user = $GLOBALS['user'];?>
	
	<li><a href="/" <? if($dispatcher->request->name == "index") { ?>class="active"<? } ?> >Home</a></li>
	<li><a href="<?=EX_URL;?>/news" <? if($dispatcher->request->name == "news") { ?>class="active"<? } ?>>News</a></li>
	<li>
		<a href="#" data-toggle="dropdown" id="user" <? if($dispatcher->request->name == "users/".$user->username) { ?>class="active"<? } ?>>
		<img src="<?=EX_URL;?>/https?url=api.eauth.org/avatar/<?= $user->username; ?>" alt="<?=$user->username;?>'s Avatar" />
		<?= $user->username; ?> <i class="fa fa-chevron-down"></i></a>
		<ul class="dropdown">
			<li><a href="/users/<?= $user->username; ?>">View Profile</a></li>
			<li><a href="/settings">Settings</a></li>
			<li><a href="/logout" id="logout">Log Out</a></li>
		</ul>
	</li>
	
<? } else { ?>
	
	<!-- Start Login -->
	<li>
		<div id="login" class="login" role="form">
			<div id="loginerror" aria-hidden="true">Login Error <a href="#">(Details)</a><div class="details"></div></div>
			<form>
				<div class="hfield">
					<input  type="text" name="username" id="username" placeholder="Username" required data-minlength="5" data-maxlength="15" aria-label="Username" aria-required="true" />
					<a href="/forgot" class="forgot">Forgot Username?</a>
				</div>
							
				<div class="hfield">
					<input type="password" name="password" id="password" placeholder="Password" required data-minlength="8" data-maxlength="18" aria-label="Password" aria-required="true" />
					<a href="/forgot" class="forgot">Forgot Password?</a>
				</div>
				<input aria-label="Login" type="submit" value="Login" />
			</form>
		</div>
	</li>
	<!-- End Login -->
	<li class="signup"><a href="/signup">Signup</a></li>
	
	
<? } ?>
